<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create the admin
        if (!User::whereEmail('admin@ks.com')->exists()) {
            factory(User::class)->create([
                'first_name' => 'Klean',
                'last_name' => 'Services',
                'email' => 'admin@ks.com',
                'type' => User::TYPE_SUPERADMIN,
            ]);
        }

        // Create a regular user
        // if (!User::whereEmail('user@ks.com')->exists()) {
        //     factory(User::class)->create([
        //         'email' => 'user@ks.com',
        //         'type' => User::TYPE_CLIENT,
        //     ]);
        // }

        // Seed some extra users
        // factory(User::class, 200)->create();
    }
}
