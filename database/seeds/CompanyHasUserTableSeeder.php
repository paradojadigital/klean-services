<?php

use App\Company;
use App\User;
use Illuminate\Database\Seeder;

class CompanyHasUserTableSeeder extends Seeder
{
    /**
     * Adds an owner and some admins to companies
     *
     * @return void
     */
    public function run()
    {
        $companies = Company::whereDoesntHave('users')->get();

        $companies->each(function (Company $company) {
        	$client = User::whereType(User::TYPE_CLIENT)->doesntHave('companies')->inRandomOrder()->first();
            
            // Attach the user as owner
            $company->users()->attach($client, [
                'role' => Company::ROLE_OWNER,
            ]);

            // And maybe attach admins
            while (true) {
                if (rand(1, 100) < 30) {
                    $client = User::whereType(User::TYPE_CLIENT)->doesntHave('companies')->inRandomOrder()->first();
                    $company->users()->attach($client, [
                        'role' => Company::ROLE_ADMIN,
                    ]);
                } else {
                    return;
                }
            }
        });
    }
}
