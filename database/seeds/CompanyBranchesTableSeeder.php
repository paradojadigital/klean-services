<?php

use App\Branch;
use App\Company;
use Illuminate\Database\Seeder;

class CompanyBranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = Company::doesntHave('branches')->get();

        $companies->each(function (Company $company) {
        	$company->branches()->saveMany(factory(Branch::class, rand(0, 5))->make());
        });
    }
}
