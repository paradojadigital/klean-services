<?php

use App\Branch;
use App\Order;
use App\Product;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class OrdersRealisticSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        Branch::each(function (Branch $branch) use ($output) {
            $admins = $branch->users()->wherePivot('role', Branch::ROLE_ADMIN)->get();
            $products = $branch->products;

            if (!$admins->count()) {
                return $output->writeln('Branch '.$branch->id.' has no admins!, can\'t create an Order');
            }

            while (true) {
                if (rand(1, 100) > 60) {
                    return;
                }

                // Add a random number of products
                $cart = $products->random(rand(1, 7));

                $order = $branch->orders()->create([
                    'user_id' => $admins->random()->id,
                    'status' => collect([
                        Order::STATUS_NEW,
                        Order::STATUS_PENDING,
                        Order::STATUS_PAYED,
                        Order::STATUS_DELIVERED,
                        Order::STATUS_COMPLETED,
                    ])->random(),
                    'data' => [],
                    'created_at' => Carbon::now()->subSeconds(rand(0,31536000)), // 1 Year
                ]);

                foreach ($cart as $product) {
                    $order->products()->attach($product->id, [
                        'quantity' => rand(1, 5),
                        'price' => $product->pivot->price,
                    ]);
                }

                // Calculate the total
                $total = 0;
                $order->products->each(function (Product $product) use (&$total) {
                    $total += $product->pivot->quantity * $product->pivot->price;
                });

                // And update the order
                $order->update([
                    'data' => $order->products()->get(['id', 'name'])->toJson(),
                    'total' => $total,
                ]);
            }
        });
    }
}
