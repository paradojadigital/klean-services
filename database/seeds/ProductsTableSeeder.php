<?php

use App\Category;
use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Creates a bunch of products for each existing category
     *
     * @return void
     */
    public function run()
    {
    	$categories = Category::all();

        $categories->each(function (Category $category) {
            $category->products()->saveMany(factory(Product::class, rand(0, 16))->make());
        });
    }
}
