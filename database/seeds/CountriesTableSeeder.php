<?php

use App\Country;
use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	if (Country::count()) {
    		return;
    	}

        Country::create([
            'name' => 'Mexico',
            'name_code' => 'MX',
            'currency_name' => 'Pesos',
            'currency_code' => 'MXN',
            'currency_symbol' => '$',
            'image' => '/images/countries/mx.png',
        ]);
        Country::create([
            'name' => 'Colombia',
            'name_code' => 'CO',
            'currency_name' => 'Pesos',
            'currency_code' => 'COP',
            'currency_symbol' => '$',
            'image' => '/images/countries/co.png',
        ]);
    }
}
