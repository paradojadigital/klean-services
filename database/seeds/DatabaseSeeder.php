<?php

use App\Branch;
use App\Category;
use App\Company;
use App\Country;
use App\Product;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            CountriesTableSeeder::class,
            // CompaniesTableSeeder::class,
            // CompanyBranchesTableSeeder::class,
            // CompanyHasUserTableSeeder::class,
            // BranchHasUserTableSeeder::class,
            // BranchHasStaffTableSeeder::class,
            CategoriesTableSeeder::class,
            // ProductsTableSeeder::class,
            // CountryHasProductTableSeeder::class,
            // ProductHiddenForCompanyTableSeeder::class,
            // OrdersRealisticSeeder::class,
        ]);
    }
}
