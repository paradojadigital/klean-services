<?php

use App\Branch;
use App\Company;
use App\User;
use Illuminate\Database\Seeder;

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed companies and their branches
        Company::doesntHave('branch')->each(function (Company $company) {
            $company->branches()->saveMany(factory(Branch::class, rand(1, 5))->make());
        });
    }
}
