<?php

use App\Branch;
use App\User;
use Illuminate\Database\Seeder;

class BranchHasStaffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Grab branches without users assigned
        $branches = Branch::doesntHave('kleanServicesStaff')->get();

        $staff = User::whereType(User::TYPE_STAFF)->get();
        $branches->each(function (Branch $branch) use ($staff) {
            // Don't give staff to branches which's id is multiple of 3
            if ($branch->id % 3 === 0) {
                return;
            }

            // Add a random number of 
            while (true) {
                if (rand(1, 100) < 50) {
                    $branch->kleanServicesStaff()->syncWithoutDetaching($staff->random());
                } else {
                    return;
                }
            }
        });
    }
}
