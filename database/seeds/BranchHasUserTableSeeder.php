<?php

use App\Branch;
use App\User;
use Illuminate\Database\Seeder;

class BranchHasUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branches = Branch::whereDoesntHave('users', function ($query) {
            $query->where('role', Branch::ROLE_ADMIN);
        })->get();

        $branches->each(function (Branch $branch) {
            $client = User::whereType(User::TYPE_CLIENT)->doesntHave('branches')->inRandomOrder()->first();
            
            // Attach an admin
            $branch->users()->attach($client, [
                'role' => Branch::ROLE_ADMIN,
            ]);

            // Attach some staff
            while (true) {
                if (rand(1, 100) < 30) {
                    $client = User::whereType(User::TYPE_CLIENT)->doesntHave('branches')->inRandomOrder()->first();
                    $branch->users()->attach($client, [
                        'role' => Branch::ROLE_STAFF,
                    ]);
                } else {
                    return;
                }
            }
        });
    }
}
