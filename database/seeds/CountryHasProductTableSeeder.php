<?php

use App\Country;
use App\Product;
use Illuminate\Database\Seeder;

class CountryHasProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add a price to products
        $countries = Country::all();
        $products = Product::doesntHave('prices')->get();
        
        $products->each(function (Product $product) use ($countries) {
            // Iterate over countries
            $countries->each(function (Country $country) use ($product) {
                // Chance for the product to be available in this country
                if (rand(1, 100) < 75) {
                    $country->products()->syncWithoutDetaching([$product->id => [
                        'price' => rand(1000, 100000) / 100,
                    ]]);
                }
            });
        });
    }
}
