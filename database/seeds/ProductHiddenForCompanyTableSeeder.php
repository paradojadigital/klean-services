<?php

use App\Company;
use App\Product;
use Illuminate\Database\Seeder;

class ProductHiddenForCompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();

        Company::doesntHave('hiddenProducts')->each(function (Company $company) use ($products) {
            $hidden = $products->random(rand(0,10));
            $company->hiddenProducts()->sync($hidden);
        });
    }
}
