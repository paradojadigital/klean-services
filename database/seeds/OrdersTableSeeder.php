<?php

use App\Branch;
use App\Order;
use App\Product;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Branch::each(function (Branch $branch) {
            while (true) {
                if (rand(1, 100) > 60) {
                    return;
                }

                $order = $branch->orders()->saveMany(factory(Order::class, 5)->make());
            }
        });
    }
}
