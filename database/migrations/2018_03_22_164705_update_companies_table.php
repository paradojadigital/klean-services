<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            // RFC (Tax ID Number)
            $table->string('tin');
            $table->string('business_name');
            $table->string('zipcode');
            $table->string('neighborhood');
            $table->string('street'); 
            $table->string('number_ext');
            $table->string('number_int')->nullable();
            $table->string('city');
            $table->string('state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            // RFC
            $table->dropColumn('tin');
            $table->dropColumn('business_name');
            $table->dropColumn('zipcode');
            $table->dropColumn('neighborhood');
            $table->dropColumn('street');
            $table->dropColumn('number_ext');
            $table->dropColumn('number_int');
            $table->dropColumn('city');
            $table->dropColumn('state');
        });
    }
}
