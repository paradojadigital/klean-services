<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCompanyBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_branches', function (Blueprint $table) {
            $table->dropColumn('address');
            $table->string('phone');
            $table->string('zipcode');
            $table->string('neighborhood');
            $table->string('street'); 
            $table->string('number_ext');
            $table->string('number_int')->nullable();
            $table->string('city');
            $table->string('state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_branches', function (Blueprint $table) {
            $table->string('address');
            $table->dropColumn('phone');
            $table->dropColumn('zipcode');
            $table->dropColumn('neighborhood');
            $table->dropColumn('street'); 
            $table->dropColumn('number_ext');
            $table->dropColumn('number_int');
            $table->dropColumn('city');
            $table->dropColumn('state');
        });
    }
}
