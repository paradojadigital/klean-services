<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'type' => $faker->randomElement(['admin', 'client', 'staff']),
        'first_name' => $faker->firstName(),
        'last_name' => $faker->lastName,
        'birthdate' => $faker->optional()->dateTimeThisCentury(),
        'image' => $faker->imageUrl(250, 250, 'people'),
        'password' => Hash::make('secret'),
        'remember_token' => str_random(10),
    ];
});
