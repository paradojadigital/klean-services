<?php

use App\Order;
use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'status' => $faker->randomElement([
            Order::STATUS_NEW,
            Order::STATUS_PENDING,
            Order::STATUS_PAYED,
            Order::STATUS_DELIVERED,
            Order::STATUS_COMPLETED,
        ]),
        'total' => $faker->numberBetween(1500, 16000),
        'data' => [],
        'created_at' => $faker->dateTimeThisYear(),
    ];
});
