<?php

use Faker\Generator as Faker;

$factory->define(App\Company::class, function (Faker $faker) {
	return [
        'name' => $faker->company,
        'image' => $faker->imageUrl(250, 250, 'business'),
        'country_id' => $faker->numberBetween(1, 2), // Works only if countries with ids 1 and 2 exist
    ];
});
