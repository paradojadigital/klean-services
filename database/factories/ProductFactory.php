<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
    	'sku' => $faker->uuid,
        'name' => $faker->catchPhrase,
        'description' => $faker->realText,
        'image' => $faker->imageUrl(250, 250),
    ];
});
