<!-- # admin sidebar. -->
@php
$is_admin = (
    Auth::user()->type == App\User::TYPE_SUPERADMIN ||
    Auth::user()->type == App\User::TYPE_ADMIN
);
@endphp

@if ($is_admin)
<aside class="sidebar">
    <a href="{{ route('home') }}">
        <div class="logo">
            <img src="/images/logo.png" alt="Central Work & Shop">
        </div>
    </a>

    <ul class="sidebar-menu">
        <li class="sidebar-menu-item">
            <a href="{{ route('home') }}"
            class="dashboard {{
                !(strpos(Route::currentRouteName(), 'home') === 0) ?:
                'active'
            }}">
                <span class="sidebar-link">
                    <i class="icon"></i>
                    <span>Dashboard</span>
                </span>

                <!-- <div class="notifications">1</div> -->
            </a>
        </li>

        <li class="sidebar-menu-item">
            <a href="{{ route('companies.index') }}"
            class="clients {{
                !(strpos(Route::currentRouteName(), 'companies') === 0) &&
                !(strpos(Route::currentRouteName(), 'branches') === 0) ?:
                'active'
            }}">
                <span class="sidebar-link">
                    <i class="icon"></i>
                    <span>Clientes</span>
                </span>

                <!-- <div class="notifications">1</div> -->
            </a>
        </li>

        <li class="sidebar-menu-item">
            <a href="{{ route('products.index') }}"
            class="products {{
                !(strpos(Route::currentRouteName(), 'products') === 0) ?:
                'active'
            }}">
                <span class="sidebar-link">
                    <i class="icon"></i>
                    <span>Productos</span>
                </span>

                <!-- <div class="notifications">1</div> -->
            </a>
        </li>

        <li class="sidebar-menu-item">
            <a href="{{ route('orders.index') }}"
            class="orders {{
                !(strpos(Route::currentRouteName(), 'orders') === 0) ?:
                'active'
            }}">
                <span class="sidebar-link">
                    <i class="icon"></i>
                    <span>Pedidos</span>
                </span>

                @if ($pendingOrders())
                    <div class="notifications">{{ $pendingOrders() }}</div>
                @endif
            </a>
        </li>

        <li class="sidebar-menu-item">
            <a href="{{ route('reports.index') }}"
            class="reports {{
                !(strpos(Route::currentRouteName(), 'reports') === 0) ?:
                'active'
            }}">
                <span class="sidebar-link">
                    <i class="icon"></i>
                    <span>Reportes</span>
                </span>

                <!-- <div class="notifications">1</div> -->
            </a>
        </li>

        <li class="sidebar-menu-item">
            <a href="{{ route('staff.index') }}"
            class="employees {{
                !(strpos(Route::currentRouteName(), 'staff') === 0) ?:
                'active'
            }}">
                <span class="sidebar-link">
                    <i class="icon"></i>
                    <span>Personal</span>
                </span>

                <!-- <div class="notifications">1</div> -->
            </a>
        </li>
    </ul>
</aside>
@endif
<!-- #/ admin sidebar. -->

<!-- # client sidebar. -->
@if (!$is_admin)
<aside class="sidebar">
    <a href="{{ route('home') }}">
        <div class="logo">
            <img src="/images/logo.png" alt="Central Work & Shop">
        </div>
    </a>

    <ul class="sidebar-menu">
        @if (request()->branch)
            <li class="sidebar-menu-item">
                <a href="{{ route('client.home', ['branch' => $currentBranch()]) }}"
                class="dashboard {{
                    !(strpos(Route::currentRouteName(), 'client.home') === 0) ?:
                    'active'
                }}">
                    <span class="sidebar-link">
                        <i class="icon"></i>
                        <span>Dashboard</span>
                    </span>

                    <!-- <div class="notifications">1</div> -->
                </a>
            </li>

            @if (Arrays::contains([App\Company::ROLE_ADMIN, App\Company::ROLE_OWNER], $roleCompany()) ||
                Arrays::contains([App\Branch::ROLE_ADMIN], $roleBranch()))
                <li class="sidebar-menu-item">
                    <a href="{{ route('branches.staff.index', ['company' => $currentCompany(), 'branch' => $currentBranch()]) }}"
                    class="clients {{
                        !(strpos(Route::currentRouteName(), 'branches.staff') === 0) ?:
                        'active'
                    }}">
                        <span class="sidebar-link">
                            <i class="icon"></i>
                            <span>Mi Staff</span>
                        </span>

                        <!-- <div class="notifications">1</div> -->
                    </a>
                </li>
            @endif

            <li class="sidebar-menu-item">
                <a href="{{ route('client.products.index', ['branch' => $currentBranch()]) }}"
                class="products {{
                    !(strpos(Route::currentRouteName(), 'client.products') === 0) ?:
                    'active'
                }}">
                    <span class="sidebar-link">
                        <i class="icon"></i>
                        <span>Productos</span>
                    </span>

                    <!-- <div class="notifications">1</div> -->
                </a>
            </li>

            <li class="sidebar-menu-item">
                <a href="{{ route('client.history.index', ['branch' => $currentBranch()]) }}"
                class="orders {{
                    !(strpos(Route::currentRouteName(), 'client.history') === 0) ?:
                    'active'
                }}">
                    <span class="sidebar-link">
                        <i class="icon"></i>
                        <span>Historial</span>
                    </span>

                    @if (request()->branch && $branchPendingOrders())
                        <div class="notifications">{{ $branchPendingOrders() }}</div>
                    @endif
                </a>
            </li>

            @if (Arrays::contains([App\Company::ROLE_ADMIN, App\Company::ROLE_OWNER], $roleCompany()) ||
                Arrays::contains([App\Branch::ROLE_ADMIN], $roleBranch()))
                <!-- <li class="sidebar-menu-item">
                    <a href="{{ route('client.services.index', ['branch' => $currentBranch()]) }}"
                    class="services {{
                        !(strpos(Route::currentRouteName(), 'client.services') === 0) ?:
                        'active'
                    }}">
                        <span class="sidebar-link">
                            <i class="icon"></i>
                            <span>Servicios</span>
                        </span>

                        <div class="notifications">2</div>
                    </a>
                </li> -->

                <li class="sidebar-menu-item">
                    <a href="{{ route('client.reports.index', ['branch' => $currentBranch()]) }}"
                    class="reports {{
                        !(strpos(Route::currentRouteName(), 'client.reports') === 0) ?:
                        'active'
                    }}">
                        <span class="sidebar-link">
                            <i class="icon"></i>
                            <span>Reportes</span>
                        </span>

                        <!-- <div class="notifications">1</div> -->
                    </a>
                </li>
            @endif

            @if (request()->branch)
                @if (Arrays::contains([App\Company::ROLE_OWNER, App\Company::ROLE_ADMIN], $roleCompany()) ||
                    Arrays::contains([App\Branch::ROLE_ADMIN], $roleBranch()))
                    <li class="sidebar-menu-item">
                        <a href="{{ route('client.company.index', ['company' => $currentCompany(), 'branch' => $currentBranch()]) }}"
                        class="company {{
                            !(strpos(Route::currentRouteName(), 'client.company') === 0) ?:
                            'active'
                        }}">
                            <span class="sidebar-link">
                                <i class="icon"></i>
                                <span>Mi empresa</span>
                            </span>

                            <!-- <div class="notifications">1</div> -->
                        </a>
                    </li>
                @endif
            @endif
        @endif
    </ul>
</aside>
@endif
<!-- #/ client sidebar. -->
