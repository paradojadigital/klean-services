<div id="modal-building-members" class="modal {{ session('modal') ? 'active' : '' }}">
    {{
        Form::model($user, [
            'route' => ['company.staff.store', $company->id],
            'method' => 'POST',
            'id' => 'form-modal',
            'class' => 'row',
            'files' => true,
        ])
    }}
        <div class="col-md-12">
            <div class="modal-header">
                <h3 class="form-section">Nuevo Admin ({{ $company->name }})</h3>
                <a href="" class="modal-close"><i class="fa fa-close"></i></a>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-field form-field-image">
                <label for="image" class="form-label">Foto</label>
                <input id="image" type="file" class="form-input" name="image">
                <label for="image" class="form-file">
                    <img src="{{ old('src') }}" alt="">
                    <input type="hidden" name="src" value="{{ old('src') }}">
                    <span class="form-file-message">
                        <span>Arrastra tu imagen</span>
                        <span>500 x 500</span>
                    </span>
                </label>

                @if ($errors->get('image'))
                    <div class="alert alert-danger">
                        {{ $errors->first('image') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-field">
                <label for="first-name" class="form-label">Nombre</label>
                <input id="first-name" type="text" class="form-input" name="first_name" placeholder="Nombre" value="{{ old('first_name') }}">

                @if ($errors->get('first_name'))
                    <div class="alert alert-danger">
                        {{ $errors->first('first_name') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-field">
                <label for="last-name" class="form-label">Appellidos</label>
                <input id="last-name" type="text" class="form-input" name="last_name" placeholder="Apellidos" value="{{ old('last_name') }}">

                @if ($errors->get('last_name'))
                    <div class="alert alert-danger">
                        {{ $errors->first('last_name') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-field">
                <label for="email" class="form-label">Correo electrónico</label>
                <input id="email" type="text" class="form-input" name="email" placeholder="ejemplo@email.com" value="{{ old('email') }}">

                @if ($errors->get('email'))
                    <div class="alert alert-danger">
                        {{ $errors->first('email') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="col-md-12">
            <div class="section-row">
                @if ($errors->get('exists'))
                    <div class="alert alert-danger">
                        <p>Nota: {{ $errors->first('exists') }}</p>
                    </div>
                @endif
            </div>
        </div>

        <div class="col-md-6 offset-md-3">
            <div class="form-field">
                <input type="submit" class="btn btn-pri" value="Guardar">
            </div>
        </div>
    </form>
</div>
