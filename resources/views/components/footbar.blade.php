@if (Route::currentRouteName() === 'client.products.add')
<div class="footbar">
    <p id="products-count" class="products-count">0 productos seleccionados</p>
    <a id="products-next" href="{{ route('client.products.checkout', [
        'branch' => $currentBranch()->id,
        'quote' => $quote
    ]) }}" class="btn btn-pri">Continuar</a>
</div>
@endif
