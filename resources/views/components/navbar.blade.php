<nav class="navbar">
    @switch (Route::currentRouteName())
        @case ('client.company.branches')
            <h2>Edificios</h2>
            @break

        @case ('client.company.admins')
            <h2>
                <a href="{{ route('client.company.branches', ['company' => $company->id]) }}" class="fa fa-angle-left"></a>
                Admins
            </h2>
            @break

        @case ('home')
            <h2>Dashboard</h2>
            @break

        @case ('companies.index')
            <h2>Clientes</h2>
            @break

        @case ('companies.admins')
            <h2>
                <a href="{{ route('branches.index', ['company' => $company->id]) }}" class="fa fa-angle-left"></a>
                Admins
            </h2>
            @break

        @case ('companies.create')
            <h2>
                <a href="{{ route('companies.index') }}" class="fa fa-angle-left"></a>
                Nuevo Cliente
            </h2>
            @break

        @case ('companies.edit')
            <h2>
                <a href="{{ route('companies.index') }}" class="fa fa-angle-left"></a>
                Editar Cliente
            </h2>
            @break

        @case ('branches.index')
            <h2>
                <a href="{{ route('companies.index') }}" class="fa fa-angle-left"></a>
                Edificios
            </h2>
            @break

        @case ('branches.create')
            <h2>
                <a href="{{ route('branches.index', ['company' => $company->id]) }}" class="fa fa-angle-left"></a>
                Nuevo Edificio
            </h2>
            @break

        @case ('branches.orders')
            <h2>
                <a href="{{ route('branches.index', ['company' => $company->id]) }}" class="fa fa-angle-left"></a>
                Edificio (Pedidos)
            </h2>
            @break

        @case ('branches.members')
            <h2>
                <a href="{{ route('branches.index', ['company' => $company->id]) }}" class="fa fa-angle-left"></a>
                Edificio (Integrantes)
            </h2>
            @break

        @case ('orders.index')
            <h2>Pedidos</h2>
            @break

        @case ('orders.show')
            <h2>
                <a href="{{ route('orders.index') }}" class="fa fa-angle-left"></a>
                Pedido
            </h2>
            @break

        @case ('products.index')
            <h2>Productos</h2>
            @break

        @case ('products.create')
            <h2>
                <a href="{{ route('products.index') }}" class="fa fa-angle-left"></a>
                Nuevo Producto
            </h2>
            @break

        @case ('staff.index')
            <h2>Integrantes</h2>
            @break

        @case ('staff.create')
            <h2>
                <a href="{{ route('staff.index') }}" class="fa fa-angle-left"></a>
                Nuevo Integrante
            </h2>
            @break

        @case ('client.home')
            <h2>Dashboard</h2>
            @break

        @case ('client.products.index')
            <h2>Productos</h2>
            @break

        @case ('client.products.add')
            <h2>
                <a href="{{ route('client.products.index', ['branch' => $currentBranch()]) }}" class="fa fa-angle-left"></a>
                Añade Productos
            </h2>
            @break

        @case ('client.products.checkout')
            <h2>
                <a href="{{ route('client.products.add', ['branch' => $currentBranch(), 'quote' => $quote]) }}{{ !count($products) ? '' : '?products='.implode(',', $products) }}" class="fa fa-angle-left"></a>
                Realizar Pedido
            </h2>
            @break

        @case ('client.services.index')
            <h2>Servicios</h2>
            @break

        @case ('client.history.index')
            <h2>Historial</h2>
            @break

        @case ('client.history.show')
            <h2>
                <a href="{{ route('client.history.index', ['branch' => $currentBranch()]) }}" class="fa fa-angle-left"></a>
                Pedido
            </h2>
            @break

        @case ('client.reports.index')
            <h2>Reportes</h2>
            @break

        @case ('branches.staff.index')
            <h2>Mi Staff</h2>
            @break

        @case ('client.company.branches.create')
            <h2>
                <a href="{{ route('client.company.branches', ['company' => $company->id]) }}" class="fa fa-angle-left"></a>
                Nuevo Edificio
            </h2>
            @break

        @case ('users.edit')
            <h2>
                <a href="{{ route('home') }}" class="fa fa-angle-left"></a>
                Perfil
            </h2>
            @break

        @default
            <div></div>
    @endswitch

    <ul class="navbar-menu">
        @if (request()->branch && Auth::user()->type == App\User::TYPE_CLIENT)
            @if (Arrays::contains([App\Company::ROLE_OWNER, App\Company::ROLE_ADMIN], $roleCompany()))
                <li class="navbar-menu-item">
                    <a class="navbar-building">
                        <div class="form-field">
                            <form id="branch" method="GET" action="{{ action('ClientHomeController@branch') }}" class="form-select">
                                <select class="form-input" name="branch_id" change-branch="#branch">
                                    <option value="all">Ver todas</option>
                                    @foreach($allBranches() as $branch)
                                        <option value="{{ $branch->id }}"
                                            {{ request()->branch ? $currentBranch()->id == $branch->id ? 'selected' : '' : '' }}>
                                            {{ $branch->name }}
                                        </option>
                                    @endforeach
                                </select>

                                <i class="fa fa-caret-down caret"></i>
                            </form>
                        </div>
                    </a>
                </li>
            @endif

            @if (Auth::user()->branches->count() > 1)
                <li class="navbar-menu-item">
                    <a class="navbar-building">
                        <div class="form-field">
                            <form id="branch" method="GET" action="{{ action('ClientHomeController@branch') }}" class="form-select">
                                <select class="form-input" name="branch_id" change-branch="#branch">
                                    @foreach (Auth::user()->branches as $branch)
                                        <option value="{{ $branch->id }}"
                                            {{ request()->branch ? request()->branch->id == $branch->id ? 'selected' : '' : '' }}>
                                            {{ $branch->name }}
                                        </option>
                                    @endforeach
                                </select>

                                <i class="fa fa-caret-down caret"></i>
                            </form>
                        </div>
                    </a>
                </li>
            @endif
        @endif

        @if (Auth::user()->type === App\User::TYPE_ADMIN || Auth::user()->type === App\User::TYPE_SUPERADMIN)
        <li class="navbar-menu-item dropdown-container">
            <a href="" class="navbar-notifications">
                <div class="alarm">
                    @if (Auth::user()->unreadNotifications()->count())
                    <div class="has-notifications"></div>
                    @endif
                </div>
            </a>

            <app-notifications></app-notifications>
        </li>
        @endif

        <li class="navbar-menu-item dropdown-container">
            <a href="" class="navbar-user">
                <div class="navbar-user-image">
                    <img src="{{ Auth::user()->image }}" alt="{{ Auth::user()->name }}">
                </div>

                <span>{{ Auth::user()->name }}</span>
                <i class="fa fa-angle-down"></i>
            </a>

            <ul class="dropdown">
                <li class="dropdown-item">
                    <a href="{{ route('users.edit', ['user' => Auth::user()->id]) }}">Perfil</a>
                </li>

                <li class="dropdown-item">
                    <form action="{{ route('logout') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" value="Cerrar sesión">
                    </form>
                </li>
            </ul>
        </li>
    </ul>
</nav>
