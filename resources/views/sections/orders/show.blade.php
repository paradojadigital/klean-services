@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="form-field form-field-inline">
                <label for="status" class="form-label">Status</label>
                <div class="v-preloading preloading-status"></div>
                <app-change-status
                    url="{{ route('orders.update', ['order' => $order->id]) }}"
                    :statuses='{!! $statuses !!}'
                    current_status="{{ $order->status }}"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-5 col-xl-3">
            <div class="order-card">
                <p class="order-card-author">{{ $order->user->name }}</p>
                <p class="order-card-street">{{ $order->branch->street }} {{ $order->branch->neighborhood }} {{ $order->branch->number_ext }} {{ $order->branch->number_int }}</p>
                <p class="order-card-country">{{ $order->branch->city }} {{ $order->branch->state }} CP: {{ $order->branch->zipcode }}</p>
                <p class="order-card-phone">Phone: {{ $order->branch->phone }}</p>
                <p class="order-card-email">Pais: {{ $order->branch->company->country->name }}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="table table-order-detail">
                <table>
                    <thead>
                        <tr>
                            <th class="col-image"></th>
                            <th class="col-quantity">Cantidad</th>
                            <th class="col-product">Producto</th>
                            <th class="col-description">Descripción</th>
                            <th class="col-category">Categoría</th>
                            <th class="col-unit-price">Precio unitario</th>
                            <th class="col-total">Total</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($order->products as $product)
                            <tr>
                                <td class="col-image">
                                    <div class="product-image">
                                        <img src="{{ $product->image }}" alt="{{ $product->name }}">
                                    </div>
                                </td>
                                <td class="col-quantity">{{ $product->pivot->quantity }}</td>
                                <td class="col-product">{{ $product->name }}</td>
                                <td class="col-description">{{ $product->description }}</td>
                                <td class="col-category">{{ $product->category->name }}</td>
                                <td class="col-unit-price">${{ number_format($product->pivot->price, 2) }}</td>
                                <td class="col-total">${{ number_format($product->pivot->price * $product->pivot->quantity, 2) }}</td>
                            </tr>
                        @endforeach

                        <tr>
                            <td class="col-subtotal" colspan="6">Subtotal</td>
                            <td class="col-subtotal">{{ number_format($order->total, 2) }}</td>
                        </tr>

                        <tr>
                            <td class="col-iva" colspan="6">IVA</td>
                            <td class="col-iva">{{ number_format($order->total * $order->vat, 2) }}</td>
                        </tr>

                        <tr>
                            <td class="col-total-all" colspan="6">Total</td>
                            <td class="col-total-all">{{ number_format($order->total * (1 + $order->vat), 2) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
