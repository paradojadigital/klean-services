@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <form class="search-bar" method="GET" action="{{ action('OrdersController@index') }}">
                <div class="form-search">
                    <i class="fa fa-search icon"></i>
                    <input id="search" type="text" class="form-input" name="id" placeholder="Buscar Pedidos" value="{{ $id }}">
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="tabs md">
                <li class="tab-item {{ $status == null ? 'active' : '' }}">
                    <a href="{{ route('orders.index') }}">Todas</a>
                </li>
                
                <li class="tab-item {{ $status == App\Order::STATUS_PENDING ? 'active' : '' }}">
                    <a href="{{ route('orders.index', ['status' => App\Order::STATUS_PENDING]) }}">
                        Pendientes ({{ App\Order::where('status', App\Order::STATUS_PENDING)->count() }})
                    </a>
                </li>
                
                <li class="tab-item {{ $status == App\Order::STATUS_COMPLETED ? 'active' : '' }}">
                    <a href="{{ route('orders.index', ['status' => App\Order::STATUS_COMPLETED]) }}">
                        Completadas ({{ App\Order::where('status', App\Order::STATUS_COMPLETED)->count() }})
                    </a>
                </li>

                <li class="tab-item {{ $status == App\Order::STATUS_DELIVERED ? 'active' : '' }}">
                    <a href="{{ route('orders.index', ['status' => App\Order::STATUS_DELIVERED]) }}">
                        Enviados ({{ App\Order::where('status', App\Order::STATUS_DELIVERED)->count() }})
                    </a>
                </li>

                <li class="tab-item {{ $status == App\Order::STATUS_PAYED ? 'active' : '' }}">
                    <a href="{{ route('orders.index', ['status' => App\Order::STATUS_PAYED]) }}">
                        Pagados ({{ App\Order::where('status', App\Order::STATUS_PAYED)->count() }})
                    </a>
                </li>

                <li class="tab-item {{ $status == App\Order::STATUS_NEW ? 'active' : '' }}">
                    <a href="{{ route('orders.index', ['status' => App\Order::STATUS_NEW]) }}">
                        Nuevos ({{ App\Order::where('status', App\Order::STATUS_NEW)->count() }})
                    </a>
                </li>

                <li class="tab-item {{ $status == App\Order::STATUS_CANCELED ? 'active' : '' }}">
                    <a href="{{ route('orders.index', ['status' => App\Order::STATUS_CANCELED]) }}">
                        Cancelados ({{ App\Order::where('status', App\Order::STATUS_CANCELED)->count() }})
                    </a>
                </li>
            </ul>

            <div class="table table-orders">
                <table>
                    @if ($orders->count())
                        <thead>
                            <tr>
                                <th class="col-id">Nº</th>
                                <th class="col-date">Fecha</th>
                                <th class="col-status">Status</th>
                                <th class="col-company">Empresa</th>
                                <th class="col-branch">Edificio</th>
                                <th class="col-author">Realizado por</th>
                            </tr>
                        </thead>
                    @endif

                    <tbody>
                        @foreach ($orders as $order)
                        <tr class="{{ $order->status }}">
                            <td class="col-id">
                                <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->id }}</a>
                            </td>
                            <td class="col-date">
                                <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->created_at->format('d M Y - h:m a') }}</a>
                            </td>
                            <td class="col-status">
                                <a href="{{ route('orders.show', ['order' => $order->id]) }}">
                                    <div class="status-tag {{ $order->status_class }}">{{ __('app.status.'.$order->status) }}</div>
                                </a>
                            </td>
                            <td class="col-company">
                                <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->company->name }}</a>
                            </td>
                            <td class="col-branch">
                                <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->branch->name }}</a>
                            </td>
                            <td class="col-author">
                                <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->user->name }}</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $orders->links() }}

                @if (!$orders->count())
                    <div class="table-empty">
                        <p>Sin pedidos</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
