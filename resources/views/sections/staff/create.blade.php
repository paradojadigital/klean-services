@extends('containers.content')

@section('content')
<div class="container-fluid not-separated">
    <div class="row">
        <div class="save-bar">
            <a href="{{ route('staff.index') }}" class="btn btn-duo">Cancelar</a>
            <input type="button" class="btn btn-pri" value="Guardar" submit="#form-staff">
        </div>

        <div class="col-lg-10 offset-lg-1 col-xl-6 offset-xl-3">
            <div class="wrapper">
                @if(Route::currentRouteName() == 'staff.create')
                    {!! Form::open(['url' => route('staff.store'), 'method' => 'POST', 'id' => 'form-staff', 'files' => true, 'no-btn']) !!}
                @else
                    {!! Form::open(['url' => route('staff.update', ['user' => $user->id]), 'method' => 'PATCH', 'id' => 'form-staff', 'files' => true, 'no-btn']) !!}
                @endif
                    <div class="row">
                        <div class="col-md-12 section-row">
                            <div class="form-field form-field-image">
                                <label for="image" class="form-label">Foto</label>
                                {!! Form::file('image', ['id' => 'image', 'class' => 'form-input', 'autofocus']) !!}
                                <label for="image" class="form-file">
                                    @if(isset($user))
                                        <img src="{{ $user->image }}" alt="{{ $user->name }}">
                                    @else
                                        <img src="" alt="">
                                    @endif
                                    <span class="form-file-message">
                                        <span>Arrastra tu imagen</span>
                                        <span>500 x 500</span>
                                    </span>
                                </label>
                                
                                @if($errors->get('image'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('image') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-field">
                                <label for="" class="form-label">Asignar rol</label>
                                
                                <ul id="staff-role" class="roles-list">
                                    <li class="roles-item">
                                        <div class="form-radio">
                                            <input id="admin" type="radio" name="role" value="{{ App\Branch::ROLE_ADMIN }}"
                                            {{ old('role') == App\Branch::ROLE_ADMIN ? 'checked' : isset($user) && $user->type == App\Branch::ROLE_ADMIN ? 'checked' : 'checked' }}>
                                            <label for="admin">Administrador</label>
                                        </div>
                                    </li>

                                    <li class="roles-item">
                                        <div class="form-radio">
                                            <input id="staff" type="radio" name="role" value="{{ App\Branch::ROLE_STAFF }}"
                                            {{ old('role') == App\Branch::ROLE_STAFF ? 'checked' : isset($user) && $user->type == App\Branch::ROLE_STAFF ? 'checked' : '' }}>
                                            <label for="staff">Staff</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-field">
                                <label for="first-name" class="form-label">Nombres</label>
                                {!! Form::text('first_name', isset($user) ? $user->first_name : null, ['id' => 'first-name', 'class' => 'form-input', 'placeholder' => 'Nombres']) !!}

                                @if($errors->get('first_name'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('first_name') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-field">
                                <label for="last-name" class="form-label">Apellidos</label>
                                {!! Form::text('last_name', isset($user) ? $user->last_name : null, ['id' => 'last-name', 'class' => 'form-input', 'placeholder' => 'Apellidos']) !!}

                                @if($errors->get('last_name'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('last_name') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div id="staff-birth" class="form-field" style="display: none;">
                                <label for="birthdate" class="form-label">Fecha de nacimiento (formato: 1990-01-25)</label>
                                {!! Form::text('birthdate', isset($user) ? $user->birthdate->format('Y-m-d') : null, ['id' => 'birthdate', 'class' => 'form-input', 'placeholder' => '1990-01-25']) !!}

                                @if($errors->get('birthdate'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('birthdate') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div id="staff-email" class="form-field" style="display: none;">
                                <label for="email" class="form-label">Email</label>
                                {!! Form::text('email', isset($user) ? $user->email : null, ['id' => 'email', 'class' => 'form-input', 'placeholder' => 'ejemplo@email.com']) !!}

                                @if($errors->get('email'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div id="staff-branch-title" class="col-md-12" style="display: none;">
                            <h3 class="form-section">Asignar a una o varias empresas</h3>
                        </div>

                        <div id="staff-branches" class="col-md-12" style="display: none;">
                            <div class="v-preloading"></div>
                            <app-assign-companies :companies="{{ $companies }}" :branches="{{ $branches }}" :defaults="{{ isset($user) ? $user->staffing->load('company') : collect([]) }}"/>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
