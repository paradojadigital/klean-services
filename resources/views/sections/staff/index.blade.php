@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <form class="search-bar" method="GET" action="{{ action('StaffController@index') }}">
                <div class="form-search">
                    <i class="fa fa-search icon"></i>
                    <input id="search" type="text" class="form-input" name="name" placeholder="Buscar Integrante" value="{{ $name }}">
                </div>

                <div class="actions">
                    <a href="{{ route('staff.create') }}" class="action">
                        <i class="fa fa-plus icon"></i>
                        <span>Nuevo Intengrante</span>
                    </a>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="tabs md">
                <li class="tab-item" tab="#tab-admin">Admin</li>
                <li class="tab-item" tab="#tab-staff">Staff</li>
            </ul>

            <!-- Tab Admin : BEGIN -->
            <div id="tab-admin" class="table table-members tab-wrapper">
                <table>
                    @if ($admins->count())
                        <thead>
                            <tr>
                                <th class="col-user">Datos</th>
                                <th class="col-companies"></th>
                                <th class="col-actions"></th>
                            </tr>
                        </thead>
                    @endif

                    <tbody>
                        @foreach ($admins as $user)
                            <tr>
                                <td class="col-user">
                                    <div class="user">
                                        @if ($user->image)
                                            <div class="user-image">
                                                <img src="{{ $user->image }}" alt="{{ $user->name }}">
                                            </div>
                                        @else
                                            <div class="user-image" style="background-color: @strToHex($user->name)">
                                                <p>{{ $user->initials }}</p>
                                            </div>
                                        @endif

                                        <p class="user-name" title="Aquiles Esquivel Madrazo">
                                            {{ $user->name }}
                                        </p>
                                    </div>
                                </td>
                                <td class="col-companies"></td>
                                <td class="col-actions">
                                    <div class="menu-points">
                                        <div class="point"></div>
                                        <div class="point"></div>
                                        <div class="point"></div>

                                        <div class="menu-points-dropdown">
                                            <form id="del-user-{{ $user->id }}" action="{{ route('staff.destroy', ['user' => $user->id]) }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <a href="javascript:{}" onclick="document.getElementById('del-user-{{ $user->id }}').submit();" class="menu-action">Eliminar</a>
                                            </form>

                                            <a href="{{ route('staff.edit', ['user' => $user->id]) }}" class="menu-action">Editar</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @if (!$admins->count())
                    <div class="table-empty">
                        <p>Sin usuarios</p>
                    </div>
                @endif
            </div>
            <!-- Tab Admin : END -->

            <!-- Tab Staff : BEGIN -->
            <div id="tab-staff" class="table table-members tab-wrapper">
                <table>
                    @if ($staff->count())
                        <thead>
                            <tr>
                                <th class="col-user">Datos</th>
                                <th class="col-companies">Empresa asignada</th>
                                <th class="col-actions"></th>
                            </tr>
                        </thead>
                    @endif

                    <tbody>
                        @foreach ($staff as $user)
                            <tr>
                                <td class="col-user">
                                    <div class="user">
                                        @if ($user->image)
                                            <div class="user-image">
                                                <img src="{{ $user->image }}" alt="{{ $user->name }}">
                                            </div>
                                        @else
                                            <div class="user-image" style="background-color: @strToHex($user->name)">
                                                <p>{{ $user->initials }}</p>
                                            </div>
                                        @endif

                                        <p class="user-name" title="Aquiles Esquivel Madrazo">
                                            {{ $user->name }}
                                        </p>
                                    </div>
                                </td>
                                <td class="col-companies">{{ $user->staffing->pluck('name')->implode(', ') }}</td>
                                <td class="col-actions">
                                    <div class="menu-points">
                                        <div class="point"></div>
                                        <div class="point"></div>
                                        <div class="point"></div>

                                        <div class="menu-points-dropdown">
                                            <form id="del-user-{{ $user->id }}" action="{{ route('staff.destroy', ['user' => $user->id]) }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <a href="javascript:{}" onclick="document.getElementById('del-user-{{ $user->id }}').submit();" class="menu-action">Eliminar</a>
                                            </form>
                                            <a href="{{ route('staff.edit', ['user' => $user->id]) }}" class="menu-action">Editar</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @if (!$staff->count())
                    <div class="table-empty">
                        <p>Sin usuarios</p>
                    </div>
                @endif
            </div>
            <!-- Tab Staff : END -->
        </div>
    </div>
</div>
@endsection
