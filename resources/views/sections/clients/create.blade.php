@extends('containers.content')

@section('content')
<div class="container-fluid not-separated">
    <div class="row">
        <div class="save-bar">
            <a href="{{ route('companies.index') }}" class="btn btn-duo">Cancelar</a>
            <input type="button" class="btn btn-pri" value="Guardar" submit="#form-client">
        </div>

        <div class="col-lg-10 offset-lg-1 col-xl-6 offset-xl-3">
            <div class="wrapper">
                {!!
                    Form::model($company, [
                        'route' => $company->id ? ['companies.update', $company->id] : 'companies.store',
                        'method' => $company->id ? 'PATCH' : 'POST',
                        'id' => 'form-client',
                        'files' => true,
                        'no-btn'
                    ])
                !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-field form-field-image">
                                <label for="image" class="form-label">Logo</label>
                                <input id="image" type="file" class="form-input" name="image" autofocus>
                                <label for="image" class="form-file">
                                    <img src="{{ $company->image }}" alt="">
                                    <span class="form-file-message">
                                        <span>Arrastra tu imagen</span>
                                        <span>500 x 500</span>
                                    </span>
                                </label>

                                @if($errors->get('image'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('image') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-section"></div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-field">
                                <label for="name" class="form-label">Nombre del cliente</label>
                                {{ Form::text('name', null, ['id' => 'name', 'class' => 'form-input', 'placeholder' => 'Nombre del cliente']) }}

                                @if($errors->get('name'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('name') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h3 class="form-section">El cliente se encuentra en</h3>
                        </div>

                        @if (!$company->country)
                            <div class="col-md-12">
                                <div class="preloading-countries v-preloading"></div>
                                <app-countries :countries="{{ $countries }}"/>

                                @if($errors->get('country_id'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('country_id') }}
                                    </div>
                                @endif
                            </div>
                        @else
                            <div class="col-md-12">
                                <div class="row section-row">
                                    <div class="col-md-3">
                                        <a href="" class="country-card active">
                                            <div class="country-card-image">
                                                <img src="{{ $company->country->image }}" alt="{{ $company->country->name }}">
                                            </div>

                                            <div class="country-card-name">
                                                <p>{{ $company->country->name }}</p>
                                            </div>
                                        </a>
                                    </div>

                                    <input type="hidden" name="country_id" value="{{ $company->country->id }}">
                                </div>
                            </div>
                        @endif

                        <div class="col-md-12">
                            <div class="form-field">
                                <label for="business-name" class="form-label">Razón social</label>
                                {{ Form::text('business_name', null, ['id' => 'business-name', 'class' => 'form-input', 'placeholder' => 'Cliente S.A. de C.V.']) }}

                                @if($errors->get('business_name'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('business_name') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-field">
                                <label for="rfc" class="form-label">RFC / NIT</label>
                                {{ Form::text('tin', null, ['id' => 'rfc', 'class' => 'form-input', 'placeholder' => 'RFC / NIT']) }}

                                @if($errors->get('tin'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('tin') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h3 class="form-section">Dirección de facturación</h3>
                        </div>

                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="zipcode" class="form-label">Código Postal</label>
                                {{ Form::text('zipcode', null, ['id' => 'zipcode', 'class' => 'form-input', 'placeholder' => '44100']) }}

                                @if($errors->get('zipcode'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('zipcode') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="form-field">
                                <label for="suburb" class="form-label">Colonia</label>
                                {{ Form::text('neighborhood', null, ['id' => 'suburb', 'class' => 'form-input', 'placeholder' => 'Colonia']) }}
                                
                                @if($errors->get('neighborhood'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('neighborhood') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="street" class="form-label">Calle</label>
                                {{ Form::text('street', null, ['id' => 'street', 'class' => 'form-input', 'placeholder' => 'Calle']) }}

                                @if($errors->get('street'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('street') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="num-ext" class="form-label">N. Exterior</label>
                                {{ Form::text('number_ext', null, ['id' => 'num-ext', 'class' => 'form-input', 'placeholder' => 'XX']) }}

                                @if($errors->get('number_ext'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('number_ext') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="num-int" class="form-label">N. Interior</label>
                                {{ Form::text('number_int', null, ['id' => 'num-int', 'class' => 'form-input', 'placeholder' => 'XX']) }}

                                @if($errors->get('number_int'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('number_int') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-field">
                                <label for="city" class="form-label">Ciudad</label>
                                {{ Form::text('city', null, ['id' => 'city', 'class' => 'form-input', 'placeholder' => 'Ciudad']) }}

                                @if($errors->get('city'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('city') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-field">
                                <label for="state" class="form-label">Estado / Departamento</label>
                                {{ Form::text('state', null, ['id' => 'state', 'class' => 'form-input', 'placeholder' => 'Estado / Departamento']) }}

                                @if($errors->get('state'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('state') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
