@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <form class="search-bar" method="GET" action="{{ action('CompaniesController@index') }}">
                <div class="form-search">
                    <i class="fa fa-search icon"></i>
                    <input id="search" type="text" class="form-input" name="search" placeholder="Buscar Clientes" value="{{ $search }}">
                </div>

                <div class="actions">
                    <a href="{{ route('companies.create') }}" class="action">
                        <i class="fa fa-plus icon"></i>
                        <span>Nuevo Cliente</span>
                    </a>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <p class="counter clients-counter">{{ $companies->count() }} cliente{{ $companies->count() == 1 ? '' : 's' }}</p>
        </div>

        @foreach ($companies as $company)
            <div class="col-lg-4 col-xl-2">
                <a href="{{ route('branches.index', ['client' => $company->id]) }}" class="client-card">
                    <div class="client-card-logo">
                        <img src="{{ $company->image }}" alt="{{ $company->name }}">
                    </div>

                    <div class="client-card-name">
                        <p title="{{ $company->name }}">{{ $company->name }}</p>
                    </div>

                    @if ($company->orders_count)
                        <div class="client-card-status">
                            <p>{{ $company->orders_count }} pedido{{ $company->orders_count == 1 ? '' : 's' }} pendientes</p>
                        </div>
                    @else
                        <div class="client-card-status-empty">
                            <p>0 pedidos pendientes</p>
                        </div>
                    @endif
                </a>
            </div>
        @endforeach

        @if (!$companies->count())
            <div class="col-md-12">
                <p class="empty">Sin clientes.</p>
            </div>
        @endif
    </div>
</div>
@endsection
