@extends('containers.app')

@section('container')
<div class="link-account">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 offset-md-2">
                @if ($expired)
                    {!! Form::open(['route' => ['users.complete-profile', 'token' => $token], 'method' => 'PATCH']) !!}
                        <img src="/images/logo.png" alt="Central Work & Shop">

                        <h1>¡Bienvenido!</h1>

                        <p>Has sido invitado con el correo <strong> {{ $user->email }}</strong></p>
                        <p>Configura tu contraseña para tu cuenta</p>

                        <div class="form-field">
                            <label for="pass" class="form-label">Contraseña</label>
                            {!! Form::password('password', ['id' => 'pass', 'class' => 'form-input']) !!}

                            @if($errors->get('password'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-field">
                            <label for="pass-confirm" class="form-label">Confirmar contraseña</label>
                            {!! Form::password('password_confirmation', ['id' => 'pass-confirm', 'class' => 'form-input']) !!}

                            @if($errors->get('password_confirmation'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('password_confirmation') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-field">
                            <input type="submit" class="btn btn-pri" value="Continuar">
                        </div>
                    </form>
                @else
                    <h1>¡El acceso ha expirado!</h1>
                    <p>Has tardado demasiado en acceder o el link ya fue usado.</p>
                    <p>Para completar tu acceso, ponte en contacto con tu administrador.</p>
                @endif
            </div>

            <div class="col-md-6 offset-md-1">
                <div class="link-account-banner">
                    <!-- banner here. -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
