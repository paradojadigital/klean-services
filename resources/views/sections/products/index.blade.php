@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <form class="search-bar" method="GET" action="{{ action('ProductsController@index') }}">
                <div class="form-search">
                    <i class="fa fa-search icon"></i>
                    <input id="search" type="text" class="form-input" name="name" placeholder="Buscar Productos" value="{{ $name }}">
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="filter-bar">
                <div class="form-field form-field-inline">
                    <label for="filter" class="form-label">Filtrar por</label>

                    <form id="search-bar" class="form-select" method="GET" action="{{ action('ProductsController@index') }}">
                        <select id="filter" class="form-input" name="category" select-form="#search-bar">
                            <option value="" selected>Todas las categorias</option>
                            @foreach ($categories as $category)
                            <option value="{{ $category->id }}"
                                {{ $selected == $category->id ? 'selected' : '' }}>
                                {{ $category->name }}
                            </option>
                            @endforeach
                        </select>

                        <i class="fa fa-caret-down caret"></i>
                    </form>
                </div>

                <div class="actions">
                    <a href="{{ route('products.create') }}" class="action">
                        <i class="fa fa-plus icon"></i>
                        <span>Nuevo Producto</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="table table-products">
                <table>
                    @if ($products->count())
                    <thead>
                        <tr>
                            <th class="col-image"></th>
                            <th class="col-name">Producto</th>
                            <th class="col-description">Descripción</th>
                            <th class="col-category">Categoria</th>
                            <th class="col-unit-price">Precio MXN</th>
                            <th class="col-unit-price">Precio COL</th>
                            <th class="col-visible">Visible</th>
                        </tr>
                    </thead>
                    @endif

                    <tbody>
                        @foreach ($products as $product)
                        <tr>
                            <td class="col-image">
                                <div class="product-image">
                                    <img src="{{ $product->image }}" alt="{{ $product->name }}">
                                </div>
                            </td>
                            <td class="col-product"><a href="{{ route('products.show', ['product' => $product->id]) }}">{{ $product->name }}</a></td>
                            <td class="col-description">{{ $product->description }}</td>
                            <td class="col-category">{{ $product->category->name }}</td>
                            <td class="col-unit-price">{{
                                $product->countries->where('currency_code', 'MXN')->count() ?
                                '$'.number_format($product->countries->where('currency_code', 'MXN')->first()->pivot->price, 2) :
                                '-'
                            }}</td>
                            <td class="col-unit-price">{{
                                $product->countries->where('currency_code', 'COP')->count() ?
                                '$'.number_format($product->countries->where('currency_code', 'COP')->first()->pivot->price, 2) :
                                '-'
                            }}</td>
                            <td class="col-visible">
                                <app-visible-checkbox
                                    url="{{ route('products.update', ['product' => $product->id]) }}"
                                    product_id="{{ $product->id }}"
                                    :product_visible="{{ $product->visible }}"
                                />
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <!-- AWESOME! https://github.com/laravel/framework/issues/19441#issuecomment-305491111  -->
                {{ $products->appends(request()->except('page'))->links() }}

                @if (!$products->count())
                <div class="table-empty">
                    <p>Sin productos.</p>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
