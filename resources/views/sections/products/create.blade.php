@extends('containers.content')

@section('content')
<div class="container-fluid not-separated">
    <div class="row">
        <div class="save-bar">
            <a href="{{ route('products.index') }}" class="btn btn-duo">Cancelar</a>
            <input type="button" class="btn btn-pri" value="Guardar" submit="#form-product">
        </div>

        <div class="col-md-12">
            {!! Form::model($product, ['route' => ($product->id ? ['products.show', $product->id] : 'products.store'), 'id' => 'form-product', 'method' => ($product->id ? 'PUT' : 'POST'), 'files' => true, 'no-btn', 'class' => 'row']) !!}
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-7 col-xl-6 offset-xl-1">
                            <div class="wrapper">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-field">
                                            <label for="name" class="form-label">Nombre</label>
                                            {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-input', 'placeholder' => 'Garrafón Grande', 'autofocus' => true]) !!}

                                            @if($errors->get('name'))
                                            <div class="alert alert-danger">
                                                {{ $errors->first('name') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-field">
                                            <label for="description" class="form-label">Descripcion</label>
                                            {!! Form::textarea('description', null, ['id' => 'description', 'class' => 'form-input', 'placeholder' => 'Breve descripción del garrafón...']) !!}

                                            @if($errors->get('description'))
                                            <div class="alert alert-danger">
                                                {{ $errors->first('description') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xl-5">
                                        <div class="form-field form-field-image">
                                            <label for="image" class="form-label">Imagen</label>
                                            {!! Form::file('image', ['id' => 'image', 'class' => 'form-input']) !!}
                                            <label for="image" class="form-file not-rounded">
                                                <img src="{{ $product->image }}" alt="">
                                                <span class="form-file-message">
                                                    <span>Arrastra tu imagen</span>
                                                    <span>500 x 500</span>
                                                </span>
                                            </label>

                                            @if($errors->get('image'))
                                            <div class="alert alert-danger">
                                                {{ $errors->first('image') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xl-7">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-field">
                                                    <label for="unit-price-mxn" class="form-label">Precio MXN</label>
                                                    {{
                                                        Form::number('price_mxn',
                                                            $product->prices()->where('currency_code', 'MXN')->first() ?
                                                                $product->prices()->where('currency_code', 'MXN')->first()->pivot->price :
                                                                null,
                                                            [
                                                                'id' => 'unit-price-mxn',
                                                                'class' => 'form-input',
                                                                'placeholder' => '$500',
                                                            ]
                                                        )
                                                    }}
                                                    

                                                    @if($errors->get('price_mxn'))
                                                    <div class="alert alert-danger">
                                                        {{ $errors->first('price_mxn') }}
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-field">
                                                    <label for="unit-price-cop" class="form-label">Precio COL</label>
                                                    {{
                                                        Form::number('price_cop',
                                                            $product->prices()->where('currency_code', 'COP')->first() ?
                                                                $product->prices()->where('currency_code', 'COP')->first()->pivot->price :
                                                                null,
                                                            [
                                                                'id' => 'unit-price-cop',
                                                                'class' => 'form-input',
                                                                'placeholder' => '$75,000',
                                                            ]
                                                        )
                                                    }}

                                                    @if($errors->get('price_cop'))
                                                    <div class="alert alert-danger">
                                                        {{ $errors->first('price_cop') }}
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-field">
                                                    <label for="category" class="form-label">Categoría</label>
                                                    <div class="form-select">
                                                        {{
                                                            Form::select(
                                                                'category_id',
                                                                App\Category::get()->pluck('name', 'id')->prepend('Selecciona la categoria', 0),
                                                                null,
                                                                [
                                                                    'id' => 'category',
                                                                    'class' => 'form-input'
                                                                ]
                                                            )
                                                        }}

                                                        <i class="fa fa-caret-down caret"></i>
                                                    </div>
                                                    @if($errors->get('category_id'))
                                                    <div class="alert alert-danger">
                                                        {{ $errors->first('category_id') }}
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-5 col-xl-4">
                            <div class="v-preloading"></div>
                            <app-visible-company :companies='{!! $companies !!}' :default_selected_companies='{!! $product->hiddenOnCompanies()->with(['country'])->get() !!}'/>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
