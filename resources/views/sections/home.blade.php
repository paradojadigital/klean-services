@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="preloading-chart v-preloading"></div>
            <app-chart
                class="dashboard-card"
                :title="'Pedidos al dia'"
                :filters="true"
                :entries='[{
                    name: "Mexico",
                    backgroundColor: "rgba(13, 117, 5, 0.2)",
                    borderColor: "rgb(13, 117, 5)",
                    data: {!! json_encode($chart_data["mx"]) !!}
                },{
                    name: "Colombia",
                    backgroundColor: "rgba(248, 209, 28, 0.2)",
                    borderColor: "rgb(248, 209, 28)",
                    data: {!! json_encode($chart_data["co"]) !!}
                }]'/>
        </div>

        <div class="col-xl-6">
            <div class="dashboard-card">
                <div class="card-header">
                    <h3>Últimos Pedidos</h3>
                </div>

                <div class="table table-orders">
                    <table>
                        @if ($orders->count())
                            <thead>
                                <tr>
                                    <th class="col-id">Nº</th>
                                    <th class="col-date">Fecha</th>
                                    <th class="col-status">Status</th>
                                    <th class="col-company">Empresa</th>
                                    <th class="col-author">Realizado por</th>
                                </tr>
                            </thead>
                        @endif

                        <tbody>
                            @foreach ($orders as $order)
                            <tr>
                                <td class="col-id">
                                    <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->id }}</a>
                                </td>
                                <td class="col-date">
                                    <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->created_at->format('d M Y H:m:s') }}</a>
                                </td>
                                <td class="col-status">
                                    <a href="{{ route('orders.show', ['order' => $order->id]) }}">
                                        <div class="status-tag {{ $order->status_class }}">{{ __('app.status.'.$order->status) }}</div>
                                    </a>
                                </td>
                                <td class="col-company">
                                    <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->company->name }}</a>
                                </td>
                                <td class="col-author">
                                    <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->user->name }}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @if (!$orders->count())
                        <div class="table-empty">
                            <p>Sin últimos pedidos</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-xl-6">
            <div class="dashboard-card">
                <div class="card-header header-tabs">
                    <h3>Top Productos</h3>
                    <ul class="top-products-tab">
                        <li class="products-tab" tab="#MX">Mexico</li>
                        <li class="products-tab" tab="#CO">Colombia</li>
                    </ul>
                </div>

                <div class="v-preloading preloading-top-products"></div>

                @foreach ($country_top_products as $key => $country)
                    <div id="{{ $key }}" class="table table-top-products tab-wrapper">
                        <table>
                            @if (count($country))
                                <thead>
                                    <tr>
                                        <th class="col-id">Nº</th>
                                        <th class="col-image"></th>
                                        <th class="col-name">Nombre</th>
                                        <th class="col-name">Ordenes</th>
                                    </tr>
                                </thead>
                            @endif

                            <tbody>
                                @foreach ($country as $product)
                                    <tr>
                                        <td class="col-id">{{ $product['detail']->id }}</td>
                                        <td class="col-image">
                                            <div class="product-image">
                                                <img src="{{ $product['detail']->image }}" alt="">
                                            </div>
                                        </td>
                                        <td class="col-name">{{ $product['detail']->name }}</td>
                                        <td>{{ $product['orders_count'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        @if (!count($country))
                            <div class="table-empty">
                                <p>Sin top productos</p>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>

        <div class="col-md-12">
            <div class="dashboard-card">
                <div class="card-header">
                    <h3>Ventas por paises</h3>
                </div>

                <div class="card-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="map" map="{{
                                collect([['MEX', $mx_orders_count], ['COL', $co_orders_count]])
                            }}"></div>
                        </div>

                        <div class="col-md-6">
                            <div class="table table-countries">
                                <table>
                                    <thead>
                                        <tr>
                                            <th class="col-id">Nº</th>
                                            <th class="col-country">País</th>
                                            <th class="col-orders">Pedidos</th>
                                            <th class="col-total">Total</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td class="col-id">1</td>
                                            <td class="col-country">México</td>
                                            <td class="col-orders">{{ $mx_orders_count }}</td>
                                            <td class="col-total">${{ number_format($mx_total->income, 2) }}</td>
                                        </tr>

                                        <tr>
                                            <td class="col-id">2</td>
                                            <td class="col-country">Colombia</td>
                                            <td class="col-orders">{{ $co_orders_count }}</td>
                                            <td class="col-total">${{ number_format($co_total->income, 2) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
