@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="filter-report-bar admin-report">
                <form method="GET" action="{{ action('ReportsController@index') }}">
                    <div class="row align-items-end">
                        <div class="filter-report-col">
                            <p class="filter-title">Filtrar por</p>
                        </div>

                        <div class="filter-report-col">
                            <div class="form-field">
                                <label for="from" class="form-label">Desde</label>
                                <input id="from" type="text" class="form-input" name="from" data-default="{{ $from }}" calendar="from">
                            </div>
                        </div>

                        <div class="filter-report-col">
                            <div class="form-field">
                                <label for="to" class="form-label">Hasta</label>
                                <input id="to" type="text" class="form-input" name="to" data-default="{{ $to }}" calendar="to">
                            </div>
                        </div>

                        <!-- <div class="filter-report-col">
                            <div class="form-field">
                                <label for="type" class="form-label">Tipo</label>
                                <div class="form-select">
                                    <select id="type" class="form-input" name="type">
                                        <option value="" selected>Selecciona el tipo</option>
                                    </select>

                                    <i class="fa fa-caret-down caret"></i>
                                </div>
                            </div>
                        </div> -->

                        <div class="multiple-filter-report-col">
                            <app-company-branch
                                company_id="{{ $company_id }}"
                                branch_id="{{ $branch_id }}"
                                :companies="{{ $companies }}"
                                :branches="{{ $branches }}"
                            />
                        </div>

                        <div class="filter-report-col">
                            <div class="form-field">
                                <input type="submit" class="btn btn-pri" value="Filtrar">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-12">
            <div class="quick-reports quick-summary-cards">
                <div class="dashboard-card">
                    @if ($total->count())
                        <div class="card-content">
                            @if ($total->has('name'))
                                <div class="field">
                                    <div class="field-label">{{ $total['name'] }}</div>
                                    <div class="field-value">${{ number_format($total['income'], 2) }}</div>
                                </div>
                            @else
                                @foreach ($total as $total_branch)
                                    <div class="field">
                                        <div class="field-label">{{ $total_branch['name'] }}</div>
                                        <div class="field-value">${{ number_format($total_branch['income'], 2) }}</div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="preloading-chart-reports v-preloading"></div>
            <app-chart
                class="report-card"
                tag="client.orders"
                from="{{ $from }}"
                to="{{ $to }}"
                :entries="{{ json_encode($chart_data) }}"/>
        </div>

        <div class="col-md-12">
           <div class="table table-report-orders">
                <table>
                    @if ($orders->count())
                    <thead>
                        <tr>
                            <th class="col-id">Nº</th>
                            <th class="col-date">Fecha</th>
                            <th class="col-status">Status</th>
                            <th class="col-company">Empresa</th>
                            <th class="col-author">Realizado por</th>
                        </tr>
                    </thead>
                    @endif

                    <tbody>
                        @if ($branch_id === 'all')
                            @foreach ($orders as $branch)
                                @foreach ($branch as $order)
                                    <tr>
                                        <td class="col-id">
                                            <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->id }}</a>
                                        </td>
                                        <td class="col-date">
                                            <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->created_at->format('d M Y - h:m a') }}</a>
                                        </td>
                                        <td class="col-status">
                                            <a href="{{ route('orders.show', ['order' => $order->id]) }}">
                                                <div class="status-tag {{ $order->status_class }}">{{ __('app.status.'.$order->status) }}</div>
                                            </a>
                                        </td>
                                        <td class="col-company">
                                            <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->company->name }}</a>
                                        </td>
                                        <td class="col-author">
                                            <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->user->name }}</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @else
                            @foreach ($orders as $order)
                                <tr>
                                    <td class="col-id">
                                        <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->id }}</a>
                                    </td>
                                    <td class="col-date">
                                        <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->created_at->format('d M Y - h:m a') }}</a>
                                    </td>
                                    <td class="col-status">
                                        <a href="{{ route('orders.show', ['order' => $order->id]) }}">
                                            <div class="status-tag {{ $order->status_class }}">{{ __('app.status.'.$order->status) }}</div>
                                        </a>
                                    </td>
                                    <td class="col-company">
                                        <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->company->name }}</a>
                                    </td>
                                    <td class="col-author">
                                        <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->user->name }}</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>

                @if (!$orders->count())
                <div class="table-empty">
                    <p>Sin ordenes en este edificio.</p>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
