@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="client-bar">
                <div class="client-bar-left">
                    <div class="client-details">
                        <div class="client-image">
                            <img src="{{ $company->image }}" alt="{{ $company->name }}">
                        </div>

                        <div class="client-name">
                            <p>{{ $branch->name }}</p>
                        </div>
                    </div>

                    <div class="client-section">
                        <p>Pedidos</p>
                    </div>

                    <form class="form-search">
                        <i class="fa fa-search icon"></i>
                        <input id="search" type="text" class="form-input" name="id" placeholder="Buscar Pedidos" value="{{ $id }}">
                    </form>
                </div>

                <div class="client-bar-right">
                    <div class="actions">
                        <a href="{{ route('branches.members', [$company->id, $branch->id]) }}" class="action">
                            <i class="fa fa-user icon"></i>
                            <span>Ver Integrantes</span>
                        </a>

                        <a href="{{ route('branches.edit', [$company->id, $branch->id]) }}" class="action">
                            <i class="fa fa-pencil icon"></i>
                            <span>Editar Edificio</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="tabs md">
                <li class="tab-item {{ $status == null ? 'active' : '' }}">
                    <a href="{{ route('branches.orders', ['company' => $company->id, 'branch' => $branch->id]) }}">Todas</a>
                </li>
                
                <li class="tab-item {{ $status == App\Order::STATUS_PENDING ? 'active' : '' }}">
                    <a href="{{ route('branches.orders', ['company' => $company->id, 'branch' => $branch->id, 'status' => App\Order::STATUS_PENDING]) }}">
                        Pendientes ({{ $branch->orders->where('status', App\Order::STATUS_PENDING)->count() }})
                    </a>
                </li>
                
                <li class="tab-item {{ $status == App\Order::STATUS_COMPLETED ? 'active' : '' }}">
                    <a href="{{ route('branches.orders', ['company' => $company->id, 'branch' => $branch->id, 'status' => App\Order::STATUS_COMPLETED]) }}">
                        Completadas ({{ $branch->orders->where('status', App\Order::STATUS_COMPLETED)->count() }})
                    </a>
                </li>

                <li class="tab-item {{ $status == App\Order::STATUS_DELIVERED ? 'active' : '' }}">
                    <a href="{{ route('branches.orders', ['company' => $company->id, 'branch' => $branch->id, 'status' => App\Order::STATUS_DELIVERED]) }}">
                        Enviados ({{ $branch->orders->where('status', App\Order::STATUS_DELIVERED)->count() }})
                    </a>
                </li>

                <li class="tab-item {{ $status == App\Order::STATUS_PAYED ? 'active' : '' }}">
                    <a href="{{ route('branches.orders', ['company' => $company->id, 'branch' => $branch->id, 'status' => App\Order::STATUS_PAYED]) }}">
                        Pagados ({{ $branch->orders->where('status', App\Order::STATUS_PAYED)->count() }})
                    </a>
                </li>

                <li class="tab-item {{ $status == App\Order::STATUS_NEW ? 'active' : '' }}">
                    <a href="{{ route('branches.orders', ['company' => $company->id, 'branch' => $branch->id, 'status' => App\Order::STATUS_NEW]) }}">
                        Nuevos ({{ $branch->orders->where('status', App\Order::STATUS_NEW)->count() }})
                    </a>
                </li>
            </ul>

            <div class="table table-building-orders">
                <table>
                    <thead>
                        @if ($orders->count())
                            <tr>
                                <th class="col-id">Nº de Pedido</th>
                                <th class="col-date">Fecha</th>
                                <th class="col-status">Status</th>
                                <th class="col-author">Realizado por</th>
                            </tr>
                        @endif
                    </thead>

                    <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td class="col-id">
                                    <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->id }}</a>
                                </td>
                                <td class="col-date">
                                    <a title="{{ $order->created_at->format('h:m:s A') }}" href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->created_at->format('d M Y') }}</a>
                                </td>
                                <td class="col-status">
                                    <a href="{{ route('orders.show', ['order' => $order->id]) }}">
                                        <div class="status-tag {{ $order->status_class }}">{{ $order->status }}</div>
                                    </a>
                                </td>
                                <td class="col-author">
                                    <a href="{{ route('orders.show', ['order' => $order->id]) }}">{{ $order->user->name }}</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $orders->links() }}

                @if (!$orders->count())
                    <div class="table-empty">
                        <p>Sin pedidos</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
