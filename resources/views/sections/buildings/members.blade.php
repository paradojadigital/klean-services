@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="client-bar">
                <div class="client-bar-left">
                    <div class="client-details">
                        <div class="client-image">
                            <img src="{{ $company->image }}" alt="{{ $company->name }}">
                        </div>

                        <div class="client-name">
                            <p>{{ $branch->name }}</p>
                        </div>
                    </div>

                    <div class="client-section">
                        <p>Integrantes</p>
                    </div>

                    <form class="form-search">
                        <i class="fa fa-search icon"></i>
                        <input id="search" type="text" class="form-input" name="name" placeholder="Buscar Integrantes" value="{{ $name }}">
                    </form>
                </div>

                <div class="client-bar-right">
                    <div class="actions">
                        <a href="{{ route('branches.orders', [$company->id, $branch->id]) }}" class="action">
                            <i class="fa fa-paste icon"></i>
                            <span>Ver Pedidos</span>
                        </a>

                        <a href="{{ route('branches.edit', [$company->id, $branch->id]) }}" class="action">
                            <i class="fa fa-pencil icon"></i>
                            <span>Editar Edificio</span>
                        </a>

                        <a href="" class="action" modal="#modal-building-members">
                            <i class="fa fa-user icon"></i>
                            <span>Añadir Integrante</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="table table-building-members">
                <table>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td class="col-user">
                                    <div class="user">
                                        @if ($user->image)
                                            <div class="user-image">
                                                <img src="{{ $user->image }}" alt="{{ $user->name }}">
                                            </div>
                                        @else
                                            <div class="user-image" style="background-color: @strToHex($user->name)">
                                                <p>{{ $user->initials }}</p>
                                            </div>
                                        @endif

                                        <p class="user-name" title="{{ $user->name }}">{{ $user->name }}</p>
                                    </div>
                                </td>
                                <td class="col-rol">{{ $user->pivot->role ?? 'personal de limpieza' }}</td>
                                <td class="col-actions">
                                    @if ($user->pivot->role)
                                        <div class="menu-points">
                                            <div class="point"></div>
                                            <div class="point"></div>
                                            <div class="point"></div>

                                            <div class="menu-points-dropdown">
                                                <form method="POST" action="{{ route('branches.staff.destroy', [
                                                    'company' => $company->id,
                                                    'branch' => $branch->id,
                                                    'user' => $user->id
                                                ]) }}">
                                                    {{ csrf_field() }}
                                                    <input type="submit" class="menu-action" value="Eliminar">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                </form>

                                                <a data="{{ $user }}"
                                                    class="menu-action"
                                                    modal="#modal-building-members">
                                                    Editar
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @if (!$users->count())
                    <div class="table-empty">
                        <p>Sin usuarios.</p>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- # modals. -->
    <div class="modal-umbrella {{ session('modal') ? 'show' : '' }}">
        @include('components.modal-building-member')
    </div>
</div>
@endsection
