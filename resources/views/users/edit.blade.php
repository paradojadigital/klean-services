@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="wrapper">
                {{ Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PATCH', 'files' => true]) }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-field form-field-image">
                                <label for="image" class="form-label">Logo</label>
                                <input id="image" type="file" class="form-input" name="image" autofocus>
                                <label for="image" class="form-file">
                                    @if ($user)
                                        <img src="{{ $user->image }}" alt="{{ $user->name }}">
                                    @else
                                        <img src="" alt="">
                                    @endif
                                    <span class="form-file-message">
                                        <span>Arrastra tu imagen</span>
                                        <span>500 x 500</span>
                                    </span>
                                </label>

                                @if($errors->get('image'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('image') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-section"></div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-field">
                                <label for="first-name" class="form-label">Nombre</label>
                                {!! Form::text('first_name', $user->first_name, ['id' => 'first-name', 'class' => 'form-input', 'placeholder' => 'Nombres']) !!}

                                @if($errors->get('first_name'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('first_name') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-field">
                                <label for="last-name" class="form-label">Apellidos</label>
                                {!! Form::text('last_name', $user->last_name, ['id' => 'last-name', 'class' => 'form-input', 'placeholder' => 'Apellidos']) !!}

                                @if($errors->get('last_name'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('last_name') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-field">
                                <label for="email" class="form-label">Correo electrónico</label>
                                {!! Form::text('email', $user->email, ['id' => 'email', 'class' => 'form-input', 'placeholder' => 'ejemplo@email.com', 'readonly']) !!}

                                @if($errors->get('email'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-lg-5 offset-lg-7 col-xl-4 offset-xl-8">
                            <div class="form-field">
                                <input type="submit" class="btn btn-pri" value="Guardar">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-section"></div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="password" class="form-label">Contraseña</label>
                                {!! Form::password('password', ['id' => 'password', 'class' => 'form-input', 'placeholder' => '******', 'autocomplete' => 'off']) !!}

                                @if($errors->get('password'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="new-password" class="form-label">Nueva contraseña</label>
                                {!! Form::password('new_password', ['id' => 'new-password', 'class' => 'form-input', 'placeholder' => '******', 'autocomplete' => 'off']) !!}

                                @if($errors->get('new_password'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('new_password') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="confirm-password" class="form-label">Confirmar contraseña</label>
                                {!! Form::password('new_password_confirmation', ['id' => 'confirm-password', 'class' => 'form-input', 'placeholder' => '******', 'autocomplete' => 'off']) !!}

                                @if($errors->get('new_password_confirmation'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('new_password_confirmation') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-lg-5 offset-lg-7 col-xl-4 offset-xl-8">
                            <div class="form-field">
                                <input type="submit" class="btn btn-pri" value="Guardar">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
