@extends('containers.app')

@section('container')
<div class="lr-section">
    <div class="lr-banner">
        <!-- banner here. -->
    </div>

    <div class="lr-form">
        <form method="POST" action="{{ route('login') }}">
            <div class="row">
                {{ csrf_field() }}

                <div class="col-md-12">
                    <div class="logo">
                        <img src="/images/logo.png" alt="Central Work & Shop">
                    </div>
                </div>

                <div class="col-md-12">
                    <h3 class="welcome">¡Hola!</h3>
                    <p class="welcome-legend">Bienvenido a Central Work & Shop</p>
                </div>

                <div class="col-md-12">
                    <div class="form-field {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label for="email" class="form-label">Correo Electronico</label>
                        <input id="email" type="email" class="form-input" name="email" value="{{ old('email') }}" placeholder="ejemplo@email.com" autofocus>

                        @if($errors->get('email'))
                            <div class="alert alert-danger">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-field {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label for="password" class="form-label">Contraseña</label>
                        <input id="password" type="password" class="form-input" name="password" placeholder="Mi contraseña">

                        @if($errors->get('password'))
                            <div class="alert alert-danger">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-field">
                        <!-- <input type="submit" class="btn btn-pri" value="Iniciar sesión"> -->
                        <input type="submit" class="btn btn-pri" value="Iniciar sesión">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
