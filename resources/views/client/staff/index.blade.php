@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <form class="search-bar" method="GET" action="">
                <div class="form-search">
                    <i class="fa fa-search icon"></i>
                    <input id="search" type="text" class="form-input" name="name" placeholder="Buscar Integrantes" value="{{ $name }}">
                </div>

                <div class="actions">
                    <a href="#" class="action" modal="#modal-building-members">
                        <i class="fa fa-plus icon"></i>
                        <span>Añadir Integrante</span>
                    </a>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="table table-staff">
                <table>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td class="col-user">
                                <div class="user">
                                    @if ($user->image)
                                    <div class="user-image">
                                        <img src="{{ $user->image }}" alt="{{ $user->name }}">
                                    </div>
                                    @else
                                    <div class="user-image" style="background-color: @strToHex($user->name)">
                                        <p>{{ $user->initials }}</p>
                                    </div>
                                    @endif
                                    
                                    <p class="user-name" title="Aquiles Esquivel Madrazo">
                                        {{ $user->name }}
                                    </p>
                                </div>
                            </td>
                            <td class="col-rol">{{ $user->pivot->role ?? 'personal de limpieza' }}</td>
                            <td class="col-actions">
                                @if ($user->pivot->role)
                                    <div class="menu-points">
                                        <div class="point"></div>
                                        <div class="point"></div>
                                        <div class="point"></div>

                                        <div class="menu-points-dropdown">
                                            <form method="POST" action="{{ route('branches.staff.destroy', [$company->id, $branch->id, $user->id]) }}">
                                                {{ csrf_field() }}
                                                <input type="submit" class="menu-action" value="Eliminar">
                                                <input type="hidden" name="_method" value="DELETE">
                                            </form>
                                            
                                            <a data="{{ $user }}"
                                                class="menu-action"
                                                modal="#modal-building-members">
                                                Editar
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                @if (!$users->count())
                <div class="table-empty">
                    <p>Este edificio no tiene usuarios.</p>
                </div>
                @endif
            </div>
        </div>
    </div>

    <!-- # modals. -->
    <div class="modal-umbrella {{ session('modal') ? 'show' : '' }}">
        @include('components.modal-building-member')
    </div>
</div>
@endsection
