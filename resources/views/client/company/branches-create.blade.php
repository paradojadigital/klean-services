@extends('containers.content')

@section('content')
<div class="container-fluid not-separated">
    <div class="row">
        <div class="save-bar">
            <a href="{{ route('client.company.branches', $company->id) }}" class="btn btn-duo">Cancelar</a>
            <input type="button" class="btn btn-pri" value="Guardar" submit="#form-building">
        </div>

        <div class="col-lg-10 offset-lg-1 col-xl-6 offset-xl-3">
            <div class="wrapper">
                {!!
                    Form::model($branch, [
                        'route' => $branch->id ? ['client.company.branches.update', $company->id, $branch->id] : ['client.company.branches.store', $company->id],
                        'method' => $branch->id ? 'PATCH' : 'POST',
                        'id' => 'form-building',
                        'files' => true,
                        'no-btn'
                    ])
                !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-field">
                                <label for="name" class="form-label">Nombre del edificio a mostrar</label>
                                {{ Form::text('name', null, ['id' => 'name', 'class' => 'form-input', 'placeholder' => 'Torre Bugambilias', 'autofocus']) }}

                                @if($errors->get('name'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('name') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-field">
                                <label for="phone" class="form-label">Telefono</label>
                                {{ Form::text('phone', null, ['id' => 'phone', 'class' => 'form-input', 'placeholder' => '33 12345678']) }}

                                @if($errors->get('phone'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('phone') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h3 class="form-section">Dirección</h3>
                        </div>

                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="zipcode" class="form-label">Código Postal</label>
                                {{ Form::text('zipcode', null, ['id' => 'zipcode', 'class' => 'form-input', 'placeholder' => '11000']) }}

                                @if($errors->get('zipcode'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('zipcode') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="form-field">
                                <label for="suburb" class="form-label">Colonia</label>
                                {{ Form::text('neighborhood', null, ['id' => 'suburb', 'class' => 'form-input', 'placeholder' => 'Colonia']) }}
                                
                                @if($errors->get('neighborhood'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('neighborhood') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="street" class="form-label">Calle</label>
                                {{ Form::text('street', null, ['id' => 'street', 'class' => 'form-input', 'placeholder' => 'Calle']) }}
                                
                                @if($errors->get('street'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('street') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="num-ext" class="form-label">N. Exterior</label>
                                {{ Form::text('number_ext', null, ['id' => 'num-ext', 'class' => 'form-input', 'placeholder' => 'XX']) }}
                                
                                @if($errors->get('number_ext'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('number_ext') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-field">
                                <label for="num-int" class="form-label">N. Interior</label>
                                {{ Form::text('num_int', null, ['id' => 'num-int', 'class' => 'form-input', 'placeholder' => 'XX']) }}
                                
                                @if($errors->get('num_int'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('num_int') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-field">
                                <label for="city" class="form-label">Ciudad</label>
                                {{ Form::text('city', null, ['id' => 'city', 'class' => 'form-input', 'placeholder' => 'Ciudad']) }}
                                
                                @if($errors->get('city'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('city') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-field">
                                <label for="state" class="form-label">Estado / Departamento</label>
                                {{ Form::text('state', null, ['id' => 'state', 'class' => 'form-input', 'placeholder' => 'Estado / Departamento']) }}
                                
                                @if($errors->get('state'))
                                    <div class="alert alert-danger">
                                        {{ $errors->first('state') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
