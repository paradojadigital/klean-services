@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="client-bar">
                <div class="client-details">
                    <div class="client-image">
                        <img src="{{ $company->image }}" alt="">
                    </div>

                    <div class="client-name">
                        <p>{{ $company->name }}</p>
                    </div>
                </div>

                <div class="actions">
                    <a href="{{ route('client.company.admins', ['company' => $company->id]) }}" class="action">
                        <i class="fa fa-user icon"></i>
                        <span>Ver Admins</span>
                    </a>

                    <a href="{{ route('client.company.branches.create', ['company' => $company->id]) }}" class="action">
                        <i class="fa fa-plus icon"></i>
                        <span>Nuevo Edificio</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach ($branches as $branch)
        <div class="col-lg-4 col-xl-2">
            <a href="{{ route('client.home', [$branch->id]) }}" class="building-card">
                <div class="building-card-name">
                    <p>{{ $branch->name }}</p>
                </div>

                @if ($branch->orders_count)
                    <div class="building-card-status">
                        <p>{{ $branch->orders_count }} Pedidos pendientes</p>
                    </div>
                @else
                    <div class="building-card-status-empty">
                        <p>0 Pedidos pendientes</p>
                    </div>
                @endif
            </a>
        </div>
        @endforeach

        @if (!$branches->count())
        <div class="col-md-12">
            <p class="empty">Sin edificios.</p>
        </div>
        @endif
    </div>
</div>
@endsection
