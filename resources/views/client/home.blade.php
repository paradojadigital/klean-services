@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="quick-summary-cards">
                <div class="dashboard-card">
                    <div class="card-content">
                        <div class="field">
                            <div class="field-label">Total de compras</div>
                            <div class="field-value">${{ number_format($total, 2) }}</div>
                        </div>

                        <div class="field">
                            <div class="field-label">Este mes</div>
                            <div class="field-value">${{ number_format($total_month, 2) }}</div>
                        </div>

                        <div class="field">
                            <div class="field-label">Esta semana</div>
                            <div class="field-value">${{ number_format($total_week, 2) }}</div>
                        </div>
                    </div>
                </div>

                <div class="dashboard-card">
                    <div class="card-content">
                        <div class="field">
                            <div class="field-label">Total de pedidos</div>
                            <div class="field-value">{{ $orders_count }}</div>
                        </div>

                        <div class="field">
                            <div class="field-label">Este mes</div>
                            <div class="field-value">{{ $orders_count_month }}</div>
                        </div>

                        <div class="field">
                            <div class="field-label">Esta semana</div>
                            <div class="field-value">{{ $orders_count_week }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="preloading-chart v-preloading"></div>
            <app-chart
                class="dashboard-card"
                :title="'Pedidos al dia'"
                :filters="true"
                tag="client.orders"
                :entries='[{
                    name: "{{ $currentCompany()->country->name }}",
                    backgroundColor: "rgba(56, 91, 232, 0.2)",
                    borderColor: "rgb(56, 91, 232)",
                    data: {!! collect($chart_data) !!}
                }]'/>
        </div>

        <div class="col-md-12">
            <div class="preloading-chart v-preloading"></div>
            <app-chart
                class="dashboard-card"
                :title="'Dinero'"
                :filters="true"
                tag="client.income"
                :entries='[{
                    name: "Dinero",
                    backgroundColor: "rgba(56, 91, 232, 0.2)",
                    borderColor: "rgb(56, 91, 232)",
                    data: {!! collect($chart_data) !!}
                }]'/>
        </div>

        <div class="col-xl-6">
            <div class="dashboard-card">
                <div class="card-header">
                    <h3>Últimos Pedidos</h3>
                </div>

                <div class="table table-orders-client">
                    <table>
                        @if ($orders->count())
                            <thead>
                                <tr>
                                    <th class="col-id">Nº</th>
                                    <th class="col-date">Fecha</th>
                                    <th class="col-status">Status</th>
                                    <th class="col-author">Realizado por</th>
                                </tr>
                            </thead>
                        @endif

                        <tbody>
                            @foreach ($orders as $order)
                            <tr>
                                <td class="col-id">
                                    <a href="{{ route('client.history.show', ['branch' => $currentBranch()->id, 'order' => $order->id]) }}">{{ $order->id }}</a>
                                </td>
                                <td class="col-date">
                                    <a href="{{ route('client.history.show', ['branch' => $currentBranch()->id, 'order' => $order->id]) }}">{{ $order->created_at->format('d M Y H:m:s') }}</a>
                                </td>
                                <td class="col-status">
                                    <a href="{{ route('client.history.show', ['branch' => $currentBranch()->id, 'order' => $order->id]) }}">
                                        <div class="status-tag {{ $order->status_class }}">{{ __('app.status.'.$order->status) }}</div>
                                    </a>
                                </td>
                                <td class="col-author">
                                    <a href="{{ route('client.history.show', ['branch' => $currentBranch()->id, 'order' => $order->id]) }}">{{ $order->user->name }}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @if (!$orders->count())
                        <div class="table-empty">
                            <p>Sin últimos pedidos</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-xl-6">
            <div class="dashboard-card">
                <div class="card-header">
                    <h3>Top Productos</h3>
                </div>

                <div class="table table-top-products">
                    <table>
                        @if ($products->count())
                            <thead>
                                <tr>
                                    <th class="col-id">Nº</th>
                                    <th class="col-image"></th>
                                    <th class="col-name">Nombre</th>
                                </tr>
                            </thead>
                        @endif

                        <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <td class="col-id">{{ $product->id }}</td>
                                    <td class="col-image">
                                        <div class="product-image">
                                            <img src="{{ $product->image }}" alt="{{ $product->name }}">
                                        </div>
                                    </td>
                                    <td class="col-name">{{ $product->name }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @if (!$products->count())
                        <div class="table-empty">
                            <p>Sin top productos</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
