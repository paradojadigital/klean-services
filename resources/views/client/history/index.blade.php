@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <form class="search-bar" method="GET" action="{{ action('ClientHistoryController@index', ['branch' => $currentBranch()->id]) }}">
                <div class="form-search">
                    <i class="fa fa-search icon"></i>
                    <input id="search" type="text" class="form-input" name="id" placeholder="Buscar Pedidos" value="{{ $id }}">
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="tabs md">
                <li class="tab-item {{ $status == null ? 'active' : '' }}">
                    <a href="{{ route('client.history.index', ['branch' => $currentBranch()->id]) }}">Todas </a>
                </li>
                
                <li class="tab-item {{ $status == App\Order::STATUS_PENDING ? 'active' : '' }}">
                    <a href="{{ route('client.history.index', ['branch' => $currentBranch()->id, 'status' => App\Order::STATUS_PENDING]) }}">
                        Pendientes ({{ $currentBranch()->orders()->where('status', App\Order::STATUS_PENDING)->count() }})
                    </a>
                </li>
                
                <li class="tab-item {{ $status == App\Order::STATUS_COMPLETED ? 'active' : '' }}">
                    <a href="{{ route('client.history.index', ['branch' => $currentBranch()->id, 'status' => App\Order::STATUS_COMPLETED]) }}">
                        Completadas ({{ $currentBranch()->orders()->where('status', App\Order::STATUS_COMPLETED)->count() }})
                    </a>
                </li>

                <li class="tab-item {{ $status == App\Order::STATUS_DELIVERED ? 'active' : '' }}">
                    <a href="{{ route('client.history.index', ['branch' => $currentBranch()->id, 'status' => App\Order::STATUS_DELIVERED]) }}">
                        Enviados ({{ $currentBranch()->orders()->where('status', App\Order::STATUS_DELIVERED)->count() }})
                    </a>
                </li>

                <li class="tab-item {{ $status == App\Order::STATUS_PAYED ? 'active' : '' }}">
                    <a href="{{ route('client.history.index', ['branch' => $currentBranch()->id, 'status' => App\Order::STATUS_PAYED]) }}">
                        Pagados ({{ $currentBranch()->orders()->where('status', App\Order::STATUS_PAYED)->count() }})
                    </a>
                </li>

                <li class="tab-item {{ $status == App\Order::STATUS_NEW ? 'active' : '' }}">
                    <a href="{{ route('client.history.index', ['branch' => $currentBranch()->id, 'status' => App\Order::STATUS_NEW]) }}">
                        Nuevos ({{ $currentBranch()->orders()->where('status', App\Order::STATUS_NEW)->count() }})
                    </a>
                </li>
            </ul>

            <div class="table table-history">
                <table>
                    <thead>
                        @if ($orders->count())
                        <tr>
                            <th class="col-id">Nº de Pedido</th>
                            <th class="col-date">Fecha</th>
                            <th class="col-status">Status</th>
                            <th class="col-author">Realizado por</th>
                        </tr>
                        @endif
                    </thead>

                    <tbody>
                        @foreach ($orders as $order)
                        <tr>
                            <td class="col-id">
                                <a href="{{ route('client.history.show', ['branch' => $currentBranch()->id, 'order' => $order->id]) }}">{{ $order->id }}</a>
                            </td>
                            <td class="col-date">
                                <a href="{{ route('client.history.show', ['branch' => $currentBranch()->id, 'order' => $order->id]) }}">{{ $order->created_at->format('d M Y - h:m a') }}</a>
                            </td>
                            <td class="col-status">
                                <a href="{{ route('client.history.show', ['branch' => $currentBranch()->id, 'order' => $order->id]) }}">
                                    <div class="status-tag {{ $order->status_class }}">{{ __('app.status.'.$order->status) }}</div>
                                </a>
                            </td>
                            <td class="col-author">
                                <a href="{{ route('client.history.show', ['branch' => $currentBranch()->id, 'order' => $order->id]) }}">{{ $order->user->name }}</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $orders->links() }}

                @if (!$orders->count())
                <div class="table-empty">
                    <p>Sin pedidos en este edificio.</p>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
