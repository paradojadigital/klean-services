@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="filter-report-bar client-report">
                <form method="GET" action="{{ action('ClientReportsController@index', ['branch' => $currentBranch()->id]) }}">
                    
                    <div class="row align-items-end">
                        <div class="filter-report-col">
                            <p class="filter-title">Filtrar por</p>
                        </div>

                        <div class="filter-report-col">
                            <div class="form-field">
                                <label for="from" class="form-label">Desde</label>
                                <input id="from" type="text" class="form-input" name="from" calendar="from" data-default="{{ $from }}">
                            </div>
                        </div>

                        <div class="filter-report-col">
                            <div class="form-field">
                                <label for="to" class="form-label">Hasta</label>
                                <input id="to" type="text" class="form-input" name="to" calendar="to" data-default="{{ $to }}">
                            </div>
                        </div>

                        <div class="filter-report-col">
                            @if (Arrays::contains([App\Company::ROLE_OWNER, App\Company::ROLE_ADMIN], $roleCompany()))
                                <div class="form-field">
                                    <label for="building" class="form-label">Edificio</label>
                                    <div class="form-select">
                                        <select id="building" class="form-input" name="selected_branch">
                                            @foreach ($allBranches() as $branch)
                                            <option value="{{ $branch->id }}" {{ $branch->id == $selected_branch->id ? 'selected' : '' }}>{{ $branch->name }}</option>
                                            @endforeach
                                        </select>

                                        <i class="fa fa-caret-down caret"></i>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="filter-report-col">
                            <div class="form-field">
                                <input type="submit" class="btn btn-pri" value="Filtrar">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-12">
            <div class="preloading-chart-reports v-preloading"></div>
            <app-chart
                class="report-card"
                tag="client.orders"
                from="{{ $from }}"
                to="{{ $to }}"
                :entries='[{
                    name: "Pedidos",
                    backgroundColor: "rgba(56, 91, 232, 0.2)",
                    borderColor: "rgb(56, 91, 232)",
                    data: {!! collect($chart_data) !!}
                }]'/>
        </div>

        <div class="col-md-12">
           <div class="table table-report-orders">
                <table>
                    @if ($orders->count())
                    <thead>
                        <tr>
                            <th class="col-id">Nº</th>
                            <th class="col-date">Fecha</th>
                            <th class="col-status">Status</th>
                            <th class="col-company">Empresa</th>
                            <th class="col-author">Realizado por</th>
                        </tr>
                    </thead>
                    @endif

                    <tbody>
                        @foreach ($orders as $order)
                        <tr>
                            <td class="col-id">
                                <a href="{{ route('client.history.show', ['branch' => $currentBranch()->id, 'order' => $order->id]) }}">{{ $order->id }}</a>
                            </td>
                            <td class="col-date">
                                <a href="{{ route('client.history.show', ['branch' => $currentBranch()->id, 'order' => $order->id]) }}">{{ $order->created_at->format('d M Y - h:m a') }}</a>
                            </td>
                            <td class="col-status">
                                <a href="{{ route('client.history.show', ['branch' => $currentBranch()->id, 'order' => $order->id]) }}">
                                    <div class="status-tag {{ $order->status_class }}">{{ __('app.status.'.$order->status) }}</div>
                                </a>
                            </td>
                            <td class="col-company">
                                <a href="{{ route('client.history.show', ['branch' => $currentBranch()->id, 'order' => $order->id]) }}">{{ $order->company->name }}</a>
                            </td>
                            <td class="col-author">
                                <a href="{{ route('client.history.show', ['branch' => $currentBranch()->id, 'order' => $order->id]) }}">{{ $order->user->name }}</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $orders->appends(request()->except('page'))->links() }}

                @if (!$orders->count())
                <div class="table-empty">
                    <p>Sin ordenes en este edificio.</p>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
