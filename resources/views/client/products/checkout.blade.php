@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-10 offset-xl-1">
            <div class="client-message">
                <h3>Tu Pedido</h3>
                <p>Crea tu lista de productos y haz tu pedido, nosotros te lo llevamos.</p>
            </div>

            @if (!count($products))
            <div class="row">
                <div class="col-md-12">
                    <div class="empty-message">
                        <p>Sin productos agrega algunos para completar tu pedido.</p>
                    </div>
                </div>
            </div>
            @endif

            @if (count($products))
            <form method="POST" action="{{ action('ClientProductsController@store', ['branch' => $currentBranch()->id, 'quote' => $quote]) }}" class="row">
                {{ csrf_field() }}

                <div class="col-xl-9">
                    <div class="table table-checkout">
                        <table>
                            <thead>
                                <tr>
                                    <th class="col-image"></th>
                                    <th class="col-name">Nombre</th>
                                    <th class="col-description"></th>
                                    <th class="col-quantity">Cantidad</th>
                                    <th class="col-unit-price">Precio Unitario</th>
                                    <th class="col-total">Total</th>
                                    <th class="col-remove"></th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $i = 0 @endphp
                                
                                @foreach ($product_models as $product)
                                <tr>
                                    <td class="col-image">
                                        <div class="product-image">
                                            <img src="{{ $product->image }}" alt="">
                                        </div>
                                    </td>
                                    <td class="col-name">{{ $product->name }}</td>
                                    <td class="col-description">{{ $product->description }}</td>
                                    <td class="col-quantity">
                                        <div class="form-increment">
                                            <i class="fa fa-minus less"></i>
                                            <input type="text" class="form-input" name="products[{{ $i }}][quantity]" value="1">
                                            <input type="hidden" name="products[{{ $i }}][id]" value="{{ $product->id }}">
                                            <i class="fa fa-plus more"></i>
                                        </div>
                                    </td>
                                    <td class="col-unit-price">
                                        <span>${{ number_format($product->pivot->price, 2) }}</span>
                                        <input type="hidden" value="{{ $product->pivot->price }}">
                                    </td>
                                    <td class="col-total">${{ number_format($product->pivot->price, 2) }}</td>
                                    <td class="col-remove">
                                        <a id="{{ $product }}" href="" class="product">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </td>
                                </tr>
                                @php $i++ @endphp

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-lg-4 col-xl-3">
                    <div class="checkout-summary">
                        <div class="checkout-summary-total">
                            <h3>Total</h3>
                            <p id="checkout-total"> $0.00</p>
                        </div>

                        <div class="checkout-summary-doit">
                            <input type="submit" class="btn btn-pri" value="Realizar pedido">
                        </div>
                    </div>
                </div>
            </form>
            @endif
        </div>
    </div>
</div>
@endsection
