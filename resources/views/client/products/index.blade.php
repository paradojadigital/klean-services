@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10 offset-lg-1 col-xl-6 offset-xl-3">
            <div class="quote-products">
                <div class="client-message center">
                    <h3>Cotiza tus productos</h3>
                    <p>Llena tu lista de pedidos recurrente o haz un pedido ocasional</p>
                </div>

                <div class="row align-items-center">
                    <div class="col-md-4">
                        @if ($order)
                            <a href="{{ route('client.products.add', ['branch' => $currentBranch(), 'quote' => 1, 'products' => $order->products->pluck('id')->implode(',')]) }}" class="quote-card sem">
                        @else
                            <a href="{{ route('client.products.add', ['branch' => $currentBranch(), 'quote' => 1]) }}" class="quote-card sem">
                        @endif
                            <div class="quote-card-image"></div>
                            <div class="quote-card-name">
                                <p>Lista Semanal</p>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-4">
                        @if ($order)
                            <a href="{{ route('client.products.add', ['branch' => $currentBranch(), 'quote' => 2, 'products' => $order->products->pluck('id')->implode(',')]) }}" class="quote-card men">
                        @else
                            <a href="{{ route('client.products.add', ['branch' => $currentBranch(), 'quote' => 1]) }}" class="quote-card sem">
                        @endif
                            <div class="quote-card-image"></div>
                            <div class="quote-card-name">
                                <p>Lista Mensual</p>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-4">
                        @if ($order)
                            <a href="{{ route('client.products.add', ['branch' => $currentBranch(), 'quote' => 3, 'products' => $order->products->pluck('id')->implode(',')]) }}" class="quote-card oca">
                        @else
                            <a href="{{ route('client.products.add', ['branch' => $currentBranch(), 'quote' => 1]) }}" class="quote-card sem">
                        @endif
                            <div class="quote-card-image"></div>
                            <div class="quote-card-name">
                                <p>Lista Ocasional</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
