@extends('containers.app')

@section('container')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="message-wrap">
                <div class="message">
                    <!-- Complete mark : BEGIN -->
                    <div class="check_mark-wrap">
                        <div class="check_mark v-preloading-complete">
                            <div class="sa-icon sa-success animate">
                                <span class="sa-line sa-tip animateSuccessTip"></span>
                                <span class="sa-line sa-long animateSuccessLong"></span>
                                <div class="sa-placeholder"></div>
                                <div class="sa-fix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Complete mark : END -->
                    <h2>¡Orden enviada correctamente!</h2>
                    <p>Tu orden ha sido recibida, no olvides revisar el status de tu pedido</p>
                    <p>para darle seguimieto.</p>
                    <a href="{{ route('home') }}" class="btn btn-pri">Regresar a la plataforma</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
