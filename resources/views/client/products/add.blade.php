@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-9 offset-lg-3 col-xl-10 offset-xl-2">
            <div class="client-message">
                <h3>Añade Productos</h3>
                <p>Crea tu lista de productos y haz tu pedido, nosotros te lo llevamos.</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-xl-2">
            <h3 class="categories-title">Categorías</h3>
            <ul class="categories-list">
                @foreach ($categories as $category)
                <li class="categories-item">
                    <a href="{{ route('client.products.add', [
                        'branch' => $currentBranch(),
                        'quote' => $quote,
                        'category_id' => $category->id,
                        'products' => $product_ids
                    ]) }}">{{ $category->name }}</a>
                </li>
                @endforeach
            </ul>
        </div>

        <div class="col-lg-9 col-xl-10">
            <div class="row">
                <div class="col-lg-6 col-xl-4">
                    <form method="GET" action="{{ action('ClientProductsController@add', [
                        'branch' => $currentBranch(),
                        'quote' => $quote
                    ]) }}" class="form-search">
                        <i class="fa fa-search icon"></i>
                        <input id="search" type="text" class="form-input" name="name" placeholder="Buscar Productos" value="{{ $name }}">
                        <input id="search-products" type="hidden" name="products" value="{{ $product_ids }}">
                    </form>
                </div>

                @if ($category_id)
                <div class="col-md-12">
                    <h3 class="categories-selected">{{ $categories->find($category_id)->name }}</h3>
                </div>
                @else
                <div class="col-md-12">
                    <h3 class="categories-selected"></h3>
                </div>
                @endif

                @foreach ($products as $product)
                <div class="col-lg-4 col-xl-2">
                    <a id="{{ $product->id }}" href="" class="product-card">
                        <div class="product-card-image">
                            <img src="{{ $product->image }}" alt="{{ $product->name }}">
                        </div>

                        <div class="product-card-name">
                            <p title="{{ $product->name }}">{{ Strings::limit($product->name, 15, '...') }}</p>
                        </div>

                        <div class="product-card-description">
                            <p title="{{ $product->description }}">{{ Strings::limit($product->description, 25, '...') }}</p>
                        </div>

                        <div class="product-card-added">
                            <p>Añadir</p>
                        </div>
                    </a>
                </div>
                @endforeach

                @if (!$products->count())
                <div class="col-md-12">
                    <p class="empty">Sin productos.</p>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
