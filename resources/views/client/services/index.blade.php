@extends('containers.content')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="client-message center">
                <h3>Cotiza tu servicio de limpieza semanal </h3>
                <p>Llena tu lista de pedidos recurrente o haz un pedido ocasional</p>
            </div>

            <div class="wrapper">
                <div class="quote-service">
                    <form method="POST" action="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-field">
                                    <label for="" class="form-label">¿Cuantos días a la semana necesitas ayuda?</label>
                                    <ul class="service-days">
                                        <li class="service-day">
                                            <div class="checkbox">
                                                <input id="monday" type="checkbox" name="monday">
                                                <label for="monday">Lunes</label>
                                            </div>
                                        </li>
                                        
                                        <li class="service-day">
                                            <div class="checkbox">
                                                <input id="tuesday" type="checkbox" name="tuesday">
                                                <label for="tuesday">Martes</label>
                                            </div>
                                        </li>
                                        
                                        <li class="service-day">
                                            <div class="checkbox">
                                                <input id="wednesday" type="checkbox" name="wednesday">
                                                <label for="wednesday">Miercoles</label>
                                            </div>
                                        </li>

                                        <li class="service-day">
                                            <div class="checkbox">
                                                <input id="thursday" type="checkbox" name="thursday">
                                                <label for="thursday">Jueves</label>
                                            </div>
                                        </li>

                                        <li class="service-day">
                                            <div class="checkbox">
                                                <input id="friday" type="checkbox" name="friday">
                                                <label for="friday">Viernes</label>
                                            </div>
                                        </li>

                                        <li class="service-day">
                                            <div class="checkbox">
                                                <input id="saturday" type="checkbox" name="saturday">
                                                <label for="saturday">Sabado</label>
                                            </div>
                                        </li>

                                        <li class="service-day">
                                            <div class="checkbox">
                                                <input id="sunday" type="checkbox" name="sunday">
                                                <label for="sunday">Domingo</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-field">
                                    <label for="time-per-day" class="form-label">¿Cuanto tiempo cada día?</label>
                                    <ul class="service-days">
                                        <li class="service-day">
                                            <div class="radio">
                                                <input id="half-day" type="radio" name="time" value="1" checked>
                                                <label for="half-day">Medio día</label>
                                            </div>
                                        </li>
                                        
                                        <li class="service-day">
                                            <div class="radio">
                                                <input id="complete-day" type="radio" name="time" value="2">
                                                <label for="complete-day">Día completo</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-field">
                                    <label for="employees" class="form-label">¿Cuantas operadoras necesitas?</label>
                                    <input id="employees" type="text" class="form-input" name="employees" placeholder="Numero de operadoras">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-field">
                                    <label for="date" class="form-label">¿Qué día te gustaría que comenzaran?</label>
                                    <input id="date" type="text" class="form-input" name="date" calendar>
                                </div>
                            </div>

                            <div class="col-md-4 offset-md-8">
                                <div class="form-field">
                                    <input type="submit" class="btn btn-pri" value="Solicitar cotización">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
