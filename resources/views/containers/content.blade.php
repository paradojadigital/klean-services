@extends('containers.app')

@section('container')

<!-- # navbar & sidebar. -->
@include('components.navbar')
@include('components.sidebar')

<!-- # site content. -->
<div class="site-content">
    @yield('content')
</div>

<!-- # footer. -->
@include('components.footbar')

@endsection
