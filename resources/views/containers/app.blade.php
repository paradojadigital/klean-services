<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#2163c4">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff"> -->
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">

    <!-- csrf token. -->
    <title>{{ config('app.name') }}</title>

    <!-- styles. -->
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
    <div id="app" class="site">
        @yield('container')
    </div>

    <!-- scripts. -->
    <script src="/js/manifest.js"></script>
    <script src="/js/vendor.js"></script>
    <script src="/js/app.js"></script>
</body>
</html>
