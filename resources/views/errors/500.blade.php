@extends('containers.app')

@section('container')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="message-wrap">
                <div class="message">
                    <div class="down-image">
                        <img src="/images/errors/500.png" alt="500">
                    </div>

                    <h2>Error 500</h2>
                    <h3>¡No eres tu, somos nosotros! Nuestro servidor está fallando.</h3>
                    <p>Estamos arreglando el problema, si es algo urgente contáctanos en:</p>
                    <p><a href="mailto:contacto@ks.com">contacto@ks.com</a></p>
                    <a href="{{ route('home') }}" class="btn btn-pri">Regresar a la plataforma</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
