@extends('containers.app')

@section('container')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="message-wrap">
                <div class="message">
                    <div class="not-allow-image">
                        <img src="/images/errors/403.png" alt="403">
                    </div>

                    <h2>¡Hey! ¿Qué haces aquí?</h2>
                    <p>Al parecer no tienes autorización para ver esta página</p>
                    <a href="{{ route('home') }}" class="btn btn-pri">Regresar a la plataforma</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
