@extends('containers.app')

@section('container')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="message-wrap">
                <div class="message">
                    <div class="not-found-image">
                        <img src="/images/errors/404.png" alt="404">
                    </div>

                    <h2>¡0h no! Error 404</h2>
                    <p>No encontramos la página que buscas</p>
                    <a href="{{ route('home') }}" class="btn btn-pri">Regresar a la plataforma</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
