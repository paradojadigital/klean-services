<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>Central Work & Shop (Cumpleanios)</title>
</head>

<body>
    <!-- Custom : BEGIN -->
    <style>
        h1, h2, h3, h4, h5, h6, p, span, a, td, th {
            font-family: sans-serif;
            color: #3d5163;
        }
    </style>
    <!-- Custom : END -->

    <!-- CSS Reset : BEGIN -->
    <style>
        /* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        /* What it does: A work-around for email clients meddling in triggered links. */
        *[x-apple-data-detectors],  /* iOS */
        .x-gmail-data-detectors,    /* Gmail */
        .x-gmail-data-detectors *,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
        /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
        .a6S {
           display: none !important;
           opacity: 0.01 !important;
       }
       /* If the above doesn't work, add a .g-img class to any image in question. */
       img.g-img + div {
           display: none !important;
       }
       /* What it does: Prevents underlining the button text in Windows 10 */
        .button-link {
            text-decoration: none !important;
        }
        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
        /* Create one of these media queries for each additional viewport size you'd like to fix */
        /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
        @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
            .email-container {
                min-width: 320px !important;
            }
        }
        /* iPhone 6, 6S, 7, 8, and X */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
            .email-container {
                min-width: 375px !important;
            }
        }
        /* iPhone 6+, 7+, and 8+ */
        @media only screen and (min-device-width: 414px) {
            .email-container {
                min-width: 414px !important;
            }
        }
    </style>
    <!-- CSS Reset : END -->

    <!-- Progressive Enhancements : BEGIN -->
    <style>
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }
        /* Media Queries */
        @media screen and (max-width: 600px) {
            .email-container {
                width: 100% !important;
                margin: auto !important;
            }
            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }
            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }
            /* What it does: Adjust typography on small screens to improve readability */
            .email-container p,
            .email-container span,
            .email-container th,
            .email-container td,
            .email-container li,
            .email-container h5 {
                font-size: 17px !important;
            }
        }
    </style>
    <!-- Progressive Enhancements : BEGIN -->

    <center style="width: 100%;">
        <!-- Header : BEGIN -->
        <table border="0" cellpadding="0" cellspacing="0" width="750" class="email-container">
            <tr>
                <td style="padding: 50px 0 20px; text-align: center;">
                    <img src="{{ asset('images/logo.png') }}" alt="Central Work & Shop"/>
                </td>
            </tr>
        </table>
        <!-- Header : END -->

        <!-- Subheader : BEGIN -->
        <table border="0" cellpadding="0" cellspacing="0" width="750" class="email-container">
            <tr>
                <td style="padding: 50px 20px 70px; text-align: center;">
                    <h1 style="font-weight: 400; letter-spacing: 0.8px; font-size: 24px;">¡Alguien del staff cumple anios hoy {{ $user->birthdate->format('d M Y') }}!</h1>
                </td>
            </tr>
        </table>
        <!-- Subheader : END -->

        <!-- Content : BEGIN -->
        <table border="0" cellpadding="0" cellspacing="0" width="750" class="email-container">
            <tr>
                <td style="padding: 0 20px 45px; line-height: 23px; font-size: 14px; letter-spacing: 0.4px; text-align: center;">
                    <p>Deseale feliz cumpleanios a {{ $user->name }}</p>
                </td>
            </tr>
        </table>
        <!-- Content: END -->

        <!-- Footer : BEGIN -->
        <table border="0" cellpadding="0" cellspacing="0" class="email-container">
            <tr>
                <td style="padding-bottom: 20px;">
                    <p style="font-size: 12px !important; letter-spacing: 0.4px; text-align: center;">Central Work & Shop® Todos Los Derechos Reservados</p>
                </td>
            </tr>
        </table>
        <!-- Footer : END -->
    </center>
</body>
</html>
