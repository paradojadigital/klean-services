/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from 'vue'
import axios from 'axios'
// import Axios from 'axios'
// import _ from 'lodash'

// Import Components.
import Chart from '@/components/Chart'
import VisibleCheckbox from '@/components/VisibleCheckbox'
import VisibleCompany from '@/components/VisibleCompany'
import Countries from '@/components/Countries'
import SelectCompanyBranch from '@/components/SelectCompanyBranch'
import AssignCompanies from '@/components/AssignCompanies'
import ChangeStatus from '@/components/ChangeStatus'
import Notifications from '@/components/Notifications'

// Declare Components.
Vue.component('app-chart', Chart)
Vue.component('app-visible-checkbox', VisibleCheckbox)
Vue.component('app-visible-company', VisibleCompany)
Vue.component('app-countries', Countries)
Vue.component('app-company-branch', SelectCompanyBranch)
Vue.component('app-assign-companies', AssignCompanies)
Vue.component('app-change-status', ChangeStatus)
Vue.component('app-notifications', Notifications)

// Central Work & Shop Modules.
import KleanServices from './modules'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
new Vue({
    el: '#app'
})

/**
 * Event handlers issue (maybe it's the way of this works), when Vue
 * renders removes the #app div and clones to put it again, so the
 * handlers that you create for one element are gone. Get the elements
 * from here and add the handlers.
 */
KleanServices.boot()

/**
 *
 */
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
