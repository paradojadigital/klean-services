export default (function (scope) {

'use strict'

const File = function () {
    this.$files = null
}

/**
 * Static Methods
 */

/**
 * Check if a file is image.
 * @return {Boolean}
 */
function fileIsImage (file) {
    return file.type.match('image')
}

/**
 * Check if can use the File API.
 * @return {Boolean}
 */
function cantDo () {
    return !(window.File && window.FileReader && window.FileList && window.Blob)
}

/**
 * Check if a file's weight is more than 2mb.
 * @return {Boolean}
 */
function fileTooHuge (file) {
    let sizes = ['B', 'KB', 'MB', 'GB']
    let fs = file.size
    let i = 0

    while (fs / 1000 > 1) {
        fs /= 1000
        i++
    }

    if (i >= 2 && fs >= 2) {
        return true
    }
}

File.prototype = {
    /**
     * Check if the File API is in the browser and get the file inputs.
     * @return {File}
     */
    init: function () {
        if (cantDo()) {
            alert('This browser not supports the features of the page, ' +
                ' please update or download another in http://browsehappy.com.')
            return this
        }

        this.$files = scope.document.querySelectorAll('input[type="file"]')
        this.$boxes = scope.document.querySelectorAll('.form-file')
        this.fileHandler()

        return this
    },

    /**
     * Add handlers to catch the files in the platform.
     * @return {File}
     */
    fileHandler: function () {
        for (let $file of this.$files) {
            $file.addEventListener('change', function (e) {
                let f = e.target.files[0]
                let $i = $file.parentElement.querySelector('.form-file img')

                // TODO: if the image fails.
                // e.target.value = null

                this.imagePreview(f, $i)
            }.bind(this))
        }

        for (let $box of this.$boxes) {
            $box.addEventListener('drop', function (e) {
                e.stopPropagation()
                e.preventDefault()
                
                let f = e.dataTransfer.files[0]
                let $i = $box.parentElement.querySelector('img')
                let $b = $box.parentElement.querySelector('input[type="file"]')

                // Assign the file, without this the field name="image" send
                // nothing.
                $b.files = e.dataTransfer.files
                this.imagePreview(f, $i)
            }.bind(this))

            $box.addEventListener('dragover', function (e) {
                e.preventDefault()
                e.stopPropagation()
            })
        }

        return this
    },

    /**
     * Show a preview of the handled image.
     * @return {File}
     */
    imagePreview: function (f, $i) {
        let r = new FileReader()

        if (fileTooHuge(f)) {
            return alert('the file is too huge, max 2mb.')
        }

        if (!fileIsImage(f)) {
            return alert('the file is not an image.')
        }

        r.onload = function () {
            $i.src = r.result
        }

        r.readAsDataURL(f)

        return this
    }
}

return new File()

})(window)
