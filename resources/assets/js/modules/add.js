export default (function (scope) {

'use strict'

const Add = function () {
    this.ids = []
    this.href = null
}

/**
 * 
 */
function extractQueryParameters (url) {
    let queries = {}
    
    url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, k, v) {
        return queries[k] = v
    })

    return queries
}

/**
 * 
 */
function queryObjectToString (o) {
    let str = '?'

    for (let k of Object.keys(o)) {
        str += k + '=' + o[k] + '&'
    }

    return str.slice(0, -1)
}

Add.prototype = {
    /**
     * 
     */
    init: function () {
        this.setNextHref()
        this.initializeIds()
        this.handleProductClick()
    },

    /**
     * 
     */
    setNextHref: function () {
        let $n = scope.document.querySelector('#products-next')

        if ($n) {
            this.href = $n.href
        }
    },

    /**
     * 
     */
    initializeIds: function () {
        let params = extractQueryParameters(decodeURIComponent(scope.location.search))
        let $ps = scope.document.querySelectorAll('.product-card')

        if (!params.products) {
            return false
        }

        this.ids = params.products.split(',')

        for (let $p of $ps) {
            if (this.ids.includes($p.getAttribute('id'))) {
                $p.classList.add('added')
                $p.querySelector('.product-card-added').innerText = 'Quitar'
            }
        }

        this.updateProductsCounter()
        this.updateNextUrl()
    },

    /**
     * 
     */
    handleProductClick: function () {
        let $ps = scope.document.querySelectorAll('.product-card')

        for (let $p of $ps) {
            $p.addEventListener('click', function (e) {
                e.preventDefault()

                if ($p.classList.contains('added')) {
                    $p.classList.remove('added')
                    $p.querySelector('.product-card-added').innerText = 'Añadir'
                    this.ids.splice(this.ids.findIndex(function (id) {
                        return id === $p.getAttribute('id')
                    }), 1)
                } else {
                    $p.classList.add('added')
                    $p.querySelector('.product-card-added').innerText = 'Quitar'
                    this.ids.push($p.getAttribute('id'))
                }

                this.updateProductsCounter()
                this.updateCategoriesUrl()
                this.updateNextUrl()
                this.updateUrl('add')
                this.updateSearchInput()
            }.bind(this))
        }
    },

    /**
     *
     */
    updateCategoriesUrl: function () {
        let $cs = scope.document.querySelectorAll('.categories-item')

        for (let $c of $cs) {
            let $a = $c.querySelector('a')
            let href = $a.getAttribute('href')
            let params = extractQueryParameters(decodeURIComponent(href))

            $a.href = [href.split('?')[0], '?', 'category_id=' + params.category_id].join('')

            if (this.ids.length) {
                $a.href = [$a.href, '&', 'products=' + this.ids.join(',')].join('')
            }
        }
    },

    /**
     *
     */
    updateSearchInput: function () {
        let $i = scope.document.querySelector('#search-products')
        $i.value = this.idsToString()
    },

    /**
     * 
     */
    updateUrl: function (route) {
        let params = extractQueryParameters(decodeURIComponent(scope.location.search))

        if (this.ids.length) {
            params['products'] = this.idsToString()
            scope.history.replaceState({}, scope.title, queryObjectToString(params))
        } else {
            delete params['products']
            scope.history.replaceState({}, scope.title, queryObjectToString(params))
        }
    },

    /**
     * 
     */
    idsToString: function () {
        return this.ids.join(',')
    },

    /**
     *
     */
    updateNextUrl: function () {
        let $n = scope.document.querySelector('#products-next')

        if ($n && this.ids.length) {
            $n.href = this.href + '?products=' + this.idsToString()
        }

        if ($n && !this.ids.length) {
            $n.href = this.href
        }
    },

    /**
     *
     */
    updateProductsCounter: function () {
        let $c = scope.document.querySelector('#products-count')

        if (!$c) {
            return false
        }

        $c.innerText = this.ids.length == 1
            ? '1 producto seleccionado'
            : this.ids.length + ' productos seleccionado'
    }
}

return new Add()

})(window)
