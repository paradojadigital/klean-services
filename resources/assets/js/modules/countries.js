export default (function (scope) {

'use strict'

const Countries = function () {
    this.$countries = null
    this.$country_val = null
}

Countries.prototype = {
    /**
     * Get the countries in the dom and the countries input that holds the value.
     * @return {Countries}
     */
    init: function () {
        this.$countries = scope.document.querySelectorAll('.country-card')
        this.$country_val = scope.document.querySelector('#country')

        if (this.$countries.length && this.$country_val) {
            this.firstSelected()
        }

        this.addSelectHandler()

        return this
    },

    /**
     * If is the first time set the default country.
     * @return {Countries}
     */
    firstSelected: function () {
        this.$countries[0].classList.add('active')
        this.$country_val.value = this.$countries[0].getAttribute('value')
        return this
    },

    /**
     * Iterate the countries and add a click handler to set the new selected
     * country.
     * @return {Countries}
     */
    addSelectHandler: function () {
        for (let $country of this.$countries) {
            $country.addEventListener('click', function (e) {
                e.preventDefault()

                if (this.$country_val) {
                    this.resetAlreadySelected()
                    this.$country_val.value = $country.getAttribute('value')
                    $country.classList.add('active')
                }
            }.bind(this))
        }

        return this
    },

    /**
     * Clear the active class of the elements.
     * @return {Countries}
     */
    resetAlreadySelected: function () {
        for (let $country of this.$countries) {
            $country.classList.remove('active')
        }

        return this
    }
}

return new Countries()

})(window)
