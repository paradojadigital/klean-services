import axios from 'axios'

export default (function (scope) {

'use strict'

const Dropdown = function () {
    this.$ds = null
}

Dropdown.prototype = {
    /**
     * Get the dropdown containers and assign the handlers to get work it.
     * @return {Dropdown}
     */
    init: function () {
        this.$ds = scope.document.querySelectorAll('.dropdown-container')

        for (let $d of this.$ds) {
            $d.querySelector('a').addEventListener('click', function (e) {
                e.preventDefault()
                e.stopPropagation()
                this.toogleDropdown($d)
            }.bind(this))
        }

        scope.document.addEventListener('click', function () {
            this.closeAllDropdown()
        }.bind(this))

        return this
    },

    /**
     * Expand the dropdown check if has the class expand if isn't add it.
     * @param {DOM} $d
     * @return {Dropdown}
     */
    toogleDropdown: function ($d) {
        if ($d.classList.contains('expand')) {
            $d.classList.remove('expand')
            return this
        }

        $d.classList.add('expand')

        // Mark notifications as read
        let response = axios.get('/admin/notifications/read')
        
        return this
    },

    /**
     * When the dropdown losses focus hide all the dropdowns.
     * @return {Dropdown}
     */
    closeAllDropdown: function () {
        for (let $d of this.$ds) {
            $d.classList.remove('expand')
        }

        return this
    }
}

return new Dropdown()

})(window)
