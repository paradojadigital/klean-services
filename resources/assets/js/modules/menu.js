export default (function (scope) {

'use strict'

const Menu = function () {
    this.$menus = null
}

Menu.prototype = {
    init: function () {
        this.$menus = scope.document.querySelectorAll('.menu-points')
        this.menuClickHandler()
    },

    menuClickHandler: function () {
        for (let $menu of this.$menus) {
            $menu.addEventListener('click', function (e) {
                let dropdown = $menu.querySelector('.menu-points-dropdown')

                // e.preventDefault()
                e.stopPropagation()

                if (dropdown.classList.contains('show')) {
                    dropdown.classList.remove('show')
                    return false
                }

                dropdown.classList.add('show')
            })
        }

        scope.document.addEventListener('click', function () {
            this.closeAllDropdowns()
        }.bind(this))

        return this
    },

    closeAllDropdowns: function () {
        for (let $menu of this.$menus) {
            let dropdown = $menu.querySelector('.menu-points-dropdown')
            dropdown.classList.remove('show')
        }

        return this
    }
}

return new Menu()

})(window)
