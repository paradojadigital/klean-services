import Datamap from 'datamaps'

export default (function (scope) {

'use strict'

const Maps = function () {
    this.$map = null
    this.map = null
    this.dataset = {}
    this.values = null
    this.min_value = null
    this.max_value = null
    this.palette = null
    this.series = [/*['COL', 1600],['MEX', 2000]*/]
    this.colors = {
        min: '#b2d3fe',
        max: '#385be8',
        border: '#fff',
        border_active: '#5a5a5a',
        default: '#d3d3d3'
    }
}

Maps.prototype = {
    init: function () {
        this.$map = scope.document.querySelector('[map]')

        // There is not a map container to draw.
        if (!this.$map) {
            return this
        }

        // Get values a set the format for this.
        this.setSeriesValues()
        this.setPaletteScale()
        this.fillSeriesWantedFormat()

        // Create the map and add handlers.
        this.createMap()
        this.mapHandlers()

        return this
    },

    createMap: function () {
        let ctx = this

        this.map = new Datamap({
            scope: 'world',
            element: ctx.$map,
            projection: 'mercator',
            responsive: true,
            data: ctx.dataset,
            geographyConfig: {
                borderColor: ctx.colors.border,
                highlightBorderWidth: 1,
                highlightBorderColor: ctx.colors.border_active,
                highlightFillColor: function (geo) {
                    return geo['fillColor'] || ctx.colors.default
                },
                popupTemplate: function (geo, data) {
                    if (!data) { return }
                    
                    return [
                        '<div class="popup-template">',
                        '<strong>', geo.properties.name, '</strong>',
                        '<br>Pedidos: <strong>', data.numberOfThings, '</strong>',
                        '</div>'
                    ].join('')
                }
            },
            fills: {
                defaultFill: ctx.colors.default
            }
        })

        return this
    },

    mapHandlers: function () {
        let ctx = this

        scope.addEventListener('resize', function () {
            ctx.map.resize()
        })

        return this
    },

    setSeriesValues: function () {
        this.series = JSON.parse(this.$map.getAttribute('map'))
        this.values = this.series.map(function (o) { return o[1] })
        this.max_value = Math.max.apply(null, this.values)
        this.min_value = 0

        return this
    },

    setPaletteScale: function () {
        this.palette = d3.scale.linear()
            .domain([this.min_value, this.max_value])
            .range([this.colors.min, this.colors.max])

        return this
    },

    /**
     * Fill the dataset with the wanted format to get working the map.
     * @return {Maps}
     */
    fillSeriesWantedFormat: function () {
        for (let serie of this.series) {
            let iso = serie[0], value = serie[1]
            this.dataset[iso] = { numberOfThings: value, fillColor: this.palette(value) }
        }

        return this
    }
}

return new Maps()

})(window)
