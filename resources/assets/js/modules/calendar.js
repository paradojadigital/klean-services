import Flatpickr from 'flatpickr'
import moment from 'moment'

export default (function (scope) {

'use strict'

const Calendar = function () {
    let date = new Date()
    let d, m, y
    let date_from, date_to
    
    this.$calendars = null

    // Get date details.
    d = date.getDate()
    m = date.getMonth() + 1
    y = date.getFullYear()

    // Make the date from and to.
    date_from = `${y}-${m}-${d} 00:00:00`
    date_to = `${y}-${m}-${d} 23:59:59`

    // Calendar options.
    this.$calendar_options = {
        dateFormat: 'Y-m-d H:i:S',
        altFormat: 'Y-m-d',
        altInput: true,
        enableTime: true,
        time_24hr: false,
        disableMobile: true,
        defaultDate: Date.now()
    }

    // Assign the date from.
    this.$calendar_from_options = Object.assign({}, this.$calendar_options, {
        defaultDate: date_from,
    })

    // Assign the date to.
    this.$calendar_to_options = Object.assign({}, this.$calendar_options, {
        defaultDate: date_to
    })
}

Calendar.prototype = {
    /**
     * Get the calendar inputs and add call the function to assign the
     * handlers.
     * @return {Calendar}
     */
    init: function () {
        this.$calendars = scope.document.querySelectorAll('[calendar]')
        this.addInputCalendars()

        return this
    },

    /**
     * Add input handlers to show the calendar (flatpickr).
     * @return {Calendar}
     */
    addInputCalendars: function () {
        for (let $calendar of this.$calendars) {
            let options = this.$calendar_options
            let calendar_type = $calendar.getAttribute('calendar')
            let calendar_default = $calendar.dataset.default

            if (calendar_type === 'from') {
                options = Object.assign({}, this.$calendar_from_options, {
                    defaultDate: calendar_default || this.$calendar_from_options.defaultDate
                })
            }

            if (calendar_type === 'to') {
                options = Object.assign({}, this.$calendar_to_options, {
                    defaultDate: calendar_default || this.$calendar_to_options.defaultDate
                })
            }

            Flatpickr($calendar, options)
        }

        return this
    }
}

return new Calendar()

})(window)
