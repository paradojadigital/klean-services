import Dropdown from './dropdown'
import Forms from './forms'
// import Countries from './countries'
import Modal from './modal'
import File from './file'
import Calendar from './calendar'
import Maps from './datamap'
import Add from './add'
import Menu from './menu'
import Preloading from './preloading'
import Tabs from './tabs'
// Uncomment if the vue lines that create his instance are uncommented.
import Autofocus from './autofocus'

// Libraries.
import '@/libs/mousetrap'
import '@/libs/mousetrap-bind-global'

export default (function (scope) {

'use strict'

const KleanServices = function () {
    //
}

KleanServices.prototype = {
    /**
     * Run the needed element to get work the platform.
     * @return {KleanServices}
     */
    boot: function () {
        Dropdown.init()
        Forms.init()
        // Countries.init()
        Modal.init()
        File.init()
        Calendar.init()
        Maps.init()
        Add.init()
        Menu.init()
        Autofocus.init()
        Preloading.init()
        Tabs.init()

        this.setKeyBindings()
        this.setModules()

        return this
    },

    setModules: function () {
        this.modules = {
            Dropdown, Forms,/* Countries,*/
            Modal, File, Calendar,
            Maps, Add, Menu, Preloading,
            Tabs
        }
    },

    setKeyBindings: function () {
        Mousetrap.bindGlobal('esc', function () {
            Modal.closeAllModals()
            Dropdown.closeAllDropdown()
            Menu.closeAllDropdowns()
        })
    }
}

return new KleanServices()

})(window)
