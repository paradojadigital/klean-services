export default (function (scope) {

'use strict'

// Class.
const Loading = function () {
    this.loadings$ = null
}

Loading.prototype = {
    /**
     *
     */
    init: function () {
        this.loadings$ = scope.document.querySelectorAll('.v-preloading')
        this.completes$ = scope.document.querySelectorAll('.v-preloading-complete')
        
        for (let loading$ of this.loadings$) {
            loading$.style.display = 'none'
        }

        for (let complete$ of this.completes$) {
            complete$.style.display = 'block'
        }
    }
}

return new Loading()

})(window)
