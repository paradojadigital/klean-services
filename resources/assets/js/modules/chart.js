

export default (function (scope) {

'use strict'

const Chart = function () {
    this.$charts = null
    this.values = null
    this.labels = null
    this.dataset = []
    this.tolerance = 1
    this.orders = [
        ['2017-10-10', 10],
        ['2017-12-20', 1],
        ['2018-01-08', 4],
        ['2018-01-10', 5],
        ['2018-01-22', 8],
        ['2018-02-01', 2],
        ['2018-02-20', 1],
        ['2018-02-26', 5]
    ]
}

Chart.prototype = {
    init: function () {
        this.$charts = scope.document.querySelectorAll('[chart]')

        this.formatEntries()
        this.setLabels()
        this.setValues()
        this.setDataset()
        this.renderCharts()

        return this
    },

    renderCharts: function () {
        let ctx = this

        for (let $chart of this.$charts) {
            new ChartJS($chart.getContext('2d'), {
                type: 'line',
                data: {
                    labels: ctx.labels,
                    datasets: ctx.dataset
                },
                options: {
                    maintainAspectRatio: false,
                    tooltips: {
                        intersect: false,
                        mode: 'nearest'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            })
        }

        return this
    },

    formatEntries: function () {
        this.orders = decimate(this.orders, this.tolerance)
        return this
    },

    setLabels: function () {
        this.labels = this.orders.map(function (o) { return o['x'] })
        return this
    },

    setValues: function () {
        this.values = this.orders.map(function (o) { return o['y'] })
        return this
    },

    setDataset: function () {
        this.dataset.push({
            label: 'Pedidos',
            backgroundColor: 'rgba(137, 161, 238, 0.2)',
            pointBackgroundColor: '#385be8',
            borderColor: '#385be8',
            data: this.values
        })
    }
}

return new Chart()

})(window)
