export default (function (scope) {

'use strict'

const Autofocus = function () {
    this.$input = null
}

Autofocus.prototype = {
    /**
     * @return {Autofocus}
     */
    init: function () {
        this.$input = scope.document.querySelector('[autofocus]')

        if (this.$input) {
            this.$input.focus()
        }

        return this
    }
}

return new Autofocus()

})(window)
