export default (function (scope) {

'use strict'

const Tabs = function () {
    this.$ts = []
    this.$tsw = []
}

Tabs.prototype = {
    /**
     *
     */
    init: function () {
        this.assignTabs()
        this.tabClickHandler()
        this.firstTime()
    },

    /**
     *
     */
    assignTabs: function () {
        this.$ts = scope.document.querySelectorAll('[tab]')
        this.$tsw = scope.document.querySelectorAll('.tab-wrapper')
    },

    /**
     * 
     */
    tabClickHandler: function () {
        for (let $t of this.$ts) {
            $t.addEventListener('click', function () {
                this.currentTab($t)
            }.bind(this))
        }
    },

    /**
     *
     */
    currentTab: function ($t) {
        this.resetTabs()

        let $tw = scope.document.querySelector($t.getAttribute('tab'))

        $t.classList.add('active')
        $tw.style.display = 'block'
    },

    /**
     *
     */
    resetTabs: function () {
        for (let $tw of this.$tsw) {
            $tw.style.display = 'none'
        }

        for (let $t of this.$ts) {
            $t.classList.remove('active')
        }
    },

    /**
     * 
     */
    firstTime: function () {
        if (!this.$ts.length) {
            return null
        }

        let $t = this.$ts[0]
        let $tw = scope.document.querySelector($t.getAttribute('tab'))

        if (!($t && $tw)) {
            return null
        }

        $t.classList.add('active')
        $tw.style.display = 'block'
    }
}

return new Tabs()

})(window)
