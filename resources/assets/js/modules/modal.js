export default (function (scope) {

'use strict'

const MODAL_DURATION = 200

const Modal = function () {
    this.$modals = null
    this.$umbrella = null
    this.$modal_close = null
    this.$modal_triggers = null
}

Modal.prototype = {
    /**
     *
     */
    init: function () {
        this.$body = scope.document.querySelector('body')
        this.$modals = scope.document.querySelectorAll('.modal')
        this.$umbrella = scope.document.querySelector('.modal-umbrella')
        this.$modal_close = scope.document.querySelectorAll('.modal-close')
        this.$modal_triggers = scope.document.querySelectorAll('[modal]')

        if (this.$umbrella) {
            this.modalHandlers()
            this.umbrellaHandler()
            this.umbrellaOverflow()
        }

        return this
    },

    /**
     *
     */
    openModal: function ($trigger) {
        let $modal = scope.document.querySelector($trigger.getAttribute('modal'))
        let $modal_input = $modal.querySelector('input:not([type="hidden"])')

        setTimeout(function () {
            $modal_input.focus()
        }, MODAL_DURATION)

        this.$body.classList.add('overflow')
        this.$umbrella.classList.add('show')
        $modal.classList.add('active')
        this.umbrellaOverflow()

        return this
    },

    /**
     *
     */
    umbrellaOverflow: function () {
        let $modal_active = scope.document.querySelector('.modal.active')
        let window_height = null
        let real_umbrella_height = null

        if (!$modal_active) {
            return this
        }

        window_height = window.innerHeight
        real_umbrella_height = $modal_active.offsetHeight + 60

        if (window_height < real_umbrella_height) {
            this.$umbrella.classList.add('overflow')
            return this
        }

        this.$umbrella.classList.remove('overflow')

        return this
    },

    /**
     *
     */
    closeAllModals: function () {
        if (this.$umbrella) {
            this.$umbrella.classList.remove('show')
            this.$body.classList.remove('overflow')
        }

        for (let $modal of this.$modals) {
            let $form = $modal.querySelector('form')
            
            $modal.classList.remove('active')
            
            this.resetHidden()
            $form.reset()
        }

        return this
    },

    /**
     * 
     */
    resetHidden: function () {
        let $f = scope.document.querySelector('#form-modal')
        
        if (!$f) {
            return null
        }

        $f.querySelector('img').removeAttribute('src')
        $f.querySelector('[name="src"]').value = null
    },

    /**
     *
     */
    umbrellaHandler: function () {
        this.$umbrella.addEventListener('click', function () {
            this.closeAllModals()
        }.bind(this))

        window.addEventListener('resize', function () {
            this.umbrellaOverflow()
        }.bind(this))

        return this
    },

    /**
     *
     */
    modalHandlers: function () {
        for (let $trigger of this.$modal_triggers) {
            $trigger.addEventListener('click', function (e) {
                e.preventDefault()
                this.openModal($trigger)
                this.defaultData($trigger)
            }.bind(this))
        }

        for (let $modal of this.$modals) {
            $modal.addEventListener('click', function (e) {
                e.stopPropagation()
            })
        }

        for (let $close of this.$modal_close) {
            $close.addEventListener('click', function (e) {
                e.preventDefault()
                this.closeAllModals()
            }.bind(this))
        }

        return this
    },

    /**
     *
     */
    defaultData: function ($trigger) {
        let data = JSON.parse($trigger.getAttribute('data'))
        let $form = scope.document.querySelector('#form-modal')
        let form_inputs = $form.elements

        if (!data) {
            return null
        }

        for (let d in data) {
            if (form_inputs[d] && form_inputs[d].type !== 'file') {
                form_inputs[d].value = data[d]
            }

            if (form_inputs[d] && form_inputs[d].type === 'file') {
                form_inputs[d].parentNode.querySelector('img').src = data[d]
                form_inputs[d].parentNode.querySelector('[name="src"]').value = data[d]
            }
        }

        for (let d in data.pivot) {
            if (form_inputs[d]) {
                form_inputs[d].value = data.pivot[d]
            }
        }
    }
}

return new Modal()

})(window)
