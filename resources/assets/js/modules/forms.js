import accounting from 'accounting-js'

export default (function (scope) {

'use strict'

const Submit = function () {
    this.$forms = null
    this.$btns = null
    this.$selects = null
    this.$forms_selects = null
    this.$is_submiting = false
}

Submit.prototype = {
    /**
     * Get the forms and buttons.
     * @return {Form}
     */
    init: function () {
        this.$forms = scope.document.querySelectorAll('[no-btn]')
        this.$increments = scope.document.querySelectorAll('.form-increment')
        this.$btns = scope.document.querySelectorAll('[submit]')
        this.$checkout_total = scope.document.querySelector('#checkout-total')

        // For the selects out his form (design so...)
        this.$selects = scope.document.querySelectorAll('[select-form]')

        this.iterateForms()
        this.submitFromButton()
        this.inputIncrementHandler()
        this.handleSelectOutForm()
        this.handleChangeBranch()

        // Central Work & Shop staff (admin, staff) change between forms.
        this.formStaff()

        return this
    },

    /**
     *
     */
    handleChangeBranch () {
        let $s = scope.document.querySelector('[change-branch]')

        if (!$s) {
            return null
        }

        let $f = scope.document.querySelector($s.getAttribute('change-branch'))

        if ($s) {
            $s.addEventListener('change', function (e) {
                $f.submit()
            })
        }
    },

    /**
     *
     */
    handleSelectOutForm: function () {
        for (let $select of this.$selects) {
            // Submit form when changes the value of the select.
            $select.addEventListener('change', function () {
                scope.document
                    .querySelector($select.getAttribute('select-form'))
                    .submit()
            })
        }
    },

    /**
     * Get the submit attribute of the button, with this get the form
     * and submit it.
     * @return {Form}
     */
    submitFromButton: function () {
        for (let $btn of this.$btns) {
            $btn.addEventListener('click', function () {
                scope.document.querySelector(this.getAttribute('submit')).submit()
            })
        }

        return this
    },

    iterateForms: function () {
        for (let $form of this.$forms) {
            this.addInputHandler($form)
        }

        return this
    },

    /**
     * Increment input with buttons.
     * @return {Form}
     */
    inputIncrementHandler: function () {
        // Show the price of all the products with quantity 1.
        this.updateCheckoutSummary()

        for (let $increment of this.$increments) {
            let $less = $increment.querySelector('.less')
            let $more = $increment.querySelector('.more')
            let $input = $increment.querySelector('input')

            // 
            $less.addEventListener('click', function () {
                if (parseInt($input.value) > 1) {
                    $input.value = parseInt($input.value) - 1
                }

                // TODO: update the total of the row.
                // TODO: update the checkout summary.
                this.updateCheckoutSummary()
            }.bind(this))

            // 
            $more.addEventListener('click', function () {
                $input.value = parseInt($input.value) + 1

                // TODO: update the total of the row.
                // TODO: update the checkout summary.
                this.updateCheckoutSummary()
            }.bind(this))

            // 
            $input.addEventListener('input', function () {
                this.updateCheckoutSummary()
            }.bind(this))
        }

        return this
    },

    /**
     *
     */
    totalProductsPrice: function () {
        let total = 0

        for (let $i of this.$increments) {
            let $col = $i.parentNode
            let $row = $col.parentNode
            let $total = $row.querySelector('.col-total')
            let quantity = Number($i.querySelector('.form-input').value)
            let price = $row.querySelector('.col-unit-price input').value

            if (isNaN(quantity)) {
                return '$0.00'
            }

            // Get his parent row.
            total += price * quantity
            $total.innerText = accounting.formatMoney(price * quantity)
        }

        return accounting.formatMoney(total)
    },

    /**
     * 
     */
    updateCheckoutSummary: function () {
        if (this.$checkout_total) {
            this.$checkout_total.innerText = this.totalProductsPrice()
        }
    },

    /**
     * Add an event listener when the user press enter when a input is focus,
     * and submit the current form.
     * @return {Form}
     */
    addInputHandler: function ($form) {
        let $inputs = $form.querySelectorAll('input[type="text"]:not(.ignore)')

        for (let $input of $inputs) {
            $input.addEventListener('keydown', function (e) {
                if (e.which === 13 && !this.is_submiting) {
                    this.is_submiting = true
                    $form.submit()
                }
            }.bind(this))
        }

        return this
    },

    /**
     *
     */
    formStaff: function () {
        const staff_types = ['admin', 'staff']

        const radios$ = scope.document.querySelectorAll('#staff-role input[type="radio"]')
        const email$ = scope.document.querySelector('#staff-email')
        const birth$ = scope.document.querySelector('#staff-birth')
        const branch_title$ = scope.document.querySelector('#staff-branch-title')
        const branches$ = scope.document.querySelector('#staff-branches')

        if (!radios$.length) {
            return null
        }

        for (let radio$ of radios$) {
            if (radio$.getAttribute('checked')) {
                renderFormStaff(radio$)
            }

            radio$.addEventListener('change', function () {
                renderFormStaff(radio$)
            })
        }

        function renderFormStaff(radio$) {
            if (radio$.value === staff_types[0]) {
                email$.style.display = 'block'
                birth$.style.display = 'none'
                branch_title$.style.display = 'none'
                branches$.style.display = 'none'
            }

            if (radio$.value === staff_types[1]) {
                email$.style.display = 'none'
                birth$.style.display = 'block'
                branch_title$.style.display = 'block'
                branches$.style.display = 'block'
            }
        }
    }
}

return new Submit()

})(window)
