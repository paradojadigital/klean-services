<?php

return [
	'status.new' => 'Nuevo',
    'status.pending' => 'Pendiente',
    'status.payed' => 'Pagado',
    'status.delivered' => 'Enviado',
    'status.completed' => 'Completado',
    'status.canceled' => 'Cancelado',
];
