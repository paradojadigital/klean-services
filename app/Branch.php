<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Branch extends Model
{
    const ROLE_ADMIN = 'admin';
    const ROLE_STAFF = 'staff';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'company_branches';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'zipcode',
        'neighborhood',
        'street',
        'number_ext',
        'number_int',
        'city',
        'state',
    ];

    /**
     *
     */
    public function users(): BelongsToMany
    {
    	return $this->belongsToMany(User::class, 'branch_has_user')->withPivot([
            'role',
        ]);
    }

    /**
     *
     */
    public function kleanServicesStaff(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'branch_has_staff');
    }

    /**
     *
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    /**
     *
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    /**
     *
     */
    public function products(): BelongsToMany
    {
        return $this->company->products();
    }
}
