<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class RolesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $user = $request->user();

        // Client roles.
        $client_roles = [
            User::TYPE_CLIENT
        ];

        // Admin roles.
        $admin_roles = [
            User::TYPE_SUPERADMIN,
            User::TYPE_ADMIN
        ];

        if ($role == 'admins' && in_array($user->type, $client_roles)) {
            // Maybe the user is not assigned to a company but to a branch.
            $company = $user->companies->first() ?? $user->branches->first()->company;

            if (!$company) {
                return abort(403, 'El usuario no tiene edificio ni compañia.');
            }

            $branch = $user->branches->first();

            if (!$branch) {
                return redirect()->route('client.company.branches', [
                    'company' => $company->id
                ]);
            }

            return redirect()->route('client.home', [
                'branch' => $branch->id
            ]);
        }

        if ($role == 'clients' && in_array($user->type, $admin_roles)) {
            // Send it to where it belongs.
            return redirect()->route('home');
        }

        return $next($request);
    }
}
