<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $orders = Order::query();

        if ($request->status) {
            $orders = $orders->where('status', $request->status);
        }

        if ($request->id) {
            $orders = $orders->where('id', $request->id);
        }

        return view('sections.orders.index', [
            'orders' => $orders->orderBy('id', 'DESC')->paginate(50)->appends(Input::except('page')),
            'orders_count' => $orders->count(),
            'id' => $request->id,
            'status' => $request->status,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $statuses = collect([
            Order::STATUS_NEW => __('app.status.new'),
            Order::STATUS_PENDING => __('app.status.pending'),
            Order::STATUS_PAYED => __('app.status.payed'),
            Order::STATUS_DELIVERED => __('app.status.delivered'),
            Order::STATUS_COMPLETED => __('app.status.completed'),
            Order::STATUS_CANCELED => __('app.status.canceled'),
        ]);

        return view('sections.orders.show', [
            'order' => $order,
            'statuses' => $statuses,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $order->update(['status' => $request->status]);

        if ($request->ajax()) {
            return response(null, 204);
        }

        return redirect()->route('orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
