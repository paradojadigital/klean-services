<?php

namespace App\Http\Controllers;

use App\User;
use Hash;
use App\Invitation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Underscore\Types\Arrays;
use App\Http\Requests\EditUserRequest;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, EditUserRequest $request)
    {
        return view('users.edit', [
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'password' => 'nullable|min:6',
            'image' => 'image|dimensions:min_width=500,min_height=500',
        ]);

        if ($request->password) {
            $request->validate([
                'new_password' => 'required_with:password|min:6|confirmed',
                'new_password_confirmation' => 'required_with:password',
            ]);
        }

        if ($request->new_password) {
            if (!Hash::check($request->password, $user->password)) {
                return redirect()->back()->withErrors(['password' => 'The current password is wrong.']);
            }

            $user->update(['password' => Hash::make($request->new_password)]);
        }

        $image = null;
        if ($request->image) {
            if ($request->hasFile('image') && $request->file('image')->isValid()) {
                $image = $request->image->store('public/avatars');
            }
        }

        $user->update(Arrays::merge($request->only([
            'first_name',
            'last_name',
        ]), [
            'image' => $image !== null ? $image : $user->image,
        ]));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *
     */
    public function complete ($token)
    {
        $invitation = Invitation::where('token', $token)->first();

        if (!$invitation) {
            return view('sections.complete', [
                'token' => null,
                'expired' => true,
            ]);
        }

        return view('sections.complete', [
            'user' => $invitation->user,
            'token' => $token,
            'expired' => Carbon::parse($invitation->expires_at) < Carbon::now(),
        ]);
    }

    /**
     *
     */
    public function completeProfile (Request $request, $token)
    {
        $invitation = Invitation::where('token', $token)->first();
        $user = $invitation->user;

        $request->validate([
            'password' => 'required|min:6|confirmed'
        ]);

        $user->update([
            'password' => Hash::make($request->password)
        ]);

        $invitation->delete();

        return redirect()->route('login');
    }
}
