<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use App\Branch;
use App\Order;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Requests\BranchRequest;
use Intervention\Image\ImageManagerStatic as Image;
use Underscore\Types\Arrays;
use Underscore\Types\Strings;
use App\Http\Requests\CompanyRequest;

class ClientCompanyController extends Controller
{
    /**
     *
     */
    public function index (BranchRequest $request, Branch $branch, Company $company)
    {
        $client = $company->load('country');

        return view('client.company.index', [
            'client' => $client,
            'branch' => $branch,
            'company' => $company,
            'country' => Country::find($client->country->id),
        ]);
    }

    /**
     *
     */
    public function updateCompany (Branch $branch, Company $company, Request $request)
    {
        $company->update($request->all());

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $manager = Image::configure(['driver' => 'gd']);
            $image = Image::make($request->file('image')->getRealPath())->fit(500, 500);
            $extension = $request->file('image')->guessExtension();

            $name = str_random('32').'.'.$extension;
            $image = $image->save(storage_path('app/public/products/'.$name));
            $image = Strings::replace($image->dirname.'/'.$name, storage_path('app/'), '');
            $company->update(['image' => $image]);
        }
        
        return redirect()->route('client.company.index', ['company' => $company, 'branch' => $branch]);
    }

    /**
     *
     */
    public function updateBranch (Branch $branch, Company $company, Request $request)
    {
        $branch->update($request->all());
        return redirect()->route('client.company.index', ['company' => $company, 'branch' => $branch]);
    }

    /**
     *
     */
    public function branches (Company $company, CompanyRequest $request)
    {
        $branches = $company->branches()->withCount(['orders' => function ($query) {
            $query->where('status', '!=', Order::STATUS_COMPLETED);
        }]);

        return view('client.company.branches', [
            'company' => $company,
            'branches' => $branches->get(),
        ]);
    }

    /**
     *
     */
    public function admins (Company $company, Branch $branch, CompanyRequest $request)
    {
        $users = $company->users()
            ->where(function ($query) {
                $query->where('company_has_user.role', Company::ROLE_OWNER);
                $query->orWhere('company_has_user.role', Company::ROLE_ADMIN);
            });

        if ($request->name) {
            $users = $users
                ->where('first_name', 'LIKE', '%'.$request->name.'%')
                ->orWhere('last_name', 'LIKE', '%'.$request->name.'%');
        }

        return view('client.company.admins', [
            'company' => $company,
            'users' => $users->get(),
            'name' => $request->name,
            'user' => new User(),
        ]);
    }

    /**
     *
     */
    public function createBranch (Company $company, CompanyRequest $request)
    {
        return view('client.company.branches-create', [
            'company' => $company,
            'branch' => new Branch(),
        ]);
    }

    /**
     *
     */
    public function storeBranch (Company $company, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'zipcode' => 'required|min:5',
            'neighborhood' => 'required',
            'street' => 'required',
            'number_ext' => 'required',
            // 'number_int' => 'required',
            'city' => 'required',
            'state' => 'required',
        ]);

        $branch = $company->branches()->create($request->all());

        return redirect()->route('client.company.branches', $company->id);
    }

    /**
     *
     */
    public function update (Company $company, Branch $branch, Request $request)
    {
        $branch->update($request->all());
        return redirect()->route('branches.orders', [$company->id, $branch->id]);
    }
}
