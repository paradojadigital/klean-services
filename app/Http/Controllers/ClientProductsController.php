<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Category;
use App\Mail\OrderReview;
use App\Notifications\OrderPendingNotification;
use App\Notifications\OrderReviewNotification;
use App\Product;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Requests\BranchRequest;
use Illuminate\Support\Facades\Mail;
use Notification;

class ClientProductsController extends Controller
{
    /**
     *
     */
    public function index (Branch $branch, BranchRequest $request)
    {
        return view('client.products.index', [
            'order' => $branch->orders->last()
        ]);
    }

    /**
     *
     */
    public function add (BranchRequest $request)
    {
        $products = $request->branch->products();

        if ($request->name) {
            $products = $products->where('name', 'LIKE', '%'.$request->name.'%');
        }

        if ($request->category_id) {
            $products = $products->where('category_id', '=', $request->category_id);
        }

        return view('client.products.add', [
            'quote' => $request->quote,
            'categories' => Category::all(),
            'products' => $products->get(),
            'name' => $request->name,
            'category_id' => $request->category_id,
            'product_ids' => $request->products
        ]);
    }

    /**
     *
     */
    public function checkout (BranchRequest $request)
    {
        $products = $request->branch->products()->whereIn(
            'id', $request->products ? explode(',', $request->products) : []
        );

        return view('client.products.checkout', [
            'quote' => $request->quote,
            'products' => $request->products ? explode(',', $request->products) : [],
            'product_models' => $products->get()
        ]);
    }

    /**
     * 
     */
    public function store (Request $request)
    {
        $order = $request->branch->orders()->create([
            'status' => 'new',
            'data' => [],
            'user_id' => $request->user()->id,
        ]);

        foreach($request->products as $product) {
            $p = $request->branch->products()->find($product['id']);
            $order->products()->attach($p->id, [
                'quantity' => $product['quantity'],
                'price' => $p->pivot->price,
            ]);
        }

        $total = 0;
        
        $order->products->each(function (Product $product) use (&$total) {
            $total += $product->pivot->quantity * $product->pivot->price;
        });

        $order->update([
            'status' => 'pending',
            'data' => $order->products()->get(['id', 'quantity'])->toArray(),
            'total' => $total
        ]);

        // Send a notification to KleanServices' Admins
        $admins = User::where('type', User::TYPE_ADMIN)->orWhere('type', User::TYPE_SUPERADMIN)->get();
        Notification::send($admins, new OrderPendingNotification($order));

        // And notify to the user making the order
        $request->user()->notify(new OrderReviewNotification($order));

        return redirect()->route('client.quote.complete', [
            'branch' => $request->branch->id,
        ]);
    }

    /**
     *
     */
    public function complete (Request $request)
    {
        return view('client.products.complete');
    }
}
