<?php

namespace App\Http\Controllers;

use App\Country;
use App\Order;
use App\Product;
use App\User;
use App\Company;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DatePeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index ()
    {
        // Get the data to chart
        $mx_data = Country::where('name_code', 'MX')->first()
            ->orders()
            ->where('created_at', '>', Carbon::now()->subYears(2)->setTime(23, 59, 59))
            ->where('status', Order::STATUS_COMPLETED)
            ->select(
                DB::raw('COUNT(*) as total'),
                DB::raw('DATE_FORMAT(created_at,\'%Y-%m-%d\') as day')
            )
            ->groupBy('day')
            ->pluck('total', 'day');

        $co_data = Country::where('name_code', 'CO')->first()
            ->orders()
            ->where('created_at', '>', Carbon::now()->subYears(2)->setTime(23, 59, 59))
            ->where('status', Order::STATUS_COMPLETED)
            ->select(
                DB::raw('COUNT(*) as total'),
                DB::raw('DATE_FORMAT(created_at,\'%Y-%m-%d\') as day')
            )
            ->groupBy('day')
            ->pluck('total', 'day');

        // Fill the holes
        $begin = Carbon::now()->subYears(2)->setTime(23, 59, 59);
        $end = Carbon::now();
        $interval = CarbonInterval::day();
        $period = new DatePeriod($begin, $interval, $end);

        foreach ($period as $day) {
            if (!$mx_data->has($day->format('Y-m-d'))) {
                $mx_data->put($day->format('Y-m-d'), 0);
            }

            if (!$co_data->has($day->format('Y-m-d'))) {
                $co_data->put($day->format('Y-m-d'), 0);
            }
        }

        // Flatten and order the data
        $mx_data = $mx_data->toArray();
        ksort($mx_data);
        $data = [];
        foreach ($mx_data as $key => $value) {
            array_push($data, [$key, $value]);
        }
        $mx_data = $data;

        $co_data = $co_data->toArray();
        ksort($co_data);
        $data = [];
        foreach ($co_data as $key => $value) {
            array_push($data, [$key, $value]);
        }
        $co_data = $data;

        // Statistic data
        $top_orders = Order::where('status', '!=', Order::STATUS_NEW)->orderBy('id', 'DESC')->limit(5)->get();
        
        $country_orders = Country::with(['companies.orders.products'])->get();
        $country_top_products = [];

        /**
         * Assign the count of products in the order of every country.
         * (with 4 nested foreach you can solve everything) Horritble.
         */
        foreach ($country_orders as $country) {
            $code = $country->name_code;
            $country_top_products[$code] = [];

            foreach ($country->companies as $company) {
                // Do nothing here (we need the products).

                foreach ($company->orders as $order) {
                    // Do nothing here (we need the products).
                    if (count($country_top_products[$code]) == 5) {
                        continue;
                    }

                    foreach ($order->products as $product) {
                        // Check if the array has the key (product_id), if does not 
                        // assign it.
                        if (!array_has($country_top_products[$code], $product->id)) {
                            $country_top_products[$code][$product->id] = [
                                'orders_count' => 1,
                                'detail' => $product
                            ];

                            continue;
                        }

                        // One more order count here.
                        $country_top_products[$code][$product->id]['orders_count'] += 1;
                    }
                }
            }
        }

        $top_products = Product::withCount(['orders' => function ($query) {
            $query->where('created_at', '>', Carbon::now()->subMonth()->setTime(23, 59, 59));
        }])->orderBy('orders_count', 'DESC')->limit(5)->get();

        $top_products = $top_products->filter(function ($product) {
            return !!$product->orders_count;
        });

        $mx_orders_count = Country::where('name_code', 'MX')->first()
            ->orders()
            ->where('status', Order::STATUS_COMPLETED)
            ->count();
        
        $co_orders_count = Country::where('name_code', 'CO')->first()
            ->orders()
            ->where('status', Order::STATUS_COMPLETED)
            ->count();

        $mx_total = Country::where('name_code', 'MX')
            ->first()
            ->orders()
            ->where('status', Order::STATUS_COMPLETED)
            ->select(DB::raw('SUM(total) as income'))
            ->first();

        $co_total = Country::where('name_code', 'CO')
            ->first()
            ->orders()
            ->where('status', Order::STATUS_COMPLETED)
            ->select(DB::raw('SUM(total) as income'))
            ->first();

        return view('sections.home', [
            'orders' => $top_orders,
            'products' => $top_products,
            'mx_orders_count' => $mx_orders_count,
            'co_orders_count' => $co_orders_count,
            'mx_total' => $mx_total,
            'co_total' => $co_total,
            'chart_data' => ['mx' => $mx_data, 'co' => $co_data],
            'country_top_products' => $country_top_products,
        ]);
    }

    public function branch (Request $request)
    {
        $user = $request->user();

        switch ($user->type) {
            case User::TYPE_SUPERADMIN:
            case User::TYPE_ADMIN:
                return redirect()->route('home');

            case User::TYPE_CLIENT:
                // Get the user company and branch
                $company = $user->companies->first() ?? $user->branches->first()->company;

                if (!$company) {
                    return abort(403, 'El usuario no tiene edificio ni compañia.');
                }

                $branch = $user->branches->first();

                if (!$branch) {
                    return redirect()->route('client.company.branches', [
                        'company' => $company->id
                    ]);
                }

                return redirect()->route('client.home', [
                    'branch' => $branch->id
                ]);

            case User::TYPE_STAFF:
                return abort(403, 'El usuario es de rol staff.');

            default:
                return abort(403, 'El usuario no tiene rol.');
        }
    }
}
