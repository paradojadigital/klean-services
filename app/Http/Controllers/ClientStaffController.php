<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Company;
use App\User;
use App\Invitation;
use Auth;
use Hash;
use Carbon\Carbon;
use App\Mail\InvitationMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\BranchRequest;
use Intervention\Image\ImageManagerStatic as Image;
use Underscore\Types\Arrays;
use Underscore\Types\Strings;
use Validator;

class ClientStaffController extends Controller
{
    /**
     *
     */
    public function index (BranchRequest $request, Company $company, Branch $branch)
    {
        $users = $request->branch->users();

        if ($request->name) {
            $users = $users
                ->where('first_name', 'LIKE', '%'.$request->name.'%')
                ->orWhere('last_name', 'LIKE', '%'.$request->name.'%')
                ->wherePivot('role', Branch::ROLE_STAFF);
        }

        $users = $users->get();
        $users = $users->merge($branch->kleanServicesStaff);

        return view('client.staff.index', [
            'users' => $users,
            'name' => $request->name,
            'branch' => $branch,
            'company' => $company,
            'user' => new User(),
        ]);
    }

    /**
     *
     */
    public function store (Request $request, Company $company, Branch $branch)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'sometimes|image',
            'email' => 'required|email',
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->with('modal', 1)
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::where('email', $request->email)->first();
        $is_new_user = !$user;
        $pass = str_random(16);

        if ($is_new_user) {
            $image = null;

            if ($request->hasFile('image') && $request->file('image')->isValid()) {
                $image = $request->image->store('public/avatars');
            }

            $user = User::create(Arrays::merge($request->all(), [
                'image' => $image,
                'type' => User::TYPE_CLIENT,
                'password' => Hash::make($pass)
            ]));
        } else {
            // The companies of the user belongs.
            $user_company = $user->companies->first() ?? $user->branches->first()->company;

            // The companies are different.
            if ($user_company->id != $company->id) {
                return redirect()
                    ->back()
                    ->with('modal', 1)
                    ->withInput()
                    ->withErrors([
                        'exists' => [
                            'Este edificio no pertenece a la compañia de este usuario. Solo se puede agregar este usuario a edificios de la misma compañia.'
                        ]
                    ]);
            }

            // If the user already exists, make sure it is not an admin
            if (Arrays::contains([User::TYPE_ADMIN, User::TYPE_SUPERADMIN, User::TYPE_STAFF], $user->type)) {
                return redirect()
                    ->back()
                    ->with('modal', 1)
                    ->withInput()
                    ->withErrors([
                        'email' => [
                            'Este correo pertenece a un administrador KleanServices',
                        ]
                    ]);
            }
        }

        $image = null;
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $manager = Image::configure(['driver' => 'gd']);
            $image = Image::make($request->file('image')->getRealPath())->fit(500, 500);
            $extension = $request->file('image')->guessExtension();

            $name = str_random('32').'.'.$extension;
            $image = $image->save(storage_path('app/public/avatars/'.$name));
            $image = Strings::replace($image->dirname.'/'.$name, storage_path('app/'), '');
        }

        // TODO. Do not let users update any user, only the ones that are already members on their company
        $user->update(Arrays::merge($request->all(), [
            'image' => $image ?? $user->image,
        ]));

        $request->branch->users()->syncWithoutDetaching([$user->id => [
            'role' => $request->role
        ]]);

        $route = 'branches.staff.index';
        if (Auth::user()->type == User::TYPE_ADMIN || Auth::user()->type == User::TYPE_SUPERADMIN) {
            $route = 'branches.members';
        }

        $token = null;
        if ($is_new_user) {
            $invitation = Invitation::create([
                'token' => str_random(32),
                'user_id' => $user->id,
                'expires_at' => Carbon::now()->addWeek(),
            ]);

            $token = $invitation->token;
        }

        // Send mail to the invited user.
        Mail::to($user->email)
            ->send(new InvitationMail($branch, $token));

        return redirect()->route($route, [
            'branch' => $branch,
            'company' => $company
        ]);
    }

    /**
     *
     */
    public function destroy (Request $request, Company $company, Branch $branch, User $user)
    {
        $route = 'branches.staff.index';

        if (in_array($request->user()->type, [User::TYPE_ADMIN, User::TYPE_SUPERADMIN])) {
            $route = 'branches.members';
        }

        if ($branch->users()->where('user_id', $user->id)->exists()) {
            $branch->users()->detach($user->id);
        }

        return redirect()->route($route, [
            'company' => $company,
            'branch' => $branch,
        ]);
    }
}
