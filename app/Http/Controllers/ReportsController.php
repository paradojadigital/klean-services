<?php

namespace App\Http\Controllers;

use DatePeriod;
use App\Order;
use App\Branch;
use App\Company;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function index (Request $request)
    {
        $companies = Company::all();
        $company = $companies->find($request->company_id);

        if (!$company) {
            return view('sections.reports.index', [
                'chart_data' => [],
                'orders' => collect([]),
                'companies' => $companies,
                'branches' => Branch::all(),
                'company_id' => $request->company_id,
                'branch_id' => $request->branch_id,
                'from' => null,
                'to' => null,
                'total' => collect([]),
            ]);
        }

        if ($request->branch_id == 'all') {
            $datas = [];
            $total = [];
            $orders = [];

            foreach ($company->branches as $branch) {
                array_push($datas, [
                    'name' => $branch->name,
                    'backgroundColor' => 'rgba(56, 91, 232, 0.2)',
                    'borderColor' => 'rgb(56, 91, 232)',
                    'data' => $this->fetchBranchData($branch),
                ]);

                array_push($orders, $branch->orders()
                    ->where('created_at', '>', Carbon::parse($request->from))
                    ->where('status', '!=', Order::STATUS_NEW)
                    ->orderBy('id', 'DESC')->get()
                );

                array_push($total, [
                    'name' => $branch->name,
                    'income' => $branch->orders()
                        ->where('status', Order::STATUS_COMPLETED)
                        ->select(DB::raw('SUM(total) as income'))
                        ->first()->income,
                ]);
            }

            array_unshift($total, [
                'name' => 'Total de compras',
                'income' => array_reduce(
                    array_map(function ($value) { return $value['income']; }, $total),
                    function ($a, $b) { return $a + $b; }
                )
            ]);

            return view('sections.reports.index', [
                'chart_data' => $datas,
                'orders' => collect($orders),
                'companies' => $companies,
                'branches' => Branch::all(),
                'company_id' => $request->company_id,
                'branch_id' => $request->branch_id,
                'from' => $request->from,
                'to' => $request->to,
                'total' => collect($total),
            ]);
        }

        $branch = Branch::find($request->branch_id);

        if (!$branch) {
            return view('sections.reports.index', [
                'chart_data' => [],
                'orders' => collect([]),
                'companies' => $companies,
                'branches' => Branch::all(),
                'company_id' => $request->company_id,
                'branch_id' => $request->branch_id,
                'from' => null,
                'to' => null,
                'total' => collect([]),
            ]);
        }

        $chart_data = [
            [
                'name' => $branch->name,
                'backgroundColor' => 'rgba(56, 91, 232, 0.2)',
                'borderColor' => 'rgb(56, 91, 232)',
                'data' => $this->fetchBranchData($branch),
            ]
        ];

        $total = $branch->orders()
            ->where('status', Order::STATUS_COMPLETED)
            ->select(DB::raw('SUM(total) as income'))
            ->first();
        
        $orders = $branch->orders()
            ->where('created_at', '>', Carbon::parse($request->from))
            ->where('status', '!=', Order::STATUS_NEW)
            ->orderBy('id', 'DESC')
            ->paginate(25);

        return view('sections.reports.index', [
            'chart_data' => $chart_data,
            'orders' => $orders,
            'companies' => $companies,
            'branches' => Branch::all(),
            'company_id' => $request->company_id,
            'branch_id' => $request->branch_id,
            'from' => $request->from,
            'to' => $request->to,
            'total' => collect([
                'name' => $branch->name,
                'income' => $total->income
            ]),
        ]);
    }


    /**
     *
     */
    public function fetchBranchData ($branch) {
        $chart_data = $branch->orders()
            ->where('created_at', '>', Carbon::now()->subYears(2)->setTime(23, 59, 59))
            ->where('status', '!=', Order::STATUS_NEW)
            ->select(
                DB::raw('COUNT(*) as orders'),
                DB::raw('SUM(total) as income'),
                DB::raw('DATE_FORMAT(created_at,\'%Y-%m-%d\') as day')
            )
            ->groupBy('day')
            ->get();

        $data = [];
        
        foreach ($chart_data as $cd) {
            $data[$cd->day] = [
                'orders' => $cd->orders,
                'income' => $cd->income,
                'day' => $cd->day,
            ];
        }
        
        $chart_data = $data;

        $begin = Carbon::now()->subYears(2)->setTime(23, 59, 59);
        $end = Carbon::now();
        $interval = CarbonInterval::day();
        $period = new DatePeriod($begin, $interval, $end);

        foreach ($period as $day) {
            if (!isset($chart_data[$day->format('Y-m-d')])) {
                $chart_data[$day->format('Y-m-d')] = [
                    'orders' => 0,
                    'income' => 0,
                    'day' => $day->format('Y-m-d'),
                ];
            }
        }

        ksort($chart_data);
        $data = [];
        
        foreach ($chart_data as $cd) {
            array_push($data, [
                'day' => $cd['day'],
                'orders' => $cd['orders'],
                'income' => $cd['income'],
            ]);
        }
        
        $chart_data = $data;

        return $chart_data;
    }
}
