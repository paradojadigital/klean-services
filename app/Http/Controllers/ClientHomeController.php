<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Order;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DatePeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use App\Http\Requests\BranchRequest;

class ClientHomeController extends Controller
{
    /**
     * 
     */
    public function index (Branch $branch, BranchRequest $request)
    {
        $chart_data = $branch->orders()
            ->where('created_at', '>', Carbon::now()->subYears(2)->setTime(23, 59, 59))
            ->where('status', Order::STATUS_COMPLETED)
            ->select(
                DB::raw('COUNT(*) as orders'),
                DB::raw('SUM(total) as income'),
                DB::raw('DATE_FORMAT(created_at,\'%Y-%m-%d\') as day')
            )
            ->groupBy('day')
            ->get();

        $data = [];
        foreach ($chart_data as $cd) {
            $data[$cd->day] = [
                'orders' => $cd->orders,
                'income' => $cd->income,
                'day' => $cd->day,
            ];
        }
        $chart_data = $data;

        $begin = Carbon::now()->subYears(2)->setTime(23, 59, 59);
        $end = Carbon::now();
        $interval = CarbonInterval::day();
        $period = new DatePeriod($begin, $interval, $end);

        foreach ($period as $day) {
            if (!isset($chart_data[$day->format('Y-m-d')])) {
                $chart_data[$day->format('Y-m-d')] = [
                    'orders' => 0,
                    'income' => 0,
                    'day' => $day->format('Y-m-d'),
                ];
            }
        }

        ksort($chart_data);
        $data = [];
        foreach ($chart_data as $cd) {
            array_push($data, [
                'day' => $cd['day'],
                'orders' => $cd['orders'],
                'income' => $cd['income'],
            ]);
        }
        $chart_data = $data;

        // Statistic data
        $top_orders = $branch->orders()->where('status', '!=', Order::STATUS_NEW)->orderBy('id', 'DESC')->limit(5)->get();
        $top_products = $branch->products()->withCount(['orders' => function ($query) {
            $query->where('created_at', '>', Carbon::now()->subMonth());
        }])->orderBy('orders_count', 'DESC')->limit(5)->get();

        $top_products = $top_products->filter(function ($product) {
            return !!$product->orders_count;
        });

        // Purchase data.
        $total = $branch->orders()
            ->where('status', Order::STATUS_COMPLETED)
            ->select(DB::raw('SUM(total) as income'))
            ->first();
        $total_month = $branch->orders()
            ->where('created_at', '>', Carbon::now()->startOfMonth())
            ->where('status', Order::STATUS_COMPLETED)
            ->select(DB::raw('SUM(total) as income'))
            ->first();
        $total_week = $branch->orders()
            ->where('created_at', '>', Carbon::now()->startOfWeek())
            ->where('status', Order::STATUS_COMPLETED)
            ->select(DB::raw('SUM(total) as income'))
            ->first();

        // Orders count data.
        $orders_count = $branch->orders()
            ->where('status', Order::STATUS_COMPLETED)
            ->count();
        $orders_count_month = $branch->orders()
            ->where('created_at', '>', Carbon::now()->startOfMonth())
            ->where('status', Order::STATUS_COMPLETED)
            ->count();
        $orders_count_week = $branch->orders()
            ->where('created_at', '>', Carbon::now()->startOfWeek())
            ->where('status', Order::STATUS_COMPLETED)
            ->count();

        return view('client.home', [
            'orders' => $top_orders,
            'products' => $top_products,

            // Orders.
            'orders_count' => $orders_count,
            'orders_count_month' => $orders_count_month,
            'orders_count_week' => $orders_count_week,

            // Purchase.
            'total' => $total->income,
            'total_month' => $total_month->income,
            'total_week' => $total_week->income,
            
            'chart_data' => $chart_data,
        ]);
    }

    /**
     * 
     */
    public function branch (Request $request)
    {
        if ($request->branch_id == 'all') {
            $user = $request->user();

            $company = $user->companies->first() ?? $user->branches->first()->company;
            $branches = $user->branches;

            return redirect()->route('client.company.branches', [
                'company' => $company
            ]);
        }

        $route_name = Route::getRoutes()
            ->match($request->create(URL::previous()))
            ->getName();

        $branch = Branch::find($request->branch_id);

        if ($route_name == 'client.products.add' || $route_name == 'client.products.checkout') {
            return redirect()->route('client.products.index', ['branch' => $branch->id]);
        }

        if ($route_name == 'client.company.branches' || $route_name == 'client.company.admins') {
            return redirect()->route('client.home', ['branch' => $branch->id]);
        }

        if ($route_name == 'branches.staff.index' || $route_name == 'client.company.index') {
            return redirect()->route($route_name, ['branch' => $branch->id, 'company' => $branch->company->id]);
        }

        if ($route_name) {
            return redirect()->route($route_name, ['branch' => $branch->id]);
        }

        return redirect()->route('home');
    }
}
