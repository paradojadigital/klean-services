<?php

namespace App\Http\Controllers;

use App\Notifications\BirthdayNotification;
use App\Notifications\OrderPendingNotification;
use App\Order;
use App\User;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    /**
     * List the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $notifications = $request->user()->notifications()->paginate(5);

        foreach ($notifications as $n) {
            switch ($n->type) {
                case BirthdayNotification::class:
                    $n->user = User::find($n->data['user_id']);
                    break;
                case OrderPendingNotification::class:
                    $n->order = Order::with(['branch.company'])->find($n->data['order_id']);
                    break;
            }
        }

        return $notifications;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function read(Request $request)
    {
        $request->user()->unreadNotifications->markAsRead();

        return response(null, 204);
    }
}
