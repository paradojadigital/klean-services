<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Requests\BranchRequest;

class ClientHistoryController extends Controller
{
    /**
     *
     */
    public function index (BranchRequest $request)
    {
        $orders = $request->branch->orders();

        if ($request->status) {
            $orders = $orders->where('status', $request->status);
        }

        if ($request->id) {
            $orders = $orders->where('id', $request->id);
        }

        return view('client.history.index', [
            'orders' => $orders->orderBy('id', 'DESC')->paginate(25),
            'id' => $request->id,
            'status' => $request->status,
        ]);
    }

    /**
     *
     */
    public function show (BranchRequest $request)
    {
        return view('client.history.show', [
            'order' => Order::find($request->order)
        ]);
    }
}
