<?php

namespace App\Http\Controllers;

use App\Company;
use App\Country;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Underscore\Types\Arrays;
use Underscore\Types\Strings;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Get the companies and their non-completed orders
        $companies = Company::withCount(['orders' => function ($query) {
            $query->where('status', '!=', Order::STATUS_COMPLETED);
        }]);

        if ($request->search) {
            $companies = $companies->where('name', 'LIKE', '%'.$request->search.'%');
        }

        return view('sections.clients.index', [
            'companies' => $companies->get(),
            'search' => $request->search,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sections.clients.create', [
            'countries' => Country::all(),
            'company' => new Company(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tin_format = "/[A-Za-z]{3,4}[ \-]{0,1}\d{6}[ \-]{0,1}[A-Za-z\d]{3}/";
        if ($request->country_id == Country::where('name_code', 'CO')->first()->id) {
            $tin_format = "/.*/";
        }

        $request->validate([
            'name' => 'required|min:3',
            'country_id' => 'required|exists:countries,id',
            'image' => 'image|dimensions:min_width=500,min_height=500',
            'tin' => 'required|regex:'.$tin_format,
            'business_name' => 'required',
            'zipcode' => 'required|min:5',
            'neighborhood' => 'required',
            'street' => 'required',
            'number_ext' => 'required',
            // 'number_int' => 'required',
            'city' => 'required',
            'state' => 'required',
        ]);

        $image = null;
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $manager = Image::configure(['driver' => 'gd']);
            $image = Image::make($request->file('image')->getRealPath())->fit(500, 500);
            $extension = $request->file('image')->guessExtension();

            $name = str_random('32').'.'.$extension;
            $image = $image->save(storage_path('app/public/products/'.$name));
            $image = Strings::replace($image->dirname.'/'.$name, storage_path('app/'), '');
        }

        $company = Company::create(Arrays::merge($request->all(), [
            'image' => $image,
        ]));

        return redirect()->route('companies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('sections.clients.create', [
            'countries' => Country::all(),
            'company' => $company,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $company->update($request->all());

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $manager = Image::configure(['driver' => 'gd']);
            $image = Image::make($request->file('image')->getRealPath())->fit(500, 500);
            $extension = $request->file('image')->guessExtension();

            $name = str_random('32').'.'.$extension;
            $image = $image->save(storage_path('app/public/products/'.$name));
            $image = Strings::replace($image->dirname.'/'.$name, storage_path('app/'), '');
            $company->update(['image' => $image]);
        }
        
        return redirect()->route('branches.index', ['company' => $company->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *
     */
    public function admins (Company $company, Request $request)
    {
        $users = $company->users()
            ->where(function ($query) {
                $query->where('company_has_user.role', Company::ROLE_OWNER);
                $query->orWhere('company_has_user.role', Company::ROLE_ADMIN);
            });

        if ($request->name) {
            $users = $users
                ->where('first_name', 'LIKE', '%'.$request->name.'%')
                ->orWhere('last_name', 'LIKE', '%'.$request->name.'%');
        }

        return view('sections.clients.admins', [
            'company' => $company,
            'users' => $users->get(),
            'name' => $request->name,
            'user' => new User(),
        ]);
    }
}
