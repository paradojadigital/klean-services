<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use App\Invitation;
use Auth;
use Hash;
use Carbon\Carbon;
use App\Mail\InvitationMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\ImageManagerStatic as Image;
use Underscore\Types\Arrays;
use Underscore\Types\Strings;
use Validator;

class ClientAdminController extends Controller
{
    /**
     *
     */
    public function index (Company $company, Request $request)
    {
        // 
    }

    /**
     *
     */
    public function store (Company $company, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'sometimes|image',
            'email' => 'required|email',
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->with('modal', 1)
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::where('email', $request->email)->first();
        $is_new_user = !$user;
        $pass = str_random(16);

        if ($is_new_user) {
            $image = null;

            if ($request->hasFile('image') && $request->file('image')->isValid()) {
                $image = $request->image->store('public/avatars');
            }

            // TODO: check if the company has an owner, if not, save it as owner
            // otherwise save it as admin of the company.
            $user = User::create(Arrays::merge($request->all(), [
                'image' => $image,
                'type' => User::TYPE_CLIENT,
                'password' => Hash::make($pass)
            ]));
        }

        if ($user->companies->count()) {
            return redirect()
                ->back()
                ->with('modal', 1)
                ->withInput()
                ->withErrors([
                    'exists' => [
                        'No puedes agregar al usuario a esta empresa debido a que: el usuario ya pertenece a una empresa (puede ser que sea la misma empresa a la que se esta agregando)'
                    ]
                ]);
        }

        if (Arrays::contains([User::TYPE_ADMIN, User::TYPE_SUPERADMIN, User::TYPE_STAFF], $user->type)) {
            return redirect()
                ->back()
                ->with('modal', 1)
                ->withInput()
                ->withErrors([
                    'email' => [
                        'El email le pertenece a un admin o staff de ks, no puede ser agregado'
                    ]
                ]);
        }

        // TODO. Add security, do not let users update any user, only the ones that are already members
        $image = null;
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $manager = Image::configure(['driver' => 'gd']);
            $image = Image::make($request->file('image')->getRealPath())->fit(500, 500);
            $extension = $request->file('image')->guessExtension();

            $name = str_random('32').'.'.$extension;
            $image = $image->save(storage_path('app/public/avatars/'.$name));
            $image = Strings::replace($image->dirname.'/'.$name, storage_path('app/'), '');
        }

        $user->update(Arrays::merge($request->all(), [
            'image' => $image ?? $user->image,
        ]));

        $role = Company::ROLE_OWNER;
        $owner = $company->users()
            ->wherePivot('role', Company::ROLE_OWNER)
            ->first();

        // Check if the company has an owner, if it's not the case add
        // the first one as an owner.
        if ($owner) {
            $role = Company::ROLE_ADMIN;
        }

        $company->users()->syncWithoutDetaching([$user->id => [
            'role' => $role
        ]]);

        $route = 'client.company.admins';
        if (Auth::user()->type == User::TYPE_ADMIN || Auth::user()->type == User::TYPE_SUPERADMIN) {
            $route = 'companies.admins';
        }

        $token = null;
        if ($is_new_user) {
            $invitation = Invitation::create([
                'token' => str_random(32),
                'user_id' => $user->id,
                'expires_at' => Carbon::now()->addWeek(),
            ]);

            $token = $invitation->token;
        }

        // Send mail to the invited user.
        Mail::to($user->email)
            ->send(new InvitationMail($company, $token));

        return redirect()->route($route, [
            'company' => $company->id,
        ]);
    }

    /**
     *
     */
    public function destroy (Company $company, User $user, Request $request)
    {
        if ($user && $user->type == User::TYPE_CLIENT) {
            $user->companies()->detach($company->id);
        }

        $route = 'client.company.admins';
        if (Auth::user()->type == User::TYPE_ADMIN || Auth::user()->type == User::TYPE_SUPERADMIN) {
            $route = 'companies.admins';
        }

        return redirect()->route($route, [
            'company' => $company->id
        ]);
    }
}
