<?php

namespace App\Http\Controllers;

use App\Branch;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DatePeriod;
use App\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\BranchRequest;

class ClientReportsController extends Controller
{
    public function index (Branch $branch, BranchRequest $request)
    {
        $selected_branch = $branch;

        if ($request->selected_branch) {
            $selected_branch = Branch::find($request->selected_branch);
        }

        $chart_data = $selected_branch->orders()
            ->where('created_at', '>', Carbon::now()->subYears(2)->setTime(23, 59, 59))
            ->where('status', '!=', Order::STATUS_NEW)
            ->select(
                DB::raw('COUNT(*) as orders'),
                DB::raw('SUM(total) as income'),
                DB::raw('DATE_FORMAT(created_at,\'%Y-%m-%d\') as day')
            )
            ->groupBy('day')
            ->get();

        $data = [];
        foreach ($chart_data as $cd) {
            $data[$cd->day] = [
                'orders' => $cd->orders,
                'income' => $cd->income,
                'day' => $cd->day,
            ];
        }
        $chart_data = $data;

        $begin = Carbon::now()->subYears(2)->setTime(23, 59, 59);
        $end = Carbon::now();
        $interval = CarbonInterval::day();
        $period = new DatePeriod($begin, $interval, $end);

        foreach ($period as $day) {
            if (!isset($chart_data[$day->format('Y-m-d')])) {
                $chart_data[$day->format('Y-m-d')] = [
                    'orders' => 0,
                    'income' => 0,
                    'day' => $day->format('Y-m-d'),
                ];
            }
        }

        ksort($chart_data);
        $data = [];
        foreach ($chart_data as $cd) {
            array_push($data, [
                'day' => $cd['day'],
                'orders' => $cd['orders'],
                'income' => $cd['income'],
            ]);
        }
        $chart_data = $data;

        $orders = $selected_branch->orders()
            ->where('created_at', '>', Carbon::parse($request->from))
            ->where('status', '!=', Order::STATUS_NEW)
            ->orderBy('id', 'DESC')
            ->paginate(25);

        return view('client.reports.index', [
            'chart_data' => $chart_data,
            'orders' => $orders,
            'selected_branch' => $selected_branch,
            'from' => $request->from,
            'to' => $request->to,
        ]);
    }
}
