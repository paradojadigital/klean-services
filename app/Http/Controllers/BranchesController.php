<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Company;
use App\Order;
use App\User;
use Illuminate\Http\Request;

class BranchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Company $company)
    {
        // Get the companies and their non-completed orders
        $branches = $company->branches()->withCount(['orders' => function ($query) {
            $query->where('status', '!=', Order::STATUS_COMPLETED);
        }]);

        return view('sections.buildings.index', [
            'company' => $company,
            'branches' => $branches->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Company $company)
    {
        return view('sections.buildings.create', [
            'branch' => new Branch(),
            'company' => $company,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Company $company)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'zipcode' => 'required|min:5',
            'neighborhood' => 'required',
            'street' => 'required',
            'number_ext' => 'required',
            // 'number_int' => 'required',
            'city' => 'required',
            'state' => 'required',
        ]);

        $branch = $company->branches()->create($request->all());

        return redirect()->route('branches.index', $company->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company, Branch $branch)
    {
        return view('sections.buildings.create', [
            'branch' => $branch,
            'company' => $company,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company, Branch $branch)
    {
        $branch->update($request->all());

        return redirect()->route('branches.orders', [$company->id, $branch->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *
     */
    public function orders(Request $request, Company $company, Branch $branch)
     {
        $orders = $branch->orders();

        if ($request->status) {
            $orders = $orders->where('status', $request->status);
        }

        if ($request->id) {
            $orders->find($request->id);
        }

        return view('sections.buildings.orders', [
            'company' => $company,
            'branch' => $branch,
            'status' => $request->status,
            'id' => $request->id,
            'orders' => $orders
                ->orderBy('id', 'DESC')
                ->paginate(25),
        ]);
    }

    /**
     *
     */
    public function members (Company $company, Branch $branch, Request $request)
    {
        $users = $branch->users();

        if ($request->name) {
            $users = $users
                ->where('first_name', 'LIKE', '%'.$request->name.'%')
                ->orWhere('last_name', 'LIKE', '%'.$request->name.'%')
                ->wherePivot('role', Branch::ROLE_STAFF);
        }

        $users = $users->get();
        $users = $users->merge($branch->kleanServicesStaff);

        return view('sections.buildings.members', [
            'company' => $company,
            'branch' => $branch,
            'users' => $users,
            'name' => $request->name,
            'user' => new User(),
        ]);
    }
}
