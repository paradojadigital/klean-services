<?php

namespace App\Http\Controllers;

use App\Category;
use App\Country;
use App\Product;
use App\Company;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Underscore\Types\Arrays;
use Underscore\Types\Strings;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::with('countries');

        if ($request->name) {
            $products = $products->where('name', 'LIKE', '%'.$request->name.'%');
        }

        if ($request->category) {
            $products = $products->where('category_id', '=', $request->category);
        }

        return view('sections.products.index', [
            'products' => $products->paginate(25),
            'categories' => Category::all(),
            'selected' => $request->category,
            'name' => $request->name,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = new Product();

        return view('sections.products.create', [
            'product' => $product,
            'companies' => Company::with('country')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'sku' => 'nullable|min:3',
            'name' => 'required|min:3',
            'price_mxn' => 'nullable|numeric|min:1',
            'price_cop' => 'nullable|numeric|min:1',
            'description' => 'nullable',
            'category_id' => 'required|exists:categories,id',
            'image' => 'image|dimensions:min_width=500,min_height=500',
            'companies' => 'array',
            'companies.*' => 'exists:companies,id'
        ]);

        $image = null;
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $manager = Image::configure(['driver' => 'gd']);
            $image = Image::make($request->file('image')->getRealPath())->fit(500, 500);
            $extension = $request->file('image')->guessExtension();

            $name = str_random('32').'.'.$extension;
            $image = $image->save(storage_path('app/public/products/'.$name));
            $image = Strings::replace($image->dirname.'/'.$name, storage_path('app/'), '');
        }

        $product = Product::create(Arrays::merge($request->only([
            'name',
            'category_id'
        ]), [
            'image' => $image,
            'description' => $request->description ?? '',
            'sku' => $request->sku ?? str_random('8'),
        ]));

        // Attach prices if provided
        if ($request->price_mxn) {
            $product->prices()->attach(Country::where('currency_code', 'MXN')->first(), [
                'price' => $request->price_mxn,
            ]);
        }

        if ($request->price_cop) {
            $product->prices()->attach(Country::where('currency_code', 'COP')->first(), [
                'price' => $request->price_cop,
            ]);
        }

        // Hide for selected companies
        if ($request->companies) {
            $product->hiddenOnCompanies()->sync($request->companies);
        }

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('sections.products.create', [
            'product' => $product,
            'companies' => Company::with('country')->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'sku' => 'nullable|min:3',
            'name' => 'min:3',
            'price_mxn' => 'nullable|numeric|min:1',
            'price_cop' => 'nullable|numeric|min:1',
            'description' => 'nullable',
            'category_id' => 'exists:categories,id',
            'image' => 'image|dimensions:min_width=500,min_height=500',
            'companies' => 'array',
            'companies.*' => 'exists:companies,id'
        ]);

        $product->update($request->all());

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $manager = Image::configure(['driver' => 'gd']);
            $image = Image::make($request->file('image')->getRealPath())->fit(500, 500);
            $extension = $request->file('image')->guessExtension();

            $name = str_random('32').'.'.$extension;
            $image = $image->save(storage_path('app/public/products/'.$name));
            $image = Strings::replace($image->dirname.'/'.$name, storage_path('app/'), '');

            $product->update(['image' => $image]);
        }

        // Attach prices if provided
        if ($request->price_mxn) {
            $mxn = Country::where('currency_code', 'MXN')->first();
            
            if ($product->prices()->where('country_id', $mxn->id)->exists()) {
                $product->prices()->updateExistingPivot($mxn->id, [
                    'price' => $request->price_mxn,
                ]);
            } else {
                $product->prices()->attach($mxn->id, [
                    'price' => $request->price_mxn,
                ]);
            }
        }
        if ($request->price_cop) {
            $cop = Country::where('currency_code', 'COP')->first();

            if ($product->prices()->where('country_id', $cop->id)->exists()) {
                $product->prices()->updateExistingPivot($cop->id, [
                    'price' => $request->price_cop,
                ]);
            } else {
                $product->prices()->attach($cop->id, [
                    'price' => $request->price_cop,
                ]);
            }
        }

        // Hide for selected companies
        $product->hiddenOnCompanies()->sync($request->companies);

        // If the request is ajax, do not redirect, just return an OK status
        if ($request->ajax()) {
            return response(null, 204);
        }

        // return redirect()->route('products.show', ['product' => $product]);
        // WARNING: I think it's better to return the user to see all the products.
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
