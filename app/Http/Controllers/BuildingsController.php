<?php

namespace App\Http\Controllers;

use Hash;
use Validator;
use App\Branch;
use App\Company;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Underscore\Types\Arrays;

class BuildingsController extends Controller
{
    // /**
    //  *
    //  */
    // public function members (Company $company, Branch $branch, Request $request)
    // {
    //     $users = $branch->users();

    //     if ($request->name) {
    //         $users = $users
    //             ->where('first_name', 'LIKE', '%'.$request->name.'%')
    //             ->orWhere('last_name', 'LIKE', '%'.$request->name.'%')
    //             ->wherePivot('role', Branch::ROLE_STAFF);
    //     }

    //     return view('sections.buildings.members', [
    //         'company' => $company,
    //         'branch' => $branch,
    //         'users' => $users->get(),
    //         'name' => $request->name,
    //     ]);
    // }

    // /**
    //  *
    //  */
    // public function deleteMember (Company $company, Branch $branch, Request $request)
    // {
    //     $user = User::find($request->user);

    //     if ($user && $user->type == User::TYPE_CLIENT) {
    //         $user->branches()->detach($branch->id);
    //     }

    //     return redirect()->route('buildings.members', [
    //         'company' => $company,
    //         'branch' => $branch,
    //         'users' => $branch->users,
    //     ]);
    // }

    // /**
    //  *
    //  */
    // public function storeMember (Company $company, Branch $branch, Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'image' => 'required|image',
    //         'email' => 'required|email',
    //         'first_name' => 'required|min:3',
    //         'last_name' => 'required|min:3'
    //     ]);

    //     if ($validator->fails()) {
    //         return redirect()
    //             ->route('buildings.members', [
    //                 'company' => $company->id,
    //                 'branch' => $branch->id,
    //                 'users' => $branch->users
    //             ])
    //             ->with('modal', 1)
    //             ->withErrors($validator)
    //             ->withInput();
    //     }

    //     $user = User::where('email', $request->email)->first();

    //     if (!$user) {
    //         $image = null;
    //         $pass = str_random(8);

    //         if ($request->hasFile('image') && $request->file('image')->isValid()) {
    //             $image = $request->image->store('public/avatars');
    //         }

    //         $user = User::create(Arrays::merge($request->all(), [
    //             'image' => $image,
    //             'type' => User::TYPE_CLIENT,
    //             'password' => Hash::make($pass)
    //         ]));
    //     }

    //     $branch->users()->syncWithoutDetaching([$user->id => [
    //         'role' => $request->role
    //     ]]);

    //     // TODO: send mail to the created user (and his password).
    //     return redirect()->route('buildings.members', [
    //         'company' => $company->id,
    //         'branch' => $branch->id,
    //         'users' => $branch->users
    //     ]);
    // }


    // /**
    //  *
    //  */
    // public function updateMember (Company $company, Branch $branch, Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'email' => 'required|email',
    //         'first_name' => 'required|min:3',
    //         'last_name' => 'required|min:3'
    //     ]);

    //     if ($validator->fails()) {
    //         return redirect()
    //             ->route('buildings.members', [
    //                 'company' => $company->id,
    //                 'branch' => $branch->id,
    //                 'users' => $branch->users
    //             ])
    //             ->with('modal', 1)
    //             ->withErrors($validator)
    //             ->withInput();
    //     }

    //     $user = User::where('email', $request->email)->first();
    //     $image = null;
    //     $data = [
    //         'first_name' => $request->first_name,
    //         'last_name' => $request->last_name,
    //         'email' => $request->email,
    //     ];

    //     if ($request->hasFile('image') && $request->file('image')->isValid()) {
    //         $image = $request->image->store('public/avatars');
    //     }

    //     // Only update the image if has one.
    //     if ($image) {
    //         $data = Arrays::merge($data, ['image' => $image]);
    //     }

    //     $user->update($data);

    //     $branch->users()->updateExistingPivot($user->id, [
    //         'role' => $request->role
    //     ]);

    //     return redirect()->route('buildings.members', [
    //         'company' => $company->id,
    //         'branch' => $branch->id,
    //         'users' => $branch->users,
    //     ]);
    // }
}
