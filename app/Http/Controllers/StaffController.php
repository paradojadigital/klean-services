<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Company;
use App\Http\Requests\StaffUpdateRequest;
use App\User;
use Hash;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Underscore\Types\Arrays;
use Underscore\Types\Strings;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $admins = User::where('type', User::TYPE_ADMIN);
        $staff = User::where('type', User::TYPE_STAFF);
        
        if ($request->name) {
            $admins = $admins
                ->where('first_name', 'LIKE', '%'.$request->name.'%')
                ->orWhere('last_name', 'LIKE', '%'.$request->name.'%');

            $staff = $staff
                ->where('first_name', 'LIKE', '%'.$request->name.'%')
                ->orWhere('last_name', 'LIKE', '%'.$request->name.'%');
        }

        return view('sections.staff.index', [
            'admins' => $admins->get(),
            'staff' => $staff->get(),
            'name' => $request->name,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sections.staff.create', [
            'companies' => Company::all(),
            'branches' => Branch::with('company')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'birthdate' => 'nullable|date',
            'image' => 'image|dimensions:min_width=500,min_height=500',
        ];

        if ($request->email) {
            $rules = Arrays::merge($rules, ['email' => 'required|email']);
        }

        $request->validate($rules);

        $image = null;
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $image = $request->image->store('public/avatars');
        }

        // Assign data depending of the role.
        $data = $request->only([
            'first_name', 'last_name', 'image', 'role'
        ]);

        if ($request->role == User::TYPE_ADMIN) {
            $data = Arrays::merge($data, [
                'email' => $request->email,
                'type' => User::TYPE_ADMIN,
            ]);
        }

        if ($request->role == User::TYPE_STAFF) {
            $data = Arrays::merge($data, [
                'branch_ids' => $request->branch_ids,
                'birthdate' => $request->birthdate,
                'type' => User::TYPE_STAFF,
                'email' => str_random(16).'@kleanservices.com',
                'password' => Hash::make(str_random(16)),
            ]);
        }

        $user = User::create(Arrays::merge($data, [
            'image' => $image,
        ]));

        $branches = [];

        if ($request->branch_ids) {
            $branches = $request->branch_ids;
        }

        // WARNING: I don't know what I'm doing.
        $user->staffing()->sync($branches);

        // TODO: send mail invitation.
        return redirect()->route('staff.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('sections.staff.create', [
            'companies' => Company::all(),
            'branches' => Branch::with('company')->get(),
            'user' => User::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StaffUpdateRequest $request, User $user)
    {
        $image = null;
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $manager = Image::configure(['driver' => 'gd']);
            $image = Image::make($request->file('image')->getRealPath())->fit(500, 500);
            $extension = $request->file('image')->guessExtension();

            $name = str_random('32').'.'.$extension;
            $image = $image->save(storage_path('app/public/avatars/'.$name));
            $image = Strings::replace($image->dirname.'/'.$name, storage_path('app/'), '');
        }

        // Assign data depending of the role.
        $data = $request->only([
            'first_name', 'last_name', 'image', 'role'
        ]);

        if ($request->role == User::TYPE_ADMIN) {
            $data = Arrays::merge($data, [
                'email' => $request->email,
                'type' => User::TYPE_ADMIN,
            ]);
        }

        if ($request->role == User::TYPE_STAFF) {
            $data = Arrays::merge($data, [
                'branch_ids' => $request->branch_ids,
                'birthdate' => $request->birthdate,
                'email' => str_random(16).'@kleanservices.com',
                'password' => Hash::make(str_random(16)),
                'type' => User::TYPE_STAFF,
            ]);
        }
        
        $data = Arrays::merge($data, [
            'image' => $image !== null ? $image : $user->image,
        ]);

        $user->update($data);

        // WARNING: I don't know what I'm doing.
        $user->staffing()->sync($request->branch_ids);

        // TODO: send mail invitation.
        return redirect()->route('staff.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user && $user->type === User::TYPE_STAFF) {
            $user->staffing()->detach();
            $user->delete();
        }
        
        return redirect()->route('staff.index');
    }
}
