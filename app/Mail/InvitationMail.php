<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvitationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     */
    public $branch;
    public $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($branch, $token)
    {
        $this->branch = $branch;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.invitation')
            ->from(env('MAIL_USERNAME'))
            ->subject('Invitación de Central Work & Shop');
    }
}
