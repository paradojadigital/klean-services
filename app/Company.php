<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Underscore\Types\Strings;

class Company extends Model
{
    const ROLE_OWNER = 'owner';
    const ROLE_ADMIN = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'country_id',
        'business_name',
        'tin',
        'zipcode',
        'neighborhood',
        'street',
        'number_ext',
        'number_int',
        'city',
        'state',
        'image',
    ];

    /**
     *
     */
    public function branches(): HasMany
    {
        return $this->hasMany(Branch::class);
    }

    /**
     *
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'company_has_user')->withPivot([
            'role',
        ]);
    }

    /**
     *
     */
    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    /**
     *
     */
    public function orders(): HasManyThrough
    {
        return $this->hasManyThrough(Order::class, Branch::class);
    }

    /**
     *
     */
    public function products(): BelongsToMany
    {
        $hidden = $this->hiddenProducts()->pluck('id');
        return $this->country->products()->where('visible', true)->whereNotIn('id', $hidden);
    }

    /**
     *
     */
    public function hiddenProducts(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_hidden_for_company');
    }

    /**
     *
     */
    public function getImageAttribute($image)
    {
        if (!$image) {
            return asset('storage/company-avatar.svg');
        }
        
        if (Strings::startsWith($image, 'http')) {
            return $image;
        }

        return asset(Strings::replace($image, 'public', 'storage'));
    }
}
