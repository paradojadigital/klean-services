<?php

namespace App;

use App\Company;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Country extends Model
{
	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_code',
        'currency_name',
        'currency_code',
        'currency_symbol',
        'image',
    ];

    /**
     *
     */
    public function companies(): HasMany
    {
    	return $this->hasMany(Company::class);
    }

    /**
     *
     */
    public function orders(): Builder
    {
        $ids = Company::where('country_id', $this->id)
            ->whereHas('branches')
            ->with('branches')
            ->get()
            ->pluck('branches')
            ->flatten()
            ->pluck('id');
        
        return Order::whereIn('branch_id', $ids);
    }

    /**
     *
     */
    public function getOrdersAttribute()
    {
        return $this->orders()->get();
    }

    /**
     *
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'country_has_product', 'country_id', 'product_id')
            ->withPivot(['price']);
    }

    /**
     *
     */
    public function getVatAttribute()
    {
        switch ($this->name_code) {
            case 'MX':
                return 0.16;
            case 'CO':
                return 0;
        }

        throw new \Exception('Uknown country VAT');
    }
}
