<?php

namespace App\Console\Commands;

use App\Notifications\BirthdayNotification;
use App\User;
use Illuminate\Console\Command;
use Notification;

class SendBirthdayNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'birthday:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends notifications to admins saying who\'s birthday is today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('type', User::TYPE_STAFF)
            ->whereRaw('DAYOFYEAR(curdate()) = DAYOFYEAR(birthdate)')
            ->get();
        $admins = User::where('type', User::TYPE_ADMIN)->orWhere('type', User::TYPE_SUPERADMIN)->get();

        foreach ($users as $user) {
            Notification::send($admins, new BirthdayNotification($user));
        }
    }
}
