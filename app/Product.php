<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Underscore\Types\Strings;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'sku',
        'name',
        'description',
        'image',
        'visible',
    ];

    /**
     *
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Alias of @Product::countries()
     */
    public function prices(): BelongsToMany
    {
        return $this->countries();
    }

    /**
     *
     */
    public function countries(): BelongsToMany
    {
        return $this->belongsToMany(Country::class, 'country_has_product')->withPivot([
            'price',
        ]);
    }

    /**
     *
     */
    public function hiddenOnCompanies(): BelongsToMany
    {
        return $this->belongsToMany(Company::class, 'product_hidden_for_company');
    }

    /**
     *
     */
    public function getPrice(string $country): ?float
    {
        $country = $this->countries()->whereCurrencyCode($country)->first();
        return $country ? $country->pivot->price : null;
    }

    /**
     *
     */
    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(Order::class, 'order_has_product')->withPivot([
            'quantity',
            'price',
        ]);
    }

    /**
     *
     */
    public function getImageAttribute($image)
    {
        if (!$image) {
            return null;
        }
        
        if (Strings::startsWith($image, 'http')) {
            return $image;
        }

        return asset(Strings::replace($image, 'public', 'storage'));
    }
}
