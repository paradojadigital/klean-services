<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends Model
{
    const STATUS_NEW = 'new';
    const STATUS_PENDING = 'pending';
    const STATUS_PAYED = 'payed';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_COMPLETED = 'completed';
    const STATUS_CANCELED = 'canceled';

    const ALL_STATUS = [
        Order::STATUS_NEW,
        Order::STATUS_PENDING,
        Order::STATUS_PAYED,
        Order::STATUS_DELIVERED,
        Order::STATUS_COMPLETED,
        Order::STATUS_CANCELED,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'data', 'user_id', 'total',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array'
    ];

    /**
     *
     */
    public function branch(): BelongsTo
    {
        return $this->belongsTo(Branch::class);
    }

    /**
     *
     */
    public function company(): BelongsTo
    {
        return $this->branch->company();
    }

    /**
     *
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     *
     */
    public function products(): BelongsToMany
    {
    	return $this->belongsToMany(Product::class, 'order_has_product')->withPivot([
    		'quantity',
    		'price',
    	]);
    }

    /**
     * Returns a css class based on the status of this order
     */
    public function getStatusClassAttribute(): String
    {
        // switch ($this->status) {
        //     case self::STATUS_NEW:
        //         return 'new';
        //     case self::STATUS_PENDING:
        //         return 'pending';
        //     case self::STATUS_PAYED:
        //         return 'payed';
        //     case self::STATUS_DELIVERED:
        //         return 'delivered';
        //     case self::STATUS_COMPLETED:
        //         return 'completed';
        //     case self::STATUS_COMPLETED:
        //         return 'canceled';
        // }
        
        return $this->status;
    }

    /**
     *
     */
    public function getVatAttribute()
    {
        return $this->company->country->vat;
    }
}
