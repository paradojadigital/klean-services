<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Underscore\Types\Strings;

class User extends Authenticatable
{
    use Notifiable;

    const TYPE_SUPERADMIN = 'superadmin';
    const TYPE_ADMIN = 'admin';
    const TYPE_CLIENT = 'client';
    const TYPE_STAFF = 'staff';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'type',
        'first_name',
        'last_name',
        'birthdate',
        'image',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'birthdate'
    ];

    /**
     *
     */
    public function staffing(): BelongsToMany
    {
        return $this->belongsToMany(Branch::class, 'branch_has_staff');
    }

    /**
     *
     */
    public function companies(): BelongsToMany
    {
        return $this->belongsToMany(Company::class, 'company_has_user')->withPivot([
            'role'
        ]);
    }

    /**
     *
     */
    public function branches(): BelongsToMany
    {
        return $this->belongsToMany(Branch::class, 'branch_has_user')->withPivot([
            'role'
        ]);
    }

    /**
     *
     */
    public function getImageAttribute($image)
    {
        if (!$image) {
            return null;
        }
        
        if (Strings::startsWith($image, 'http')) {
            return $image;
        }

        return asset(Strings::replace($image, 'public', 'storage'));
    }

    /**
     * Returns the user's full name
     */
    public function getFullNameAttribute()
    {
        return $this->getNameAttribute();
    }

    /**
     * Returns the user's full name
     */
    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * Returns the user's full name
     */
    public function getInitialsAttribute()
    {
        return strtoupper($this->first_name[0].$this->last_name[0]);
    }
}
