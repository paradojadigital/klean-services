<?php

namespace App\Providers;

use App\Order;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Blade::directive('strToHex', function ($str) {
            return "<?php echo '#'.substr(md5($str), 0, 6); ?>";
        });

        //
        View::share('currentCompany', function () {
            $user = request()->user();
            $company = $user->companies->first() ?? $user->branches->first()->company;

            if (!$company) {
                return null;
            }

            return $company;
        });

        //
        View::share('allBranches', function (): Collection {
            $user = request()->user();
            $company = $user->companies->first() ?? $user->branches->first()->company;

            if (!$company) {
                return collect([]);
            }

            return $company->branches;
        });

        //
        View::share('currentBranch', function () {
            $branch = request()->branch;

            if (!$branch) {
                return null;
            }

            return $branch;
        });

        //
        View::share('pendingOrders', function (): int {
            return Order::whereNotIn('status', [
                Order::STATUS_COMPLETED,
                Order::STATUS_NEW,
                Order::STATUS_CANCELED
            ])->count();
        });

        // 
        View::share('branchPendingOrders', function (): int {
            $branch = request()->branch;

            if (!$branch) {
                return 0;
            }

            return $branch->orders()->where('status', '!=', Order::STATUS_COMPLETED)->count();
        });

        // 
        View::share('roleCompany', function () {
            $user = request()->user();
            $branch = request()->branch;
            $company = $user->companies->find($branch->company->id);

            if (!$company) {
                return null;
            }

            return $company->pivot->role;
        });

        // 
        View::share('roleBranch', function () {
            $user = request()->user();
            $branch = $user->branches->find(request()->branch->id);

            if (!$branch) {
                return null;
            }

            return $branch->pivot->role;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
