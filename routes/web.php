<?php

use App\Http\Controllers\UsersController;
use App\Http\Controllers\admins;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::resource('users', 'UsersController', ['only' => ['edit', 'update']]);
    Route::resource('companies/{company}/branch/{branch}/staff', 'ClientStaffController', ['names' => 'branches.staff', 'parameters' => ['staff' => 'user']]);
    Route::resource('companies/{company}/admin', 'ClientAdminController', ['only' => ['store', 'destroy'], 'names' => 'company.staff', 'parameters' => ['admin' => 'user']]);

    // Company (branches).
    Route::get('/company/{company}/branches', 'ClientCompanyController@branches')->name('client.company.branches');
    Route::get('/company/{company}/admins', 'ClientCompanyController@admins')->name('client.company.admins');
    Route::get('/company/{company}/branches/create', 'ClientCompanyController@createBranch')->name('client.company.branches.create');
    Route::post('/company/{company}/branches', 'ClientCompanyController@storeBranch')->name('client.company.branches.store');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'roles:admins']], function () {
    // GET.
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/reports', 'ReportsController@index')->name('reports.index');
    
    // Companies.
    Route::resource('companies', 'CompaniesController');
    Route::get('/companies/{company}/admins', 'CompaniesController@admins')->name('companies.admins');

    // Branches.
    Route::resource('companies/{company}/branches', 'BranchesController');
    Route::get('/companies/{company}/branches/{branch}/orders', 'BranchesController@orders')->name('branches.orders');
    Route::get('/companies/{company}/branches/{branch}/members', 'BranchesController@members')->name('branches.members');

    // Refactored Controllers.
    Route::resource('products', 'ProductsController');
    Route::resource('orders', 'OrdersController', ['only' => ['index', 'show', 'update']]);
    Route::resource('staff', 'StaffController', ['parameters' => ['staff' => 'user']]);

    // Notifications
    Route::get('notifications', 'NotificationsController@index');
    Route::get('notifications/read', 'NotificationsController@read');
});

Route::group(['prefix' => 'branch/{branch}', 'middleware' => ['auth', 'roles:clients']], function () {
    // General.
    Route::get('/', 'ClientHomeController@index')->name('client.home');
    Route::get('/services', 'ClientServicesController@index')->name('client.services.index');
    Route::get('/reports', 'ClientReportsController@index')->name('client.reports.index');

    // Company.
    Route::get('/company/{company}', 'ClientCompanyController@index')->name('client.company.index');
    Route::patch('/company/{company}/update_company', 'ClientCompanyController@updateCompany')->name('client.company.update-company');
    Route::patch('/company/{company}/update_branch', 'ClientCompanyController@updateBranch')->name('client.company.update-branch');

    // History.
    Route::get('/history', 'ClientHistoryController@index')->name('client.history.index');
    Route::get('/history/{order}', 'ClientHistoryController@show')->name('client.history.show');

    // Quote.
    Route::get('/quote', 'ClientProductsController@index')->name('client.products.index');
    Route::get('/quote/{quote}/add', 'ClientProductsController@add')->name('client.products.add');
    Route::get('/quote/{quote}/checkout', 'ClientProductsController@checkout')->name('client.products.checkout');
    Route::post('/quote/{quote}/make_checkout', 'ClientProductsController@store')->name('client.quote.make-checkout');
    Route::get('/quote/complete', 'ClientProductsController@complete')->name('client.quote.complete');
});

// Change password after invite an user.
Route::get('/complete/{token}', 'UsersController@complete')->name('users.complete');
Route::patch('/complete/{token}', 'UsersController@completeProfile')->name('users.complete-profile');

Route::get('/', function () { return redirect()->route('login'); });
Route::get('/home', 'HomeController@branch')->middleware(['auth']);
Route::get('/change_branch', 'ClientHomeController@branch')->middleware('auth');
Route::get('/register', function () { return abort(404); }); // Register not needed.
