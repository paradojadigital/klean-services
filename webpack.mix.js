const mix  = require('laravel-mix')
const path = require('path')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const wp_config = {
    resolve: {
        extensions: ['.js', '.vue'],
        alias: { '@': path.join(__dirname, '/resources/assets/js') }
    }
}

const bs_config = {
    notify: false,
    proxy: 'localhost:8000',
    // files: [
    //     'app/**/*',
    //     'public/**/*',
    //     'resources/views/**/*',
    //     'resources/lang/**/*',
    //     'routes/*'
    // ]
}

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.sass', 'public/css')
   .extract(['vue', 'datamaps', 'chart.js', 'moment', 'lodash'])
   .webpackConfig(wp_config)
   .disableNotifications()
   .browserSync(bs_config)
